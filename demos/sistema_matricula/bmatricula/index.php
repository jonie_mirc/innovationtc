<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

//suave
require_once('vendor/autoload.php');
use Phalcon\Mvc\Micro;
use \Firebase\JWT\JWT;  
use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Http\Response;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
require_once('PHPMailer/PHPMailerAutoload.php');


// include('app/pojos/MPersonModel.php');
// include('app/pojos/MUserModel.php');

//Loader() Autoloads the models
/*-----------------------------------------------------------------------------------------------*/
$loader = new Loader();

$loader->registerDirs(
    array(
        __DIR__ . '/app/models/',
        __DIR__ . '/app/pojos/'
    )
)->register();

$di = new FactoryDefault();

// Set up database connection
/*-----------------------------------------------------------------------------------------------*/
$di->set('db', function () {
    return new PdoMysql(
        array(
            "host"          =>      "localhost",
            "username"      =>      "root",
            "password"      =>      "",
            "dbname"        =>      "bd_matricula",
            "charset"       =>      "utf8"
        )
    );
});


// Objeto de conexion
$app = new Micro($di);

$app->post('/login', function () use ($app) {
    $response = new Phalcon\Http\Response();
    $request_data = $app->request->getJsonRawBody();

    $username = $request_data->username;
    $pass = $request_data->userpassword;

    #creacion de llave para la encriptacion
    $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
    #creacion de la llave compuesta por tres letras del username
    $llave = substr($username, 0,2).''.substr($username, -1);
    $llave = strtoupper($llave);
    #encriptacion de la llave
    $llave = encrypt($llave,$key);
    #encriptacion del password como complemento del password total
    $complement = encrypt($pass,$key);
    #concatenacion de la llave con el complemento para generar un password seguro
    $password_encrypted = $llave.$complement;
    #encriptando el password
    $password_encrypted = encrypt($password_encrypted,$key);

    try{
        
        $log = 1;
        $MUser = MUser::query()
                ->where("user_name = :username:")
                ->andWhere("user_password = :password:")
                ->andWhere("user_log = :log:")
                ->bind(array("username" => $username,
                             "password" => $password_encrypted,
                             "log" => $log
                             )
                    )
                ->execute();
        $idUser = $MUser[0]->user_id;
        
        $changePassword = $MUser[0]->changePassword;
        $personDate = MPerson::findFirst(
                                        MUsersbyperson::find(
                                            [
                                                "user_id = $idUser"
                                            ]
                                        )[0]->person_id
                                    );

        
        if (count($MUser)>0) {
            $key = MDatatoken::findFirst(1)->datatoken_id;
            $token = array(
                "user" => $MUser[0]->user_name,
                "timeLog" => date('Y-m-d')
            );

            $jwt = JWT::encode($token, $key);
            http_response_code(200);
            $phql = 'SELECT 
                            B.profile_desciption,
                            D.resource_name,
                            D.resource_superiorCode,
                            D.resource_id,
                            D.resource_url,
                            D.resource_icon 
                     FROM MUser A
                     LEFT JOIN MProfile B
                     ON(A.profile_id = B.profile_id)
                     LEFT JOIN MProfilebyresource C
                     ON(B.profile_id = C.profile_id)
                     LEFT JOIN MResource D
                     ON(C.resource_id = D.resource_id)
                     WHERE A.user_id = :id:';

            $resources = $app->modelsManager->executeQuery($phql, array(
                        'id'      => $idUser
                     ));
            $resourcesP = array();
            $profileName = '';
            if(COUNT($resources)>0){
                $profileName = $resources[0]->profile_desciption;
                foreach ($resources as $resource ) {
                    if($resource->resource_id == $resource->resource_superiorCode){
                        $menu = new MMenu();
                        $menu->resource_name = $resource->resource_name;
                        $menu->resource_id = $resource->resource_id;
                        $menu->resource_url = $resource->resource_url;
                        $menu->resource_icon = $resource->resource_icon;
                        array_push($resourcesP, $menu);  
                    }
                }
                foreach ($resourcesP as $resourceP ) {
                    foreach ($resources as $resource ) {
                        if($resourceP->resource_id == $resource->resource_superiorCode && $resourceP->resource_id != $resource->resource_id){
                            array_push($resourceP->listResources, $resource);  
                        }
                    }
                }
            }         
            
            
        }
        else{
            $jwt = 0;
            http_response_code(403);
        }
        $response->setJsonContent(
           array(
                'status' => http_response_code(),
                'message' => 'success',
                'userToken' => $jwt,
                'data' => array(
                    'changePassword' => $changePassword,
                    'userName' => $username,
                    'personId' => $personDate->person_id,
                    'courseId' => $personDate->course_id,
                    'personImage' => $personDate->person_image,
                    'userCompleteName' => $personDate->person_name,
                    'userRole' => $profileName,
                    'userMenu'=>$resourcesP
                )
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'bodegasTipo' => $e->getMessage(),
            'error'=>'si un error',
            'password_encrypted'=>$password_encrypted,
            'complement'=>$pass,
            'username'=>$username
           )
            
        );
    
        return $response;
    }
});

$app->post('/addStudent', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $person_name=$request_data->person_name;
        $person_identity=$request_data->person_identity;
        $person_celphone=$request_data->person_celphone;
        $person_phone=$request_data->person_phone;
        $person_email=$request_data->person_email;
        $person_birthdate=$request_data->person_birthdate;
        $course_id=$request_data->course_id;
        $person_direction=$request_data->person_direction;
        $person_genus=$request_data->person_genus;
        $person_image=$request_data->person_image;
        $profile_id=$request_data->profile_id;
        
        $personExists = MPerson::find(
                                        [
                                            "person_identity = $person_identity"
                                        ]
                                    );
        if(COUNT($personExists)==0){
            $lengthIdentity = strlen($person_identity);
            $pattern = '/\d/i';
            $substitution = '';
            $resultValidation = preg_replace($pattern, $substitution, $person_identity);

            $msjErr = "";
            if($lengthIdentity == 13 && $resultValidation==''){
                $manager = new TxManager();
                $transaction = $manager->get();

                $person = new MPerson();
                $person->setTransaction($transaction);

                $person->person_name=$person_name;
                $person->person_identity=$person_identity;
                $person->person_celphone=$person_celphone;
                $person->person_phone=$person_phone;
                $person->person_email=$person_email;
                $person->person_birthdate=$person_birthdate;
                $person->course_id=$course_id;
                $person->person_direction=$person_direction;
                $person->person_genus=$person_genus;
                $person->person_image=$person_image;
                
                $isCommit = $person->save();

                if ($isCommit === false) {
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo insert en persona"
                    );
                    $msjErr = "No se pudo realizar el registro, fallo insert en persona";
                }
                elseif($isCommit===true){
                    #creacion de llave para la encriptacion
                    $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
                    
                    $time_start = microtime(true);
                    $tokenName = explode(" ", $person_name);
                    $now = date("Y-m-d H:i:s");
                    if(COUNT($tokenName) > 0){
                        #creacion de la llave compuesta por tres letras del username
                        $llave = substr($person_identity, 0,2).substr($person_identity, -1);
                        $llave = strtoupper($llave);
                        #encriptacion de la llave
                        $llave = encrypt($llave,$key);
                        #encriptacion del password como complemento del password total
                        $complemetName = substr($person_name, 0,2).substr($person_name, -1);
                        $completePass = $complemetName.''.$time_start;

                        $complement = encrypt($completePass,$key);
                        #concatenacion de la llave con el complemento para generar un password seguro
                        $password_encrypted = $llave.$complement;
                        #encriptando el password
                        $password_encrypted = encrypt($password_encrypted,$key);
                    }
                    $msjErr = "La persona si se registro";
                    $MUser = new MUser();
                    $MUser->setTransaction($transaction);

                    $MUser->user_name= $person->person_identity;
                    $MUser->user_password=$password_encrypted;
                    $MUser->user_log= 1;
                    $MUser->user_creationDate= $now;
                    $MUser->profile_id= $profile_id;
                    $MUser->changePassword = 1;

                    $isCommitUser = $MUser->save();
                    if ($isCommitUser === false) {
                        http_response_code(406);
                        $transaction->rollback(
                            "No se pudo realizar el registro, fallo inserten usuarios"
                        );
                        $msjErr = "No se pudo realizar el registro, fallo inserten usuarios";
                    }
                    elseif($isCommitUser === true){
                        $userByPerson = new MUsersbyperson();
                        $userByPerson->setTransaction($transaction);
                        $msjErr = "El usuario si se registro";
                        $userByPerson->user_id = $MUser->user_id;
                        $userByPerson->person_id = $person->person_id;

                        $isCommitUserByPerson = $userByPerson->save();

                        if ($isCommitUserByPerson === false) {
                            http_response_code(406);
                            $transaction->rollback(
                                "No se pudo realizar el registro, fallo insert en usuarios por persona"
                            );
                            $msjErr = "No se pudo realizar el registro, fallo insert en usuarios por persona";
                        }
                        elseif($isCommitUserByPerson === true){

                            $msjErr = "Tabien se registro el usuario por persona";
                            $message = '<b>'.$person_name.'</b>, bienvenido(a) a nuestro sistema. <br ><p><b>¡Su registro fue exitoso.!</b></p><br >Datos: <br >Usuario: '.$person_identity.'<br >Contrase&#241;a: '.$completePass;
                            $subject = 'Matricula AHBEJA';
                            try{
                                sendMailData($person_email,$message,$subject);
                                http_response_code(200);
                            }catch(\Exception $e){
                                $msjErr = "Lo que fallo fue el envio de correo.";
                                http_response_code(406);
                            }
                            
                        }
                    }
                }

                $transaction->commit();
                $statusCode = http_response_code();
                if($statusCode != 200){
                    $response->setJsonContent(
                        array(
                            'status' => http_response_code(),
                            'message' => $msjErr,
                            'data' => ''
                        )
                    );
                }
                elseif($statusCode == 200){
                    $response->setJsonContent(
                        array(
                            'status' => http_response_code(),
                            'message' => 'success',
                            'data' => array(
                                'data'=>$person
                            ) 
                        )
                    );
                }
            }
            else{
                http_response_code(400);
                $response->setJsonContent(
                    array(
                        'status' => http_response_code(),
                        'message' => 'El numero de identidad no tiene el formato correcto',
                        'data' => ''
                    )
                );
            }
        }
        else{
            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'Este usuario ya fue registrado',
                    'data' => ''
                )
            );
        }
        
        
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});
$app->post('/user', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $user_name=$request_data->userName;
        $profile_id=$request_data->profileId;
        $person_id=$request_data->personId;

        $personExists = MPerson::findFirst($person_id);
        $person_email = $personExists->person_email;    
        $person_name =  $personExists->person_name;
        $person_identity =  $user_name;

        $userExists = MUser::find(
                                    [
                                        "user_name = '$user_name'"
                                    ]
                                );
        if(COUNT($userExists)>0){

            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'Este nombre de usuario ya fue registrado',
                    'data' => ''
                )
            );

            return $response;
        }

        if(COUNT($personExists)>0){
            $manager = new TxManager();
            $transaction = $manager->get();

            #creacion de llave para la encriptacion
            $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
            
            $time_start = microtime(true);
            $now = date("Y-m-d H:i:s");

            #creacion de la llave compuesta por tres letras del username
            $llave = substr($user_name, 0,2).substr($user_name, -1);
            $llave = strtoupper($llave);

            $completePass = $llave.''.$time_start; 
            #encriptacion de la llave
            $llave = encrypt($llave,$key);
            #encriptacion del password como complemento del password total
            $complement = encrypt($completePass,$key);
            #concatenacion de la llave con el complemento para generar un password seguro
            $password_encrypted = $llave.$complement;
            #encriptando el password
            $password_encrypted = encrypt($password_encrypted,$key);
        

            $MUser = new MUser();
            $MUser->setTransaction($transaction);

            $MUser->user_name= $user_name;
            $MUser->user_password=$password_encrypted;
            $MUser->user_log= 1;
            $MUser->user_creationDate= $now;
            $MUser->profile_id= $profile_id;
            $MUser->changePassword = 1;

            $isCommitUser = $MUser->save();
            if ($isCommitUser === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo inserten usuarios"
                );
            }
            elseif($isCommitUser === true){
                $userByPerson = new MUsersbyperson();
                $userByPerson->setTransaction($transaction);

                $userByPerson->user_id = $MUser->user_id;
                $userByPerson->person_id = $person_id;

                $isCommitUserByPerson = $userByPerson->save();

                if ($isCommitUserByPerson === false) {
                    http_response_code(406);
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo insert en usuarios por persona"
                    );

                }
                elseif($isCommitUserByPerson === true){
                    $message = '<b>'.$person_name.'</b>, bienvenido(a) a nuestro sistema. <br ><p><b>¡Su registro fue exitoso.!</b></p><br >Datos: <br >Usuario: '.$person_identity.'<br >Contrase&#241;a: '.$completePass;
                    $subject = 'Matricula del instituto biblico INSUMIL';
                    try{
                        sendMailData($person_email,$message,$subject);
                        http_response_code(200);
                    }catch(\Exception $e){
                        http_response_code(406);
                    }
                    
                }
            }
            

            $transaction->commit();
            $statusCode = http_response_code();
            if($statusCode != 200){
                $response->setJsonContent(
                    array(
                        'status' => http_response_code(),
                        'message' => 'No se pudo realizar el registro',
                        'data' => ''
                    )
                );
            }
            elseif($statusCode == 200){
                $response->setJsonContent(
                    array(
                        'status' => http_response_code(),
                        'message' => 'success',
                        'data' => array(
                            'data'=>$MUser
                        ) 
                    )
                );
            }
            
        }
        else{
            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'Este usuario aun no ha sido registrado',
                    'data' => ''
                )
            );
        }
        
        
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/allPersons',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allPersons = MPerson::find();
        $usersByPerson = array();
        foreach ($allPersons as $res) {
            $person = new MPersonModel();
            $person->person_id = $res->person_id;
            $person->person_name=$res->person_name;
            $person->person_identity=$res->person_identity;
            $person->person_celphone=$res->person_celphone;
            $person->person_phone=$res->person_phone;
            $person->person_email=$res->person_email;
            $person->person_birthdate=$res->person_birthdate;
            $person->course_id=$res->course_id;
            $person->person_direction=$res->person_direction;
            $person->person_genus=$res->person_genus;
            $person->person_image=$res->person_image;
            $allUsersPerson = array();
            
            $users = MUsersbyperson::find(
                                    [
                                        "person_id = '$res->person_id'"
                                    ]
                                );
            foreach ($users as $resuser) {
                $user = MUser::findFirst($resuser->user_id);
                $MUser = new MUserModel();
                $MUser->user_id = $user->user_id;
                $MUser->user_name= $user->user_name;
                $MUser->user_log= $user->user_log;
                $MUser->user_creationDate= $user->user_creationDate;
                $MUser->profile = MProfile::findFirst($user->profile_id);
                
                array_push($allUsersPerson,$MUser);
            }
            $person->usersByPerson = $allUsersPerson;
            array_push($usersByPerson,$person);
        }
        http_response_code(200);
        $response->setJsonContent(
           array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $usersByPerson,
                'dt'=>$users
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/slidesbyprofile/{profile_description}',function ($profile_description) use($app){
    $response = new Phalcon\Http\Response();
    try{
        $profile = MProfile::find(
            [
                "profile_desciption = '$profile_description'"
            ]
        );
        $allSlides = [];
        if(COUNT($profile)>0){
            $profile_id = $profile[0]->profile_id;
            $allSlides = CCarousel::find(
                [
                    "profile_id = '$profile_id'"
                ]
            );
        }
        
        
        http_response_code(200);
        $response->setJsonContent(
           array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allSlides
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->put('/person', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        $person_id=$request_data->person_id;
        $person_name=$request_data->person_name;
        $person_identity=$request_data->person_identity;
        $person_celphone=$request_data->person_celphone;
        $person_phone=$request_data->person_phone;
        $person_email=$request_data->person_email;
        $person_birthdate=$request_data->person_birthdate;
        $course_id=$request_data->course_id;
        $person_direction=$request_data->person_direction;
        $person_genus=$request_data->person_genus;
        $person_image=$request_data->person_image;
        $profile_id=$request_data->profile_id;
        
        $personExists = MPerson::find(
                                        [
                                            "person_identity = $person_identity"
                                        ]
                                    );
        if(COUNT($personExists)>0){
            $lengthIdentity = strlen($person_identity);
            $pattern = '/\d/i';
            $substitution = '';
            $resultValidation = preg_replace($pattern, $substitution, $person_identity);


            if($lengthIdentity == 13 && $resultValidation==''){
                $manager = new TxManager();
                $transaction = $manager->get();

                $person = new MPerson();
                $person->setTransaction($transaction);
                $person->person_id = $person_id;
                $person->person_name=$person_name;
                $person->person_identity=$person_identity;
                $person->person_celphone=$person_celphone;
                $person->person_phone=$person_phone;
                $person->person_email=$person_email;
                $person->person_birthdate=$person_birthdate;
                $person->course_id=$course_id;
                $person->person_direction=$person_direction;
                $person->person_genus=$person_genus;
                $person->person_image=$person_image;
                
                $isCommit = $person->save();

                if ($isCommit === false) {
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo insert en persona"
                    );
                }
                

                $transaction->commit();
                $statusCode = http_response_code();
                if($statusCode != 200){
                    $response->setJsonContent(
                        array(
                            'status' => http_response_code(),
                            'message' => 'No se pudo realizar el registro',
                            'data' => ''
                        )
                    );
                }
                elseif($statusCode == 200){
                    $response->setJsonContent(
                        array(
                            'status' => http_response_code(),
                            'message' => 'success',
                            'data' => array(
                                'data'=>$person
                            ) 
                        )
                    );
                }
            }
            else{
                http_response_code(400);
                $response->setJsonContent(
                    array(
                        'status' => http_response_code(),
                        'message' => 'El numero de identidad no tiene el formato correcto',
                        'data' => ''
                    )
                );
            }
        }
        else{
            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'Este usuario no se encuentra registrado',
                    'data' => ''
                )
            );
        }
        
        
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/allCourses',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allCourses = MCourse::find();
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $allCourses
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/classes',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allClasses = MClass::find();

        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allClasses
            )
        );
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/classes/{course_id}',function ($course_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allClasses = MClassebycourse::find(
                                            [
                                                "course_id = '$course_id'"
                                            ]
                                          );
        if(COUNT($allClasses)>0){

            $classes = array();
            foreach($allClasses as $resclasses){
                $class = MClass::findFirst($resclasses->class_id);
                array_push($classes,$class);
            }
            http_response_code(200);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'success',
                    'data' => $classes
                )
            );
            
        }
        else{
            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'No hay clases disponibles para este curso',
                    'data' => null
                )
            );
        }
        
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/sections/{id_class}',function ($id_class) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        // $allSections = MSection::find(
        //     [
        //         "class_id='$id_class'",
        //         "period_id='$period->parameter_value'",
        //         "section_date = ''"
        //     ]
        // );
        
        $period = MParameter::find(
            [
                "parameter_name='currentPeriod'"
            ]
        );
        $date = MParameter::find(
            [
                "parameter_name='startPeriod'"
            ]
        );
        $line = split("-", $date[0]->parameter_value);
         $phql2 = 'SELECT 
                        *
                FROM MSection
                WHERE YEAR(section_date) = :year:
                AND class_id = :class_id:
                AND period_id = :period:';

        $allSections = $app->modelsManager->executeQuery($phql2, array(
                    'year'           => (int) $line[0],
                    'class_id'      => (int) $id_class,
                    'period'         => (int) $period[0]->parameter_value
                 ));
        
        $sections = array();
        if(COUNT($allSections) > 0){
            foreach( $allSections as $ressection ){
                $sectionModel = new MSectionModel();
                $sectionModel->section_id = $ressection->section_id;
                $sectionModel->section_date = $ressection->section_date;
                $sectionModel->schedule_id = $ressection->schedule_id;
                $sectionModel->schedule_start = MSchedule::findFirst($ressection->schedule_id)->schedule_start;
                $sectionModel->schedule_end = MSchedule::findFirst($ressection->schedule_id)->schedule_end;
                $sectionModel->class_id = $ressection->class_id;
                $sectionModel->class = MClass::findFirst($ressection->class_id)->class_name;
                $sectionModel->building_id = MClassroom::findFirst($ressection->classroom_id)->building_id;
                $sectionModel->building = MBuilding::findFirst(MClassroom::findFirst($ressection->classroom_id)->building_id)->building_name;
                $sectionModel->classDesciption = MClass::findFirst($ressection->class_id)->class_description;
                $sectionModel->classroom_id = $ressection->classroom_id;
                $sectionModel->classroom = MClassroom::findFirst($ressection->classroom_id)->classroom_name;
                $sectionModel->period_id = $ressection->period_id;
                $sectionModel->period = MPeriod::findFirst($ressection->period_id)->period_name;
                $sectionModel->person_id = $ressection->person_id;
                $sectionModel->person = MPerson::findFirst($ressection->person_id)->person_name;
                $sectionModel->classDay_id = $ressection->classDay_id;
                $sectionModel->classDay = MClassday::findFirst($ressection->classDay_id)->classDay_name;
                $sectionModel->sectionDescription = MSchedule::findFirst($ressection->schedule_id)->schedule_start.'-'.MSchedule::findFirst($ressection->schedule_id)->schedule_end.' '.MClassday::findFirst($ressection->classDay_id)->classDay_name;

                array_push($sections,$sectionModel );
            }
        }
        else{

        }
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $sections
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/allProfiles',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allProfiles = MProfile::find();
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $allProfiles
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/currentEnrollment/{person_id}/{period}/{year}',function ($person_id,$period,$year) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $phql = 'SELECT 
                        A.enrolment_id,
                        A.person_id,
                        A.section_id,
                        A.enrolment_date,
                        YEAR(A.enrolment_date) as enrolment_year 
                FROM MEnrolment A
                LEFT JOIN MSection B
                ON(A.section_id = B.section_id)
                WHERE YEAR(A.enrolment_date) = :year:
                AND A.person_id = :person_id:
                AND B.period_id = :period:';

            $enrollmentNow = $app->modelsManager->executeQuery($phql, array(
                        'year'           => (int) $year,
                        'person_id'      => (int) $person_id,
                        'period'         => (int) $period
                     ));
            $enrollmentArray = array();
            if(COUNT($enrollmentNow) > 0){
                foreach( $enrollmentNow as $ressection ){
                    $sectionModel = new MSectionModel();
                    $sectionModel->enrollment_id = $ressection->enrolment_id;
                    $sectionModel->section_id = $ressection->section_id;
                    $sectionModel->section_date = $ressection->enrolment_date;
                    $sectionModel->schedule_id = MSection::findFirst($ressection->section_id)->schedule_id;
                    $sectionModel->schedule_start = MSchedule::findFirst(MSection::findFirst($ressection->section_id)->schedule_id)->schedule_start;
                    $sectionModel->schedule_end = MSchedule::findFirst(MSection::findFirst($ressection->section_id)->schedule_id)->schedule_end;
                    $sectionModel->class_id = MSection::findFirst($ressection->section_id)->class_id;
                    $sectionModel->class = MClass::findFirst(MSection::findFirst($ressection->section_id)->class_id)->class_name;
                    $sectionModel->building_id = MClassroom::findFirst(MSection::findFirst($ressection->section_id)->classroom_id)->building_id;
                    $sectionModel->building = MBuilding::findFirst(MClassroom::findFirst(MSection::findFirst($ressection->section_id)->classroom_id)->building_id)->building_name;
                    $sectionModel->classDesciption = MClass::findFirst(MSection::findFirst($ressection->section_id)->class_id)->class_description;
                    $sectionModel->classroom_id = MSection::findFirst($ressection->section_id)->classroom_id;
                    $sectionModel->classroom = MClassroom::findFirst(MSection::findFirst($ressection->section_id)->classroom_id)->classroom_name;
                    $sectionModel->period_id = MSection::findFirst($ressection->section_id)->period_id;
                    $sectionModel->period = MPeriod::findFirst(MSection::findFirst($ressection->section_id)->period_id)->period_name;
                    $sectionModel->student = $person_id;
                    $sectionModel->person_id = MSection::findFirst($ressection->section_id)->person_id;
                    $sectionModel->person = MPerson::findFirst(MSection::findFirst($ressection->section_id)->person_id)->person_name;
                    $sectionModel->classDay_id = MSection::findFirst($ressection->section_id)->classDay_id;
                    $sectionModel->classDay = MClassday::findFirst(MSection::findFirst($ressection->section_id)->classDay_id)->classDay_name;
                    $sectionModel->sectionDescription = MSchedule::findFirst(MSection::findFirst($ressection->section_id)->schedule_id)->schedule_start.'-'.MSchedule::findFirst(MSection::findFirst($ressection->section_id)->schedule_id)->schedule_end.' '.MClassday::findFirst(MSection::findFirst($ressection->section_id)->classDay_id)->classDay_name;

                    array_push($enrollmentArray,$sectionModel );
                }
            }
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $enrollmentArray
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/countEnrollment/{period}/{year}',function ($period,$year) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $phql = 'SELECT 
                        COUNT(A.enrolment_id) as totalMatricula
                FROM MEnrolment A
                LEFT JOIN MSection B
                ON(A.section_id = B.section_id)
                WHERE YEAR(A.enrolment_date) = :year:
                AND B.period_id = :period:';

            $enrollmentNow = $app->modelsManager->executeQuery($phql, array(
                        'year'           => (int) $year,
                        'period'         => (int) $period
                     ));
            
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $enrollmentNow[0]->totalMatricula
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/parameters',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allParameters = MParameter::find();
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $allParameters
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/searchMaster/{section_id}',function ($section_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $section = MSection::findFirst($section_id);
        $person = MPerson::findFirst($section->person_id);
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $person
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/masters/{typeperson}',function ($typeperson) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $phql = 'SELECT     C.person_id,
                            C.person_name
                 FROM MUsersbyperson A
                 LEFT JOIN MUser B
                 ON(A.user_id = B.user_id)
                 LEFT JOIN MPerson C
                 ON(A.person_id = C.person_id)
                 WHERE B.profile_id = :typeperson:';

        $persons = $app->modelsManager->executeQuery($phql, array(
                        'typeperson'         => $typeperson
                     ));

        http_response_code(200);
        $response->setJsonContent(
           array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $persons
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});
$app->post('/enrollment',function() use ($app){
    $response = new Phalcon\Http\Response();
    $request_data = $app->request->getJsonRawBody(); 


    try{
        if(COUNT($request_data)>0){
            
                
            
            
            $message = 'Tiene datos correctos';


            $date = $request_data[0]->section_date;
            $person_id = $request_data[0]->student;
            $period_id = $request_data[0]->period_id;

            $phql = 'SELECT 
                            A.enrolment_id,
                            A.person_id,
                            A.section_id,
                            B.class_id,
                            A.enrolment_date,
                            YEAR(A.enrolment_date) as enrolment_year 
                    FROM MEnrolment A
                    LEFT JOIN MSection B
                    ON(A.section_id = B.section_id)
                    WHERE YEAR(A.enrolment_date) = YEAR(:year:)
                    AND A.person_id = :person_id:
                    AND B.period_id = :period:';

            $enrollmentNow = $app->modelsManager->executeQuery($phql, array(
                        'year'           => $date,
                        'person_id'      => (int) $person_id,
                        'period'         => (int) $period_id
                        ));
            

            $indexEnrollment = 0;
            if(COUNT($enrollmentNow)>0){

                
                $manager = new TxManager();
                $transaction = $manager->get();

                
                $contEnrollment = false;
                $cont = 0;
                
                foreach($request_data as $resenrollment){
                    
                    foreach($enrollmentNow as $rescurrentenrollment){
                    
                        if($resenrollment->class_id == $rescurrentenrollment->class_id || $resenrollment->section_id == $rescurrentenrollment->section_id){
                            $contEnrollment = true;
                            
                        }
                    }
                    
                    $dateNow = date("Y-m-d H:i:s"); 
                    if($contEnrollment === false){
                        
                        $enrollment = new MEnrolment();
                        $enrollment->setTransaction($transaction);

                        $enrollment->person_id =$resenrollment->student;
                        $enrollment->section_id =$resenrollment->section_id;
                        $enrollment->enrolment_date = $dateNow;

                        $isCommit = $enrollment->save();

                        $contEnrollment = false;
                        $message = "generando matricula";
                        if ($isCommit === false) {
                            $transaction->rollback(
                                "No se pudo realizar la matricula, fallo el registro "
                            );
                            http_response_code(406);
                            $errorEnrollment = true;
                            $message = "Error en el proceso de matricula";
                            break;
                        }
                        elseif($isCommit === true){
                            $newHistoric = new MHistoric();
                            $newHistoric->setTransaction($transaction);
                            $newHistoric->historic_qualification = 0;
                            $newHistoric->historic_observation = "ND";
                            $newHistoric->historic_date = $dateNow;
                            $newHistoric->enrolment_id = $enrollment->enrolment_id;

                            $isHistoricCommit = $newHistoric->save();

                            if ($isHistoricCommit === false) {
                                
                                $transaction->rollback(
                                    "No se genero el historial de clase, fallo el registro"
                                );
                                http_response_code(406);
                                $message = "Error en el proceso de matricula";
                                $errorEnrollment = true;
                            }else if($isHistoricCommit){

                                $paramenters = MParameter::find();
                                $paymentByEnrollment = new MPaymentbyenrolment();
                                $paymentByEnrollment->setTransaction($transaction);
                                $paymentByEnrollment->paymentConcept_id = $paramenters[5]->parameter_value;
                                $paymentByEnrollment->enrolment_id = $enrollment->enrolment_id;
                                $paymentByEnrollment->person_id = $resenrollment->student;
                                $paymentByEnrollment->paymentByEnrolment_state = 0;
                                // $paymentByEnrollment->paymentByEnrolment_date = 
                                $isPaymentByEnrollmentCommit = $paymentByEnrollment->save();
                                if ($isPaymentByEnrollmentCommit === false) {
                                    
                                    $transaction->rollback(
                                        "No se genero el estado de pago, fallo el registro payment"
                                    );
                                    http_response_code(406);
                                    $message = "Error en el proceso de matricula";
                                    $errorEnrollment = true;
                                }
                                else if($isPaymentByEnrollmentCommit){
                                    http_response_code(200);
                                    $message = "success";
                                    
                                }
                            }
                            
                        }
                        
                    }
                    else{
                        $cont++;
                    }
                    $contEnrollment = false;
                }
                
            }
            else{
                
                $manager = new TxManager();
                $transaction = $manager->get();

                 $dateNow = date("Y-m-d H:i:s"); 
                foreach($request_data as $resenrollment){
                    
                    if(true){
                        $enrollment = new MEnrolment();
                        $enrollment->setTransaction($transaction);

                        $enrollment->person_id =$resenrollment->student;
                        $enrollment->section_id =$resenrollment->section_id;
                        $enrollment->enrolment_date =$dateNow;

                        $isCommit = $enrollment->save();

                        if ($isCommit === false) {
                            $transaction->rollback(
                                "No se pudo realizar la matricula, fallo el registro"
                            );
                            http_response_code(406);
                            $errorEnrollment = true;
                            $message = "Error en el proceso de matricula";
                            break;
                        }elseif($isCommit === true){
                            $newHistoric = new MHistoric();
                            $newHistoric->setTransaction($transaction);
                            $newHistoric->historic_qualification = 0;
                            $newHistoric->historic_observation = "ND";
                            $newHistoric->historic_date = $dateNow;
                            $newHistoric->enrolment_id = $enrollment->enrolment_id;

                            $isHistoricCommit = $newHistoric->save();

                            if ($isHistoricCommit === false) {
                                
                                $transaction->rollback(
                                    "No se genero el historial de clase, fallo el registro hist"
                                );
                                http_response_code(406);
                                $message = "Error en el proceso de matricula";
                                $errorEnrollment = true;
                            }else if($isHistoricCommit){

                                $paramenters = MParameter::find();
                                $paymentByEnrollment = new MPaymentbyenrolment();
                                $paymentByEnrollment->setTransaction($transaction);
                                $paymentByEnrollment->paymentConcept_id = $paramenters[5]->parameter_value;
                                $paymentByEnrollment->enrolment_id = $enrollment->enrolment_id;
                                $paymentByEnrollment->person_id = $resenrollment->student;
                                $paymentByEnrollment->paymentByEnrolment_state = 0;
                                
                                $isPaymentByEnrollmentCommit = $paymentByEnrollment->save();
                                if ($isPaymentByEnrollmentCommit === false) {
                                    
                                    $transaction->rollback(
                                        "No se genero el estado de pago, fallo el registro payment"
                                    );
                                    http_response_code(406);
                                    $message = "Error en el proceso de matricula";
                                    $errorEnrollment = true;
                                }
                                else if($isPaymentByEnrollmentCommit){
                                    http_response_code(200);
                                    $message = "success";
                                    
                                }
                            }
                        }
                    }
                }    
            }
            
            if($cont != COUNT($request_data)){
                $transaction->commit();
            }
            else{
                $message = 'Estas asignaturas ya fueron matriculadas';
                http_response_code(406);    
            }
        }
        else{
            $message = 'Debe seleccionar almenos una clase para realizar la matricula';
            http_response_code(406);
        }
        
        
        $response->setJsonContent(
            array(
                    'status' => http_response_code(),
                    'message' => $message,
                    'data' => null
            )
        );
        return $response;
        // 'repeat'=>$cont,
        // 'enrollmentCur'=>COUNT($request_data),
        // 'person_id'=>$person_id,
        // 'sectionData'=>$date,
        
        // 'currentEnrollment'=>$enrollmentNow
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/historic/{person_id}',function ($person_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        $phql = 'SELECT 
                        A.enrolment_id,
                        A.section_id,
                        C.historic_id,
                        C.historic_qualification,
                        C.historic_observation,
                        C.historic_date,
                        YEAR(A.enrolment_date) as enrolment_year
                FROM MEnrolment A
                LEFT JOIN MHistoric C
                ON(A.enrolment_id = C.enrolment_id)
                WHERE A.person_id = :person_id:';

        $currentHistoric = $app->modelsManager->executeQuery($phql, array(
                    'person_id'      => (int) $person_id
                    ));
        
        $allClassHistoric = array();
        foreach($currentHistoric as $resenrolment){
            $historic = new MHistoricModel();
            $historic->enrolment_id = $resenrolment->enrolment_id;
            $historic->section_id = $resenrolment->section_id;
            $historic->historic_id = $resenrolment->historic_id;
            $historic->historic_qualification = $resenrolment->historic_qualification;
            $historic->historic_observation = $resenrolment->historic_observation;
            $historic->historic_date = $resenrolment->historic_date;
            $historic->enrolment_year = $resenrolment->enrolment_year;
            $historic->class_name = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_name;
            $historic->class_description = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_description;
            $historic->period = MPeriod::findFirst(MSection::findFirst($resenrolment->section_id)->period_id)->period_name;
            array_push($allClassHistoric, $historic);
        }
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $allClassHistoric
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->delete('/enrollment/{enrollment_id}',function ($enrollment_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $manager = new TxManager();
        $transaction = $manager->get();
        
        $currentEnrollment = MEnrolment::findFirst($enrollment_id);
        if(COUNT($currentEnrollment)>0){

            $enrollmentNow = new MEnrolment();
            $enrollmentNow->setTransaction($transaction);
            $enrollmentNow->enrolment_id = $currentEnrollment->enrolment_id;
            $enrollmentNow->person_id = $currentEnrollment->person_id;
            $enrollmentNow->section_id = $currentEnrollment->section_id;
            $enrollmentNow->enrolment_date = $currentEnrollment->enrolment_date;

            $isCommit = $enrollmentNow->delete();

            if($isCommit === false){
                http_response_code(406);
                $errorEnrollment = true;
                $message = "Error al cancelar asignatura";
                $messages = $enrollmentNow->getMessages();
                foreach ($messages as $message) {
                    $transaction->rollback(
                        $message->getMessage()
                    );
                }
            }
            else{
                $historic = MHistoric::find(
                    [
                        "enrolment_id='$enrollment_id'"
                    ]
                );
                
                if(COUNT($historic)>0){
                    $currentHistoric = new MHistoric();
                    $currentHistoric->historic_id = $historic[0]->historic_id;
                    $currentHistoric->historic_qualification = $historic[0]->historic_qualification;
                    $currentHistoric->historic_observation = $historic[0]->historic_observation;
                    $currentHistoric->historic_date = $historic[0]->historic_date;
                    $currentHistoric->enrolment_id = $historic[0]->enrolment_id;

                    $isHisctoricCommit = $currentHistoric->delete();
                    if($isHisctoricCommit===false){
                        http_response_code(406);
                        $errorEnrollment = true;
                        $message = "Error al cancelar asignatura";
                        $messages = $currentHistoric->getMessages();
                        foreach ($messages as $message) {
                            $transaction->rollback(
                                $message->getMessage()
                            );
                        }
                    }
                    else{
                        $currentPaymentByEnrollment = MPaymentbyenrolment::find(
                            ["enrolment_id='$enrollment_id'"]
                        );
                        if(COUNT($currentPaymentByEnrollment)>0){
                            $paymentByEnrollment = new MPaymentbyenrolment();
                            $paymentByEnrollment->paymentByEnrolment_id =$currentPaymentByEnrollment[0]->paymentByEnrolment_id;
                            $paymentByEnrollment->paymentConcept_id =$currentPaymentByEnrollment[0]->paymentConcept_id;
                            $paymentByEnrollment->enrolment_id =$currentPaymentByEnrollment[0]->enrolment_id;
                            $paymentByEnrollment->person_id =$currentPaymentByEnrollment[0]->person_id;
                            $paymentByEnrollment->paymentByEnrolment_state = $currentPaymentByEnrollment[0]->paymentByEnrolment_state;
                            $paymentByEnrollment->paymentByEnrolment_date =$currentPaymentByEnrollment[0]->paymentByEnrolment_date;

                            $isPaymentCommit = $paymentByEnrollment->delete();
                            if($isPaymentCommit===false){
                                http_response_code(406);
                                $errorEnrollment = true;
                                $message = "Error al cancelar asignatura";
                                $messages = $paymentByEnrollment->getMessages();
                                foreach ($messages as $message) {
                                    $transaction->rollback(
                                        $message->getMessage()
                                    );
                                }
                            }
                        }
                    }
                }
                
            }
        }
        else{
            http_response_code(406);
            $errorEnrollment = true;
            $message = "Esta asignatura ya fue cancelada";
        }
        $httpStatus = http_response_code();
        if($httpStatus == 200){
            $message = "success";
            $transaction->commit();
        }
        
        
        $response->setJsonContent(
           array(
                'status' => $httpStatus,
                'message' => $message,
                'data' => null
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/paymentState/{person_id}/{period}/{year}',function ($person_id,$period,$year) use($app){
    $response = new Phalcon\Http\Response();
    try{
        $phql = 'SELECT 
                        A.enrolment_id,
                        A.section_id,
                        C.paymentConcept_id,
                        C.person_id,
                        C.paymentByEnrolment_state,
                        C.paymentByEnrolment_date,
                        YEAR(A.enrolment_date) as enrolment_year
                FROM MEnrolment A
                LEFT JOIN MSection B
                ON(A.section_id = B.section_id)
                LEFT JOIN MPaymentbyenrolment C
                ON(A.enrolment_id = C.enrolment_id)
                WHERE YEAR(A.enrolment_date) = :year:
                AND A.person_id = :person_id:
                AND B.period_id = :period:';
                

        $currentPayment = $app->modelsManager->executeQuery($phql, array(
                    'year'           => (int) $year,
                    'person_id'      => (int) $person_id,
                    'period'         => (int) $period
        ));
        
        $allPayment = array();
        $paymentStructure = new MPaymentStructure();
        foreach($currentPayment as $resenrolment){
            $payment = new MPaymentModel();
            $dat = (int)$resenrolment->paymentConcept_id;
            $payment->enrolment_id = $resenrolment->enrolment_id;
            $payment->section_id = $resenrolment->section_id;
            $payment->paymentConcept_id = $resenrolment->paymentConcept_id;
            $payment->paymentConcept_amount = MPaymentconcept::findFirst($dat)->paymentConcept_amount;
            $payment->person_id = $resenrolment->person_id;
            $payment->paymentByEnrolment_state = $resenrolment->paymentByEnrolment_state;
            $payment->paymentByEnrolment_date = $resenrolment->paymentByEnrolment_date;
            $payment->enrolment_year = $resenrolment->enrolment_year;
            $payment->class_name = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_name;
            $payment->class_description = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_description;
            $payment->period = MPeriod::findFirst(MSection::findFirst($resenrolment->section_id)->period_id)->period_name;
            if($resenrolment->paymentByEnrolment_state == 0){
                $payment->paymentByEnrolment_stateName = 'Pendiente de pago';
                $paymentStructure->total += MPaymentconcept::findFirst($dat)->paymentConcept_amount;
            }else if($resenrolment->paymentByEnrolment_state == 1){
                $payment->paymentByEnrolment_stateName = 'Cancelado';
            }else if($resenrolment->paymentByEnrolment_state == 2){
                $payment->paymentByEnrolment_stateName = 'Becado';
            }
            array_push($allPayment, $payment);
            
        }
        $paymentStructure->allPaymentbyclass = $allPayment;
        
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $paymentStructure
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/paymentState/{person_id}',function ($person_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        $phql = 'SELECT 
                        A.enrolment_id,
                        A.section_id,
                        C.paymentConcept_id,
                        C.person_id,
                        C.paymentByEnrolment_state,
                        C.paymentByEnrolment_date,
                        YEAR(A.enrolment_date) as enrolment_year
                FROM MEnrolment A
                LEFT JOIN MSection B
                ON(A.section_id = B.section_id)
                LEFT JOIN MPaymentbyenrolment C
                ON(A.enrolment_id = C.enrolment_id)
                WHERE A.person_id = :person_id:
                ORDER BY C.paymentByEnrolment_date DESC';
                

        $currentPayment = $app->modelsManager->executeQuery($phql, array(
                    
                    'person_id'      => (int) $person_id
                    
        ));
        
        $allPayment = array();
        $paymentStructure = new MPaymentStructure();
        foreach($currentPayment as $resenrolment){
            $payment = new MPaymentModel();
            $dat = (int)$resenrolment->paymentConcept_id;
            $payment->enrolment_id = $resenrolment->enrolment_id;
            $payment->section_id = $resenrolment->section_id;
            $payment->paymentConcept_id = $resenrolment->paymentConcept_id;
            $payment->paymentConcept_amount = MPaymentconcept::findFirst($dat)->paymentConcept_amount;
            $payment->person_id = $resenrolment->person_id;
            $payment->paymentByEnrolment_state = $resenrolment->paymentByEnrolment_state;
            $payment->paymentByEnrolment_date = $resenrolment->paymentByEnrolment_date;
            $payment->enrolment_year = $resenrolment->enrolment_year;
            $payment->class_name = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_name;
            $payment->class_description = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_description;
            $payment->period = MPeriod::findFirst(MSection::findFirst($resenrolment->section_id)->period_id)->period_name;
            if($resenrolment->paymentByEnrolment_state == 0){
                $payment->paymentByEnrolment_stateName = 'Pendiente de pago';
                $paymentStructure->total += MPaymentconcept::findFirst($dat)->paymentConcept_amount;
            }else if($resenrolment->paymentByEnrolment_state == 1){
                $payment->paymentByEnrolment_stateName = 'Cancelado';
            }else if($resenrolment->paymentByEnrolment_state == 2){
                $payment->paymentByEnrolment_stateName = 'Becado';
            }
            array_push($allPayment, $payment);
            
        }
        $paymentStructure->allPaymentbyclass = $allPayment;
        
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $paymentStructure
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});
$app->get('/regionalCenter/{regionalCenter_id}',function ($regionalCenter_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        $center = MRegionalcenter::findFirst($regionalCenter_id);
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $center
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/historic/{person_id}/{period}/{year}',function ($person_id,$period,$year) use($app){
    $response = new Phalcon\Http\Response();
    try{
        $phql = 'SELECT 
                        A.enrolment_id,
                        A.section_id,
                        C.historic_id,
                        C.historic_qualification,
                        C.historic_observation,
                        C.historic_date,
                        YEAR(A.enrolment_date) as enrolment_year
                FROM MEnrolment A
                LEFT JOIN MSection B
                ON(A.section_id = B.section_id)
                LEFT JOIN MHistoric C
                ON(A.enrolment_id = C.enrolment_id)
                WHERE YEAR(A.enrolment_date) = :year:
                AND A.person_id = :person_id:
                AND B.period_id = :period:';

        $currentHistoric = $app->modelsManager->executeQuery($phql, array(
                    'year'           => (int) $year,
                    'person_id'      => (int) $person_id,
                    'period'         => (int) $period
                    ));
        
        $allClassHistoric = array();
        foreach($currentHistoric as $resenrolment){
            $historic = new MHistoricModel();
            $historic->enrolment_id = $resenrolment->enrolment_id;
            $historic->section_id = $resenrolment->section_id;
            $historic->historic_id = $resenrolment->historic_id;
            $historic->historic_qualification = $resenrolment->historic_qualification;
            $historic->historic_observation = $resenrolment->historic_observation;
            $historic->historic_date = $resenrolment->historic_date;
            $historic->enrolment_year = $resenrolment->enrolment_year;
            $historic->class_name = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_name;
            $historic->class_description = MClass::findFirst(MSection::findFirst($resenrolment->section_id)->class_id)->class_description;
            $historic->period = MPeriod::findFirst(MSection::findFirst($resenrolment->section_id)->period_id)->period_name;
            array_push($allClassHistoric, $historic);
        }
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $allClassHistoric
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->post('/changePassword',function() use ($app){
    $response = new Phalcon\Http\Response();
    $request_data = $app->request->getJsonRawBody(); 

    $type = $request_data->type;
    try{
        if($type == 2){
            $identity = $request_data->identity;
            $email = $request_data->email;
            $celphone = $request_data->celphone;
            $username = $request_data->username;
            
            $phql = 'SELECT 
                        C.person_name,
                        A.user_id,
                        A.user_name,
                        A.user_log,
                        A.user_creationDate,
                        A.profile_id
                FROM MUser A
                LEFT JOIN MUsersbyperson B
                ON(A.user_id = B.user_id)
                LEFT JOIN MPerson C
                ON(C.person_id = B.person_id)
                WHERE C.person_identity = :identity:
                AND C.person_email = :email:
                AND C.person_celphone = :celphone:
                AND A.user_name = :username:';

            $user = $app->modelsManager->executeQuery($phql, array(
                        "username" => $username,
                        "identity" => $identity,
                        "email" => $email,
                        "celphone" => $celphone
                     ));

            if(COUNT($user)>0){

                $now = date("D");
                $time_start = microtime(true);
                $llave = strtoupper($now);
                $clearpassword = $llave.''.$time_start;
                $password = encryptPass($request_data->username,$clearpassword);

                $manager = new TxManager();
                $transaction = $manager->get();

                $MUser = new MUser();
                $MUser->setTransaction($transaction);

                $MUser->user_id= $user[0]->user_id;
                $MUser->user_name= $user[0]->user_name;
                $MUser->user_password= $password;
                $MUser->user_log= 1;
                $MUser->changePassword= 1;
                $MUser->user_creationDate= $user[0]->user_creationDate;
                $MUser->profile_id= $user[0]->profile_id;
                
                $isCommit = $MUser->save();

                if ($isCommit === false) {
                    $transaction->rollback(
                        "No se pudo realizar el cambio de contraseña, fallo el proceso"
                    );
                    http_response_code(406);
                    $errorEnrollment = true;
                    $message = "Error en el proceso de cambio de contraseña";
                    
                }
                elseif($isCommit){
                    $transaction->commit();
                    $message = "success";
                    $msj = '<b>'.$user[0]->person_name.'</b>, <br ><p><b>¡contraseña cambiada con exito.!</b></p><br >Datos: <br >Usuario: '.$username.'<br >Contrase&#241;a: '.$clearpassword;
                    $subject = 'Recuperacion de contraseña';
                    try{
                        sendMailData($email,$msj,$subject);
                        http_response_code(200);
                    }catch(\Exception $e){
                        http_response_code(406);
                    }
                }

            }
            else{
                http_response_code(406);
                $message = 'Fallo la verificacion, respuestas incorrectas';
            }
                
            
        }
        elseif($type == 1){
            if(strlen($request_data->currentPassword)>0 && strlen($request_data->newPassword)>0 && strlen($request_data->confirmNewPassword)>0 && strlen($request_data->username)>0){
                $password = $request_data->currentPassword;
                $password = encryptPass($request_data->username,$password);

                $user = MUser::query()
                    ->where("user_name = :username:")
                    ->andWhere("user_password = :password:")
                    ->bind(array("username" => $request_data->username,
                                "password" => $password
                                )
                        )
                    ->execute();
                if(COUNT($user)>0 && $request_data->newPassword == $request_data->confirmNewPassword && $request_data->newPassword != $request_data->currentPassword){
                    $cleanPassword = $request_data->newPassword;
                    $password = encryptPass($request_data->username,$cleanPassword);
                    $manager = new TxManager();
                    $transaction = $manager->get();

                    $MUser = new MUser();
                    $MUser->setTransaction($transaction);

                    $MUser->user_id= $user[0]->user_id;
                    $MUser->user_name= $user[0]->user_name;
                    $MUser->user_password= $password;
                    $MUser->user_log= 1;
                    $MUser->user_creationDate= $user[0]->user_creationDate;
                    $MUser->profile_id= $user[0]->profile_id;
                    $MUser->changePassword= 0;
                    $isCommit = $MUser->save();

                    if ($isCommit === false) {
                        $transaction->rollback(
                            "No se pudo realizar el cambio de contraseña, fallo el proceso"
                        );
                        http_response_code(406);
                        $errorEnrollment = true;
                        $message = "Error en el proceso de cambio de contraseña";
                        
                    }
                    elseif($isCommit){
                        $transaction->commit();
                        $message = "success";
                        $msj = '<b>'.MPerson::findFirst(MUsersbyperson::find(["user_id='$MUser->user_id'"])[0]->person_id)->person_name.'</b>, <br ><p><b>¡contraseña cambiada con exito.!</b></p><br >Datos: <br >Usuario: '.$MUser->user_name.'<br >Contrase&#241;a: '.$cleanPassword;
                        $subject = 'Modificacion de contraseña';
                        try{
                            sendMailData(MPerson::findFirst(MUsersbyperson::find(["user_id='$MUser->user_id'"])[0]->person_id)->person_email,$msj,$subject);
                            http_response_code(200);
                        }catch(\Exception $e){
                            http_response_code(406);
                        }
                    }

                }
                else{
                    http_response_code(406);
                    if(COUNT($user)==0){
                        $message = 'Credenciales incorrectas';
                    }
                    if($request_data->newPassword != $request_data->confirmNewPassword){
                        $message = 'Las contraseñas no coinciden';
                    }
                    if($request_data->newPassword == $request_data->currentPassword){
                        $message = 'La nueva contraseña debe ser distinta a la actual';
                    }
                }
                    
                
            }
            else{
                http_response_code(406);
                $message = 'Todos los campos son requeridos';
            }
        }
        $response->setJsonContent(
            array(
                    'status' => http_response_code(),
                    'message' => $message,
                    'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/persons/{person_id}',function ($person_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $person = MPerson::findFirst($person_id);
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $person
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/grantees/{grantees_identity}',function ($grantees_identity) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $person = MGrantees::find(["grantees_identity = '$grantees_identity'"]);
        $allGGrantees = array();
         foreach($person as $res){
            $datos  = $person;
            $MCourse = new GGrantees();
            $MCourse->grantees_id  = $res->grantees_id ;
            $MCourse->grantees_name  = $res->grantees_name ;
            $MCourse->grantees_identity  = $res->grantees_identity ;
            $MCourse->grantees_email  = $res->grantees_email ;
            $MCourse->grantees_profession  = $res->grantees_profession ;
            $MCourse->grantees_program  = $res->coursesReceived_nroprogram ;
            $MCourse->grantees_coursetitle  = $res->coursesReceived_name ;
            $MCourse->grantees_jicacenter  = $res->grantees_jicacenter ;
            $MCourse->grantees_teammanager  = $res->grantees_teammanager ;
            $MCourse->grantees_startingdate  = $res->grantees_startingdate ;
            $MCourse->grantees_finishingdate  = $res->grantees_finishingdate ;
            $MCourse->grantees_numofperson  = $res->grantees_numofperson ;
            $MCourse->grantees_institution  = $res->grantees_institution ;
            $MCourse->grantees_celphone  = $res->grantees_celphone ;
            $MCourse->grantees_telephone  = $res->grantees_telephone ;
            $MCourse->department_id  = $res->department_id ;
            $MCourse->city_id  = $res->city_id ;
            $MCourse->grantees_creationDate  = $res->grantees_creationDate ;
            $MCourse->payment  = $res->payment ;
            $MCourse->language_id  = $res->language_id ;
            $MCourse->grantees_occupation  = $res->grantees_occupation ;
            $MCourse->grantees_birthdate  = $res->grantees_birthdate ;
            $MCourse->otherLanguages  = $res->otherLanguages ;
            $MCourse->courseReceived  = GCoursesreceived::find(
                                            [
                                                "person_id = '$res->grantees_id'"
                                            ]
                                        ); 

            array_push($allGGrantees, $MCourse);
        }
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $allGGrantees
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});




$app->get('/sections/{person_id}/{period}/{year}',function ($person_id,$period,$year) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $phql = 'SELECT 
                        section_id,
                        person_id,
                        section_date,
                        class_id,
                        classroom_id,
                        schedule_id,
                        period_id,
                        person_id,
                        classDay_id,
                        YEAR(section_date) as section_year 
                FROM MSection 
                WHERE YEAR(section_date) = :year:
                AND person_id = :person_id:
                AND period_id = :period:';

            $enrollmentNow = $app->modelsManager->executeQuery($phql, array(
                        'year'           => (int) $year,
                        'person_id'      => (int) $person_id,
                        'period'         => (int) $period
                     ));
            $enrollmentArray = array();
            if(COUNT($enrollmentNow) > 0){
                foreach( $enrollmentNow as $ressection ){
                    $sectionModel = new MSectionModel();
                    
                    $sectionModel->section_id = $ressection->section_id;
                    $sectionModel->section_date = $ressection->section_date;
                    $sectionModel->schedule_id = $ressection->schedule_id;
                    $sectionModel->schedule_start = MSchedule::findFirst($ressection->schedule_id)->schedule_start;
                    $sectionModel->schedule_end = MSchedule::findFirst($ressection->schedule_id)->schedule_end;
                    $sectionModel->class_id = $ressection->class_id;
                    $sectionModel->class = MClass::findFirst($ressection->class_id)->class_name;
                    $sectionModel->building_id = MClassroom::findFirst($ressection->classroom_id)->building_id;
                    $sectionModel->building = MBuilding::findFirst(MClassroom::findFirst($ressection->classroom_id)->building_id)->building_name;
                    $sectionModel->classDesciption = MClass::findFirst($ressection->class_id)->class_description;
                    $sectionModel->classroom_id = $ressection->classroom_id;
                    $sectionModel->classroom = MClassroom::findFirst($ressection->classroom_id)->classroom_name;
                    $sectionModel->period_id = $ressection->period_id;
                    $sectionModel->period = MPeriod::findFirst($ressection->period_id)->period_name;
                    $sectionModel->person_id = $ressection->person_id;
                    $sectionModel->person = MPerson::findFirst($ressection->person_id)->person_name;
                    $sectionModel->classDay_id = $ressection->classDay_id;
                    $sectionModel->classDay = MClassday::findFirst($ressection->classDay_id)->classDay_name;
                    $sectionModel->sectionDescription = MClass::findFirst($ressection->class_id)->class_name.' '.MSchedule::findFirst($ressection->schedule_id)->schedule_start.'-'.MSchedule::findFirst($ressection->schedule_id)->schedule_end.' '.MClassday::findFirst($ressection->classDay_id)->classDay_name;

                    array_push($enrollmentArray,$sectionModel );
                }
            }
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $enrollmentArray
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/sections/{period}/{year}',function ($period,$year) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $phql = 'SELECT 
                        section_id,
                        person_id,
                        section_date,
                        class_id,
                        classroom_id,
                        schedule_id,
                        period_id,
                        person_id,
                        classDay_id,
                        YEAR(section_date) as section_year 
                FROM MSection 
                WHERE YEAR(section_date) = :year:
                AND period_id = :period:';

            $enrollmentNow = $app->modelsManager->executeQuery($phql, array(
                        'year'           => (int) $year,
                        'period'         => (int) $period
                     ));
            $enrollmentArray = array();
            if(COUNT($enrollmentNow) > 0){
                foreach( $enrollmentNow as $ressection ){
                    $sectionModel = new MSectionModel();
                    
                    $sectionModel->section_id = $ressection->section_id;
                    $sectionModel->section_date = $ressection->section_date;
                    $sectionModel->schedule_id = $ressection->schedule_id;
                    $sectionModel->schedule_start = MSchedule::findFirst($ressection->schedule_id)->schedule_start;
                    $sectionModel->schedule_end = MSchedule::findFirst($ressection->schedule_id)->schedule_end;
                    $sectionModel->class_id = $ressection->class_id;
                    $sectionModel->classCode = MClass::findFirst($ressection->class_id)->class_name;
                    $sectionModel->building_id = MClassroom::findFirst($ressection->classroom_id)->building_id;
                    $sectionModel->building = MBuilding::findFirst(MClassroom::findFirst($ressection->classroom_id)->building_id)->building_name;
                    $sectionModel->classDesciption = MClass::findFirst($ressection->class_id)->class_description;
                    $sectionModel->classroom_id = $ressection->classroom_id;
                    $sectionModel->classroom = MClassroom::findFirst($ressection->classroom_id)->classroom_name;
                    $sectionModel->period_id = $ressection->period_id;
                    $sectionModel->period = MPeriod::findFirst($ressection->period_id)->period_name;
                    $sectionModel->person_id = $ressection->person_id;
                    $sectionModel->person = MPerson::findFirst($ressection->person_id)->person_name;
                    $sectionModel->course_id = MClassebycourse::find(["class_id = '$ressection->class_id'"])[0]->course_id;
                    $sectionModel->classDay_id = $ressection->classDay_id;
                    $sectionModel->classDay = MClassday::findFirst($ressection->classDay_id)->classDay_name;
                    $sectionModel->sectionDescription = MClass::findFirst($ressection->class_id)->class_name.' '.MSchedule::findFirst($ressection->schedule_id)->schedule_start.'-'.MSchedule::findFirst($ressection->schedule_id)->schedule_end.' '.MClassday::findFirst($ressection->classDay_id)->classDay_name;

                    array_push($enrollmentArray,$sectionModel );
                }
            }
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $enrollmentArray
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/studentsBySection/{section_id}/{period}/{year}',function ($section_id,$period,$year) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $phql = 'SELECT 
                        section_id,
                        person_id,
                        section_date,
                        class_id,
                        classroom_id,
                        schedule_id,
                        period_id,
                        person_id,
                        classDay_id,
                        YEAR(section_date) as section_year 
                FROM MSection 
                WHERE YEAR(section_date) = :year:
                AND section_id = :section_id:
                AND period_id = :period:';

            $enrollmentNow = $app->modelsManager->executeQuery($phql, array(
                        'year'           => (int) $year,
                        'section_id'      => (int) $section_id,
                        'period'         => (int) $period
                     ));
            
            $sectionData = array();
            if(COUNT($enrollmentNow) > 0){
                foreach( $enrollmentNow as $ressection ){
                    $sectionModel = new MSectionModel();
                    
                    $sectionModel->section_id = $ressection->section_id;
                    $sectionModel->section_date = $ressection->section_date;
                    $sectionModel->schedule_id = $ressection->schedule_id;
                    $sectionModel->schedule_start = MSchedule::findFirst($ressection->schedule_id)->schedule_start;
                    $sectionModel->schedule_end = MSchedule::findFirst($ressection->schedule_id)->schedule_end;
                    $sectionModel->class_id = $ressection->class_id;
                    $sectionModel->class = MClass::findFirst($ressection->class_id)->class_name;
                    $sectionModel->building_id = MClassroom::findFirst($ressection->classroom_id)->building_id;
                    $sectionModel->building = MBuilding::findFirst(MClassroom::findFirst($ressection->classroom_id)->building_id)->building_name;
                    $sectionModel->classDesciption = MClass::findFirst($ressection->class_id)->class_description;
                    $sectionModel->classroom_id = $ressection->classroom_id;
                    $sectionModel->classroom = MClassroom::findFirst($ressection->classroom_id)->classroom_name;
                    $sectionModel->period_id = $ressection->period_id;
                    $sectionModel->period = MPeriod::findFirst($ressection->period_id)->period_name;
                    $sectionModel->person_id = $ressection->person_id;
                    $sectionModel->person = MPerson::findFirst($ressection->person_id)->person_name;
                    $sectionModel->classDay_id = $ressection->classDay_id;
                    $sectionModel->classDay = MClassday::findFirst($ressection->classDay_id)->classDay_name;
                    $sectionModel->sectionDescription = MClass::findFirst($ressection->class_id)->class_name.' '.MSchedule::findFirst($ressection->schedule_id)->schedule_start.'-'.MSchedule::findFirst($ressection->schedule_id)->schedule_end.' '.MClassday::findFirst($ressection->classDay_id)->classDay_name;
                    $sectionModel->mSectionDescription = MClass::findFirst($ressection->class_id)->class_name.' '.MPerson::findFirst($ressection->person_id)->person_name.' '.MSchedule::findFirst($ressection->schedule_id)->schedule_start.'-'.MSchedule::findFirst($ressection->schedule_id)->schedule_end.' '.MClassday::findFirst($ressection->classDay_id)->classDay_name;

                    array_push($sectionData,$sectionModel );
                }
            }

            $phql2 = 'SELECT 
                        A.enrolment_id,
                        A.person_id,
                        A.section_id,
                        A.enrolment_date,
                        YEAR(A.enrolment_date) as enrolment_year ,
                        C.historic_qualification,
                        C.historic_observation
                FROM MEnrolment A
                LEFT JOIN MSection B
                ON(A.section_id = B.section_id)
                LEFT JOIN MHistoric C
                ON(A.enrolment_id = C.enrolment_id)
                WHERE YEAR(A.enrolment_date) = :year:
                AND A.section_id = :section_id:
                AND B.period_id = :period:';

            $students = $app->modelsManager->executeQuery($phql2, array(
                        'year'           => (int) $year,
                        'section_id'      => (int) $section_id,
                        'period'         => (int) $period
                     ));

            $enrollmentArray = array();
            if(COUNT($students) > 0){
                foreach( $students as $ressection ){

                    if(MPaymentbyenrolment::find(["enrolment_id = '$ressection->enrolment_id'"])[0]->paymentByEnrolment_state == 0){
                        $stateName = 'Pendiente de pago';
                    }else if(MPaymentbyenrolment::find(["enrolment_id = '$ressection->enrolment_id'"])[0]->paymentByEnrolment_state == 1){
                        $stateName = 'Cancelado';
                    }else if(MPaymentbyenrolment::find(["enrolment_id = '$ressection->enrolment_id'"])[0]->paymentByEnrolment_state == 2){
                        $stateName = 'Becado';
                    }
                    $currentEnrolment = array(
                        "enrolment_id" => $ressection->enrolment_id,
                        "person_name" => MPerson::findFirst($ressection->person_id)->person_name,
                        "person_email" => MPerson::findFirst($ressection->person_id)->person_email,
                        "person_celphone" => MPerson::findFirst($ressection->person_id)->person_celphone,
                        "payment" => $stateName,
                        "historic_qualification" => $ressection->historic_qualification,
                        "historic_observation" => $ressection->historic_observation
                    );

                    array_push($enrollmentArray,$currentEnrolment );
                }
            }
        
        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => array(
                    "sectionData" => $sectionData[0],
                    "studentList" => $enrollmentArray
                )
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->put('/studentsBySectionQualification',function () use($app){
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();

        if(COUNT($request_data)>0){
            


            
                $manager = new TxManager();
                $transaction = $manager->get();

                foreach($request_data as $res){
                    if($res->historic_qualification > 65){
                        $qualification = 'APR';
                    }
                    elseif($res->historic_qualification == 0){
                        $qualification = 'ABN';
                    }
                    else{
                        $qualification = 'RPB';
                    }
                    $historic = MHistoric::find(["enrolment_id = '$res->enrolment_id'"]);
                    $newHistoric = new MHistoric();
                    $newHistoric->setTransaction($transaction);
                    $newHistoric->historic_id = $historic[0]->historic_id;
                    $newHistoric->historic_qualification = $res->historic_qualification;
                    $newHistoric->historic_observation = $qualification;
                    $newHistoric->historic_date = $historic[0]->historic_date;
                    $newHistoric->enrolment_id = $res->enrolment_id;

                    $isHistoricCommit = $newHistoric->save();
                    
                    if ($isHistoricCommit === false) {
                        $statusCode = http_response_code(406);
                        $transaction->rollback(
                            "No se pudo realizar el registro, fallo la acción"
                        );
                    }
                    elseif($isHistoricCommit === true){
                        $cMessage = 'Estoy dento de la validacion';
                        if($qualification == 'APR'){
                            $cMessage = 'El estudiante aprobo';
                            $currectEnrollment = MEnrolment::findFirst($res->enrolment_id);
                            #Traer los datos de la persona
                            $person_data = MPerson::findFirst($currectEnrollment->person_id);
                            $person_id=$person_data->person_id;
                            $person_name=$person_data->person_name;
                            $person_identity=$person_data->person_identity;
                            $person_celphone=$person_data->person_celphone;
                            $person_phone=$person_data->person_phone;
                            $person_email=$person_data->person_email;
                            $person_birthdate=$person_data->person_birthdate;
                            $course_id=$person_data->course_id;
                            $person_direction=$person_data->person_direction;
                            $person_genus=$person_data->person_genus;
                            $person_image=$person_data->person_image;
                            $profile_id=$person_data->profile_id;
                            $currentSection = MSection::findFirst($currectEnrollment->section_id);
                            #Traer los datos de la clase
                            $currentClass = MClass::findFirst($currentSection->class_id);
                            $cClassByCourses = MClassebycourse::find(["class_id = '$currentClass->class_id'"]);

                            $phql1 = 'SELECT 
                                            COUNT(*) AS totalClass
                                    FROM MClassebycourse
                                    WHERE course_id = :couserId:';

                            $contClass = $app->modelsManager->executeQuery($phql1, array(
                                        'couserId'      => $course_id
                                    ));
                            
                            $phql2 = 'SELECT 
                                            COUNT(DISTINCT B.class_id) AS approvalClass
                                        FROM MClassebycourse A
                                        LEFT JOIN MClass B
                                        ON(A.class_id = B.class_id)
                                        LEFT JOIN MSection C
                                        ON(B.class_id = C.class_id)
                                        LEFT JOIN MEnrolment D
                                        ON(C.section_id = D.section_id)
                                        LEFT JOIN MHistoric E
                                        ON(D.enrolment_id = E.enrolment_id)
                                        WHERE A.course_id = :course:
                                        AND D.person_id = :person:
                                        AND E.historic_observation = :observation:';

                            $contApprovalClass = $app->modelsManager->executeQuery($phql2, array(
                                'observation'      => 'APR',
                                'course' => $course_id,
                                'person' => $person_id
                            ));

                            #Validacion para Pasar al proximo nivel
                            if($course_id == $cClassByCourses[0]->course_id ){
                                if($contApprovalClass[0]->approvalClass == $contClass[0]->totalClass)
                                    $course_id += 1;
                                $cMessage ='Hizo el intento de aumentar el curso';
                            }

                            if($course_id == 1)
                                $course_id += 1;
                                
                            $person = new MPerson();
                            $person->setTransaction($transaction);
                            $person->person_id = $person_id;
                            $person->person_name=$person_name;
                            $person->person_identity=$person_identity;
                            $person->person_celphone=$person_celphone;
                            $person->person_phone=$person_phone;
                            $person->person_email=$person_email;
                            $person->person_birthdate=$person_birthdate;
                            $person->course_id=$course_id;
                            $person->person_direction=$person_direction;
                            $person->person_genus=$person_genus;
                            $person->person_image=$person_image;

                            $isCommitPerson = $person->save();
                            
                            if ($isCommitPerson === false) {
                                $transaction->rollback(
                                    "No se pudo realizar el registro, fallo insert en persona"
                                );
                                $cMessage = 'Definitivamente revento';
                            }
                        };
                    }
                }
                

                $transaction->commit();
                $statusCode = http_response_code();
                if($statusCode != 200){
                    $response->setJsonContent(
                        array(
                            'status' => http_response_code(),
                            'message' => 'No se pudo realizar el registro',
                            'data' => ''
                        )
                    );
                }
                elseif($statusCode == 200){
                    $response->setJsonContent(
                        array(
                            'status' => http_response_code(),
                            'message' => 'success',
                            'data' => null
                        )
                    );
                }
            
        }
        else{
            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'La lista debe contener al menos un estudiante',
                    'data' => ''
                )
            );
        }
        
        
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/schedules',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allShedules = MSchedule::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allShedules
            )
        );
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/classDays',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allDays = MClassday::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allDays
            )
        );
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/buildings',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allBuilding = MBuilding::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allBuilding
            )
        );
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->post('/buildings', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $buildingExists = MBuilding::find(["building_name='$request_data->building_name'"]);

        if(COUNT($buildingExists)==0){

            $manager = new TxManager();
            $transaction = $manager->get();

            $MBuilding = new MBuilding();
            $MBuilding->setTransaction($transaction);
            
            $MBuilding->building_name= $request_data->building_name;
            $MBuilding->building_description= $request_data->building_description;
            $MBuilding->regionalCenter_id= $request_data->regionalCenter_id;

            $isCommit = $MBuilding->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
            }
            elseif($isCommit === true){
                $transaction->commit();
                http_response_code(200);
                $message = 'success';
            }
            

            
            
        }
        else{
            http_response_code(409);
            $message = 'No se puede registrar, este nombre de edificio ya existe';
        }
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MBuilding
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/buildings', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $buildingExists = MBuilding::findFirst($request_data->building_id);

        if($buildingExists){

            $manager = new TxManager();
            $transaction = $manager->get();

            $MBuilding = new MBuilding();
            $MBuilding->setTransaction($transaction);
            $MBuilding->building_id= $request_data->building_id;
            $MBuilding->building_name= $request_data->building_name;
            $MBuilding->building_description= $request_data->building_description;
            $MBuilding->regionalCenter_id= $request_data->regionalCenter_id;

            $isCommit = $MBuilding->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
            }
            elseif($isCommit === true){
                $transaction->commit();
                http_response_code(200);
                $message = 'success';
            }
            

            
            
        }
        else{
            http_response_code(409);
            $message = 'No se puede modificar, este edificio no existe';
        }
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MSection
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/classRooms/{building_id}',function ($building_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allClassRooms = MClassroom::find(
                                            [
                                                "building_id = '$building_id'"
                                            ]
                                          );
        if(COUNT($allClassRooms)>0){

            $classrooms = array();
            foreach($allClassRooms as $resclassrooms){
                $rooms = MClassroom::findFirst($resclassrooms->classroom_id);
                array_push($classrooms,$rooms);
            }
            http_response_code(200);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'success',
                    'data' => $classrooms
                )
            );
            
        }
        else{
            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'No hay aulas disponibles para este edificio',
                    'data' => null
                )
            );
        }
        
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/classRooms',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allClassRooms = MClassroom::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allClassRooms
            )
        );
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->post('/classRooms', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();

        $manager = new TxManager();
        $transaction = $manager->get();

        $MClassrooms = new MClassroom();
        $MClassrooms->setTransaction($transaction);
        
        $MClassrooms->classroom_name= $request_data->classroom_name;
        $MClassrooms->classroom_description= $request_data->classroom_description;
        $MClassrooms->building_id= $request_data->building_id;

        $isCommit = $MClassrooms->save();
        if ($isCommit === false) {
            http_response_code(406);
            $transaction->rollback(
                "No se pudo realizar el registro, fallo la transacción"
            );
        }
        elseif($isCommit === true){
            $transaction->commit();
            http_response_code(200);
            $message = 'success';
        }
        

          
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MClassrooms
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/classRooms', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        

        $manager = new TxManager();
        $transaction = $manager->get();

        $MClassrooms = new MClassroom();
        $MClassrooms->setTransaction($transaction);
        
        $MClassrooms->classroom_id= $request_data->classroom_id;
        $MClassrooms->classroom_name= $request_data->classroom_name;
        $MClassrooms->classroom_description= $request_data->classroom_description;
        $MClassrooms->building_id= $request_data->building_id;

        $isCommit = $MClassrooms->save();
        if ($isCommit === false) {
            http_response_code(406);
            $transaction->rollback(
                "No se pudo realizar el registro, fallo la transacción"
            );
        }
        elseif($isCommit === true){
            $transaction->commit();
            http_response_code(200);
            $message = 'success';
        }
        

        
        
    
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MClassrooms
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/periods',function ($course_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $allPeriods = MPeriod::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allPeriods
            )
        );
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->post('/sections', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $personExists = MPerson::findFirst($request_data->person_id);

        if($personExists){

            $manager = new TxManager();
            $transaction = $manager->get();

            $MSection = new MSection();
            $MSection->setTransaction($transaction);
            $now = date("Y-m-d H:i:s");
            $MSection->section_date= $now;
            $MSection->schedule_id= $request_data->schedule_id;
            $MSection->person_id= $request_data->person_id;
            $MSection->classroom_id=$request_data->classroom_id;
            $MSection->class_id= $request_data->class_id;
            $MSection->classDay_id= $request_data->classDay_id;
            $MSection->period_id= $request_data->period_id;

            $isCommit = $MSection->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
            }
            elseif($isCommit === true){
                $transaction->commit();
                http_response_code(200);
                $message = 'success';
            }
            

            
            
        }
        else{
            http_response_code(409);
            $message = 'La persona que desea asignar no esta registrada';
        }
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MSection
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/sections', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $personExists = MPerson::findFirst($request_data->person_id);

        if($personExists){

            $manager = new TxManager();
            $transaction = $manager->get();

            $MSection = new MSection();
            $MSection->setTransaction($transaction);
            $MSection->section_id= $request_data->section_id;
            $now = date("Y-m-d H:i:s");
            $MSection->section_date= $now;
            $MSection->schedule_id= $request_data->schedule_id;
            $MSection->person_id= $request_data->person_id;
            $MSection->classroom_id=$request_data->classroom_id;
            $MSection->class_id= $request_data->class_id;
            $MSection->classDay_id= $request_data->classDay_id;
            $MSection->period_id= $request_data->period_id;

            $isCommit = $MSection->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
            }
            elseif($isCommit === true){
                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
            

            
            
        }
        else{
            http_response_code(409);
            $message = 'La persona que desea asignar no esta registrada';
        }
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MSection
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->delete('/sections/{section_id}',function ($section_id) use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $manager = new TxManager();
        $transaction = $manager->get();
        
        $currentSection = MSection::findFirst($section_id);
        $enrollmentBySection = MEnrolment::find(["section_id = '$section_id'"]);
        if(COUNT($currentSection)>0 && COUNT($enrollmentBySection)==0){

            $sectionNow = new MSection();
            $sectionNow->setTransaction($transaction);
            $sectionNow->section_id = $currentSection->section_id;
            $sectionNow->person_id = $currentSection->person_id;
            $sectionNow->class_id = $currentSection->class_id;
            $sectionNow->period_id = $currentSection->period_id;
            $sectionNow->section_date = $currentSection->section_date;
            $sectionNow->schedule_id = $currentSection->schedule_id;
            $sectionNow->classroom_id = $currentSection->classroom_id;
            $sectionNow->classDay_id = $currentSection->classDay_id;

            $isCommit = $sectionNow->delete();

            if($isCommit === false){
                http_response_code(406);
                $errorEnrollment = true;
                $message = "Error para eliminar sección";
                $messages = $sectionNow->getMessages();
                foreach ($messages as $message) {
                    $transaction->rollback(
                        $message->getMessage()
                    );
                }
            }
            else{
                http_response_code(200);
                $errorEnrollment = false;
                $message = "success";
                
            }
        }
        else if(COUNT($currentSection)<=0){
            http_response_code(406);
            $errorEnrollment = true;
            $message = "Esta seccion ya fue cancelada";
        }
        else if(COUNT($enrollmentBySection)>0){
            http_response_code(406);
            $errorEnrollment = true;
            $message = "No se puede cancelar, aun hay estudiantes matriculados en esta sección";
        }
        $httpStatus = http_response_code();
        if($httpStatus == 200){
            $message = "success";
            $transaction->commit();
        }
        
        
        $response->setJsonContent(
           array(
                'status' => $httpStatus,
                'message' => $message,
                'data' => null
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->post('/modules', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $modulesExists = MCourse::find(["course_name='$request_data->course_name'"]);

        if(COUNT($modulesExists)==0){

            $manager = new TxManager();
            $transaction = $manager->get();

            $MCourse = new MCourse();
            $MCourse->setTransaction($transaction);
            
            $MCourse->course_name= $request_data->course_name;
            $MCourse->course_description= $request_data->course_description;

            $isCommit = $MCourse->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
            }
            elseif($isCommit === true){
                $transaction->commit();
                http_response_code(200);
                $message = 'success';
            }
            

            
            
        }
        else{
            http_response_code(409);
            $message = 'No se puede registrar, este elemento ya existe';
        }
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MCourse
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/modules', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        $modulesExists = MCourse::findFirst($request_data->course_id);

        if(COUNT($modulesExists)>0){

            $manager = new TxManager();
            $transaction = $manager->get();

            $MCourse = new MCourse();
            $MCourse->setTransaction($transaction);
            
            $MCourse->course_id= $request_data->course_id;
            $MCourse->course_name= $request_data->course_name;
            $MCourse->course_description= $request_data->course_description;

            $isCommit = $MCourse->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
            }
            elseif($isCommit === true){
                $transaction->commit();
                http_response_code(200);
                $message = 'success';
            }
            

            
            
        }
        else{
            http_response_code(409);
            $message = 'No se puede actualizar, este elemento no existe';
        }
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MCourse
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->post('/lessons', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();

        $manager = new TxManager();
        $transaction = $manager->get();

        $currentClass = MClass::find(["class_name = '$request_data->name'"]);
        $moduleExist = MCourse::findFirst($request_data->optionElement);
        if(COUNT($moduleExist)>0 && COUNT($currentClass)==0){
            $MClass = new MClass();
            $MClass->setTransaction($transaction);
            
            $MClass->class_name= $request_data->name;
            $MClass->class_description= $request_data->description;
            

            $isCommit = $MClass->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $MClassebycourse = new MClassebycourse();
                $MClassebycourse->setTransaction($transaction);
            
                $MClassebycourse->class_id= $MClass->class_id;
                $MClassebycourse->course_id= $request_data->optionElement;

                $isCommitClassByCourse = $MClassebycourse->save();
                if($isCommitClassByCourse === false){
                    http_response_code(406);
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo la transacción"
                    );
                    $message = 'No se pudo realizar el registro, fallo la transacción';
                }else if($isCommitClassByCourse === true){
                    $transaction->commit();
                    http_response_code(200);
                    $message = 'success';
                }
                
            }
        }
        else if(COUNT($moduleExist)<=0){
            http_response_code(406);
            $message = 'El modulo no existe';
        }else if(COUNT($currentClass)>0){
            http_response_code(406);
            $message = 'La clase ya existe';
        }

          
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/lessons', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        

        $manager = new TxManager();
        $transaction = $manager->get();

        $currentClass = MClass::findFirst($request_data->id);
        $moduleExist = MCourse::findFirst($request_data->optionElement);
        if(COUNT($moduleExist)>0 && COUNT($currentClass)>0){
            $MClass = new MClass();
            $MClass->setTransaction($transaction);
            
            $MClass->class_id= $request_data->id;
            $MClass->class_name= $request_data->name;
            $MClass->class_description= $request_data->description;
            

            $isCommit = $MClass->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
        }
        else if(COUNT($moduleExist)<=0){
            http_response_code(406);
            $message = 'El modulo no existe';
        }else if(COUNT($currentClass)>0){
            http_response_code(406);
            $message = 'La clase no existe';
        }
        

        
        
    
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MClassrooms
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->post('/scheduls', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();

        $manager = new TxManager();
        $transaction = $manager->get();
        
        $current = $request_data->name.' - '.$request_data->description;
        $currentElement = MSchedule::find(["schedule_name = '$current'"]);
        
        if(COUNT($currentElement)==0){
            $MSchedule = new MSchedule();
            $MSchedule->setTransaction($transaction);
            
            $MSchedule->schedule_name= $current;
            $MSchedule->schedule_description= $current;
            $MSchedule->schedule_start= $request_data->name;
            $MSchedule->schedule_end= $request_data->description;
            

            $isCommit = $MSchedule->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
        }else if(COUNT($currentElement)>0){
            http_response_code(406);
            $message = 'El horario ya existe';
        }

          
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/scheduls', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        

        $manager = new TxManager();
        $transaction = $manager->get();

        $current = $request_data->name.' - '.$request_data->description;
        $currentElement = MSchedule::findFirst($request_data->id);
        
        if(COUNT($currentElement)>0){
            $MSchedule = new MSchedule();
            $MSchedule->setTransaction($transaction);
            
            $MSchedule->schedule_id= $request_data->id;
            $MSchedule->schedule_name= $current;
            $MSchedule->schedule_description= $current;
            $MSchedule->schedule_start= $request_data->name;
            $MSchedule->schedule_end= $request_data->description;
            

            $isCommit = $MSchedule->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
        }else if(COUNT($currentElement)>0){
            http_response_code(406);
            $message = 'El horario no existe';
        }
        
        
    
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->post('/classdays', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();

        $manager = new TxManager();
        $transaction = $manager->get();
        
        
        $currentElement = MClassday::find(["classDay_name = '$request_data->name'"]);
        
        if(COUNT($currentElement)==0){
            $MClassday = new MClassday();
            $MClassday->setTransaction($transaction);
            
            $MClassday->classDay_name= $request_data->name;
            $MClassday->classDay_description= $request_data->description;
            

            $isCommit = $MClassday->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
        }else if(COUNT($currentElement)>0){
            http_response_code(406);
            $message = 'Este rango de dias ya existe';
        }

          
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/classdays', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        

        $manager = new TxManager();
        $transaction = $manager->get();

        
        $currentElement = MClassday::findFirst($request_data->id);
        
        if(COUNT($currentElement)>0){
            $MClassday = new MClassday();
            $MClassday->setTransaction($transaction);
            $MClassday->classDay_id= $request_data->id;
            $MClassday->classDay_name= $request_data->name;
            $MClassday->classDay_description= $request_data->description;
            

            $isCommit = $MClassday->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
        }else if(COUNT($currentElement)>0){
            http_response_code(406);
            $message = 'Este rango de dias no existe';
        }
        
        
    
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->post('/parameters', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();

        $manager = new TxManager();
        $transaction = $manager->get();
        
        
        $currentElement = MParameter::find(["parameter_name = '$request_data->name'"]);
        
        if(COUNT($currentElement)==0){
            $MParameter = new MParameter();
            $MParameter->setTransaction($transaction);
            
            $MParameter->parameter_name= $request_data->name;
            $MParameter->parameter_description= $request_data->description;
            $MParameter->parameter_value= $request_data->optionElement;

            $isCommit = $MParameter->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
        }else if(COUNT($currentElement)>0){
            http_response_code(406);
            $message = 'El parametro ya existe';
        }

          
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/parameters', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        

        $manager = new TxManager();
        $transaction = $manager->get();

        
        $currentElement = MParameter::findFirst($request_data->id);
        
        if(COUNT($currentElement)>0){
            $MParameter = new MParameter();
            $MParameter->setTransaction($transaction);
            
            $MParameter->parameter_id= $request_data->id;
            $MParameter->parameter_name= $request_data->name;
            $MParameter->parameter_description= $request_data->description;
            $MParameter->parameter_value= $request_data->optionElement;

            $isCommit = $MParameter->save();
            if ($isCommit === false) {
                http_response_code(406);
                $transaction->rollback(
                    "No se pudo realizar el registro, fallo la transacción"
                );
                $message = 'No se pudo realizar el registro, fallo la transacción';
            }
            elseif($isCommit === true){

                $transaction->commit();
                http_response_code(200);
                $message = 'success';
                
            }
        }else if(COUNT($currentElement)>0){
            http_response_code(406);
            $message = 'El parametro no existe';
        }
        
        
    
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/payments', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        

        $manager = new TxManager();
        $transaction = $manager->get();

        
        
        
        if(COUNT($request_data)>0){
            foreach($request_data as $res){
                $MPayment = MPaymentbyenrolment::find(["enrolment_id = '$res->enrolment_id'"]);
                $Paymentbyenrolment = new MPaymentbyenrolment();
                $Paymentbyenrolment->setTransaction($transaction);
                $now = date("Y-m-d H:i:s");
                $Paymentbyenrolment->paymentByEnrolment_id= $MPayment[0]->paymentByEnrolment_id;
                $Paymentbyenrolment->enrolment_id= $MPayment[0]->enrolment_id;
                $Paymentbyenrolment->person_id= $MPayment[0]->person_id;
                $Paymentbyenrolment->paymentByEnrolment_state= $res->payment - 1;
                $Paymentbyenrolment->paymentByEnrolment_date= $now;
                $Paymentbyenrolment->paymentConcept_id= $MPayment[0]->paymentConcept_id;

                $isCommit = $Paymentbyenrolment->save();
                if ($isCommit === false) {
                    http_response_code(406);
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo la transacción"
                    );
                    $message = 'No se pudo realizar el registro, fallo la transacción';
                }
                elseif($isCommit === true){

                    //$transaction->commit();
                    http_response_code(200);
                    $message = 'success';
                    
                }
            }
            
        }else if(COUNT($currentElement)>0){
            http_response_code(406);
            $message = 'No hay datos para actualizar';
        }
        
        if(http_response_code()==200)
            $transaction->commit();
    
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => null
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/test',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        $time_start = microtime(true); 
        $name = 'Esteban Miralda';
        $porciones = explode(" ", $name);

        $response->setJsonContent(
           array(
                'status' => 200,
                'message' => 'success',
                'data' => $hoy,
                'time'=>$porciones[0][0].$time_start
            )
        );
        
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->get('/oldPerson',function () use($app){
    $response = new Phalcon\Http\Response();
    #2334 codigo persona que debo tomar en cuenta despues, recordar que serian los menores a este numero descendientemente limit 500 por nivel
    try{
        $rol = 2;
        $strIdentidad=13;
        //$oldPersons = TblPersonas::find(["CODIGO_ROL = 2"]);
        /*$oldPersons = TblPersonas::query()
                ->where("CODIGO_ROL = :codigorol:")
                ->andWhere("CHAR_LENGTH(NUMERO_IDENTIDAD) = :numeroidentidad:")
                ->bind(array("codigorol" => $rol,
                             "numeroidentidad" => $strIdentidad
                             )
                    )
                ->execute();*/

        $phql = 'SELECT CODIGO_PERSONA, 
                        NOMBRE, 
                        APELLIDO, 
                        TEL_MOVIL, 
                        TEL_FIJO, 
                        CORREO_ELECTRONICO, 
                        NUMERO_IDENTIDAD, 
                        FECHA_NACIMIENTO, 
                        GENERO, 
                        CODIGO_LOCALIZACION, 
                        CODIGO_ROL, 
                        estado, 
                        CODIGO_NIVEL, 
                        profile 
                    FROM TblPersonas 
                    WHERE CODIGO_ROL = :rol:  
                    AND CHAR_LENGTH(NUMERO_IDENTIDAD) = :id: 
                    AND TEL_MOVIL <> :movil: 
                    AND CORREO_ELECTRONICO <> :mail: 
                    AND CODIGO_NIVEL = :nivel:
                    ORDER BY CODIGO_PERSONA DESC 
                    LIMIT 500';

        $oldPersons = $app->modelsManager->executeQuery($phql,array(
            'rol' => $rol,
            'id' => 13,
            'movil' => 0,
            'mail' => '',
            'nivel' => 0
        ));
        $oldPersonNewStructure = array();

        foreach($oldPersons as $oldPerson){
            $currentPerson = new MPerson();
            $currentPerson->person_name = $oldPerson->NOMBRE.' '.$oldPerson->APELLIDO;
            $currentPerson->person_email = $oldPerson->CORREO_ELECTRONICO;
            $currentPerson->person_phone = $oldPerson->TEL_FIJO;
            $currentPerson->person_celphone = $oldPerson->TEL_MOVIL;
            $currentPerson->person_identity = $oldPerson->NUMERO_IDENTIDAD;
            $currentPerson->person_birthdate = $oldPerson->FECHA_NACIMIENTO;
            $currentPerson->course_id = $oldPerson->CODIGO_NIVEL+1;
            $currentPerson->person_direction = 'Debes actualizar tu dirección';
            $currentPerson->person_image = 'resources/icons/profile/profile.png';
            $currentPerson->person_genus = 'M';
            $currentPerson->oldIdPerson = $oldPerson->CODIGO_PERSONA;

            // $cadena = $oldPerson->CORREO_ELECTRONICO;
            // $patrón = '(.*)@(.*)\.(.*)';
            // $sustitución = '';
            // $result = preg_replace($patrón, $sustitución, $cadena);
            if(eregi("^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9]+@[a-zA-Z0-9]+[a-zA-Z0-9-]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$", $oldPerson->CORREO_ELECTRONICO)){
                array_push($oldPersonNewStructure,$currentPerson);
            }
            
            
            
        }
        
        

        foreach($oldPersonNewStructure as $request_data){
            $person_name=$request_data->person_name;
            $person_identity=$request_data->person_identity;
            $person_celphone=$request_data->person_celphone;
            $person_phone=$request_data->person_phone;
            $person_email=$request_data->person_email;
            $person_birthdate=$request_data->person_birthdate;
            $course_id=$request_data->course_id;
            $person_direction=$request_data->person_direction;
            $person_genus=$request_data->person_genus;
            $person_image=$request_data->person_image;
            $oldIdPerson = $request_data->oldIdPerson;
            $profile_id=2;
            
           $personExists = MPerson::find(
                                            [
                                                "person_identity = '$person_identity'"
                                            ]
                                        );
            if(COUNT($personExists)==0){
                $lengthIdentity = strlen($person_identity);
                $pattern = '/\d/i';
                $substitution = '';
                $resultValidation = preg_replace($pattern, $substitution, $person_identity);


                 if($lengthIdentity == 13 && $resultValidation==''){
                    $manager = new TxManager();
                    $transaction = $manager->get();

                    $person = new MPerson();
                    $person->setTransaction($transaction);

                    $person->person_name=$person_name;
                    $person->person_identity=$person_identity;
                    $person->person_celphone=$person_celphone;
                    $person->person_phone=$person_phone;
                    $person->person_email=$person_email;
                    $person->person_birthdate=$person_birthdate;
                    $person->course_id=$course_id;
                    $person->person_direction=$person_direction;
                    $person->person_genus=$person_genus;
                    $person->person_image=$person_image;
                    $person->oldIdPerson = $oldIdPerson;
                    
                    $isCommit = $person->save();
                    
                    if ($isCommit === false) {
                        $transaction->rollback(
                            "No se pudo realizar el registro, fallo insert en persona"
                        );
                    }
                    elseif($isCommit===true){
                        #creacion de llave para la encriptacion
                        $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
                        
                        $time_start = microtime(true);
                        $tokenName = explode(" ", $person_name);
                        $now = date("Y-m-d H:i:s");
                        if(COUNT($tokenName) > 0){
                            #creacion de la llave compuesta por tres letras del username
                            $llave = substr($person_identity, 0,2).substr($person_identity, -1);
                            $llave = strtoupper($llave);
                            #encriptacion de la llave
                            $llave = encrypt($llave,$key);
                            #encriptacion del password como complemento del password total
                            $complemetName = substr($person_name, 0,2).substr($person_name, -1);
                            $completePass = $complemetName.''.$time_start;

                            $complement = encrypt($completePass,$key);
                            #concatenacion de la llave con el complemento para generar un password seguro
                            $password_encrypted = $llave.$complement;
                            #encriptando el password
                            $password_encrypted = encrypt($password_encrypted,$key);
                        }

                        $MUser = new MUser();
                        $MUser->setTransaction($transaction);

                        $MUser->user_name= $person->person_identity;
                        $MUser->user_password=$password_encrypted;
                        $MUser->user_log= 1;
                        $MUser->user_creationDate= $now;
                        $MUser->profile_id= $profile_id;
                        $MUser->changePassword = 1;

                        $isCommitUser = $MUser->save();
                        if ($isCommitUser === false) {
                            http_response_code(406);
                            $transaction->rollback(
                                "No se pudo realizar el registro, fallo inserten usuarios"
                            );
                        }
                        elseif($isCommitUser === true){
                            $userByPerson = new MUsersbyperson();
                            $userByPerson->setTransaction($transaction);

                            $userByPerson->user_id = $MUser->user_id;
                            $userByPerson->person_id = $person->person_id;

                            $isCommitUserByPerson = $userByPerson->save();

                            if ($isCommitUserByPerson === false) {
                                http_response_code(406);
                                $transaction->rollback(
                                    "No se pudo realizar el registro, fallo insert en usuarios por persona"
                                );

                            }
                            elseif($isCommitUserByPerson === true){
                                $message = '<b>'.$person_name.'</b>, bienvenido(a) a nuestro sistema. <br ><p><b>¡Su registro fue exitoso.!</b></p><br >Datos: <br >Usuario: '.$person_identity.'<br >Contrase&#241;a: '.$completePass;
                                $subject = 'Asociacón Hondureña de Ex-Becarios de Japón';
                                try{
                                    sendMailData($person_email,$message,$subject);
                                    http_response_code(200);
                                }catch(\Exception $e){
                                    http_response_code(406);
                                }
                                
                            }
                        }
                    }

                    $transaction->commit();
                    $statusCode = http_response_code();
                    if($statusCode != 200){
                        $response->setJsonContent(
                            array(
                                'status' => http_response_code(),
                                'message' => 'No se pudo realizar el registro',
                                'data' => ''
                            )
                        );
                    }
                    elseif($statusCode == 200){
                        $response->setJsonContent(
                            array(
                                'status' => http_response_code(),
                                'message' => 'success',
                                'data' => array(
                                    'data'=>$person
                                ) 
                            )
                        );
                    }
                }
                else{
                    http_response_code(400);
                    $response->setJsonContent(
                        array(
                            'status' => http_response_code(),
                            'message' => 'El numero de identidad no tiene el formato correcto',
                            'data' => ''
                        )
                    );
                }
            }
            else{
                http_response_code(409);
                $response->setJsonContent(
                    array(
                        'status' => http_response_code(),
                        'message' => 'Este usuario ya fue registrado',
                        'data' => ''
                    )
                );
            }
        }
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $oldPersons ,
                'result' => COUNT($oldPersonNewStructure)
            )
        );
        return $response;
        
    }catch(\Exception $e){
        $response->setJsonContent(
           array(
            'status' => 500,
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->post('/fileUpload', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        
        if(isset($_FILES["file"]))
        {
            
            {
                if($_FILES["file"]["type"]=="image/jpeg" || $_FILES["file"]["type"]=="image/pjpeg" || $_FILES["file"]["type"]=="image/gif" || $_FILES["file"]["type"]=="image/bmp" || $_FILES["file"]["type"]=="image/png"){
                    $info=getimagesize($_FILES["file"]["tmp_name"]);
                    
                    $size = $_FILES["file"]["size"];
                    $width =  $info[0];
                    $height = $info[1];
                    $pathSave='';
                    $parameters = MParameter::find();
                    if(($size < $parameters[8]->parameter_value && $size > $parameters[7]->parameter_value) && ($width>$parameters[9]->parameter_value && $width< $parameters[10]->parameter_value) && ($height> $parameters[11]->parameter_value && $height < $parameters[12]->parameter_value) ){
                        if(!is_dir("../../resources/profiles/"))
                            mkdir("../../resources/profiles/", 0777);
                        $path = '../../resources/profiles/';
                        $file = $_FILES["file"];
                        $pattern = '/[^\w\d/\.]/';
                        $sustitucion = '';
                        if(is_uploaded_file($file['tmp_name']))
                        {
                            $time_start = microtime(true);
                            
                            $extension = end(explode(".", $_FILES['file']['name']));
                            $pth = "profile_".$time_start.".".$extension;
                            $path  = $path."".$pth;
                            
                                if(move_uploaded_file($file['tmp_name'], $path))
                                {
                                    $pathSave = 'resources/profiles/'.$pth;
                                    http_response_code(200);
                                    $message = 'success';
                                }
                                else
                                {
                                    http_response_code(406);
                                    $message = "Error al intentar almacenar el archivo.";
                                }
                        }
                        else
                        {
                            http_response_code(406);
                            $message = "No se pudo cargar la imagen, vuelva a intentar";
                        }
                    }elseif($size < $parameters[9]->parameter_value && $size > $parameters[8]->parameter_value){
                        http_response_code(406);
                        $message = "La imagen es demasiado pesada, no debe exceder los ".$parameters[8]->parameter_value." bytes";
                        
                    }
                    else{
                        http_response_code(406);
                        $message = "La imagen no cumple con las dimensiones, altura maxima: ".$parameters[12]->parameter_value.", anchura maxima: ".$parameters[10]->parameter_value;
                    }
                }
                else{
                    http_response_code(406);
                    $message = "El archivo que desea subir no es una imagen";   
                }
                
            }
        }

        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => array(
                    "path" => $pathSave,
                    'peso' =>$size." bytes",
                    'dimensiones' => $width.' X '.$height.' px'
                )
                
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});



$app->get('/grantees',function () use($app){
    $response = new Phalcon\Http\Response();
    try{
        
        //$allGrantees = MGrantees::find();
        

        $phql = 'SELECT 
                            A.grantees_id,
                            A.grantees_name,
                            A.grantees_identity,
                            A.grantees_email,
                            A.grantees_profession,
                            B.coursesReceived_nroprogram,
                            B.coursesReceived_name,
                            A.grantees_jicacenter,
                            A.grantees_teammanager,
                            A.grantees_startingdate,
                            A.grantees_finishingdate,
                            A.grantees_numofperson,
                            A.grantees_institution,
                            A.grantees_celphone,
                            A.grantees_telephone,
                            A.department_id,
                            A.city_id,
                            A.grantees_creationDate,
                            A.payment,
                            A.language_id,
                            A.grantees_occupation,
                            A.grantees_birthdate,
                            A.otherLanguages
                     FROM MGrantees A
                     LEFT JOIN GCoursesreceived B
                     ON(A.grantees_id = B.person_id)';

            $allGrantees = $app->modelsManager->executeQuery($phql);

        $allGGrantees = array();

    $datos  = COUNT($allGrantees);

        foreach($allGrantees as $res){
            $datos  = $allGrantees;
            $MCourse = new GGrantees();
            $MCourse->grantees_id  = $res->grantees_id ;
            $MCourse->grantees_name  = $res->grantees_name ;
            $MCourse->grantees_identity  = $res->grantees_identity ;
            $MCourse->grantees_email  = $res->grantees_email ;
            $MCourse->grantees_profession  = $res->grantees_profession ;
            $MCourse->grantees_program  = $res->coursesReceived_nroprogram ;
            $MCourse->grantees_coursetitle  = $res->coursesReceived_name ;
            $MCourse->grantees_jicacenter  = $res->grantees_jicacenter ;
            $MCourse->grantees_teammanager  = $res->grantees_teammanager ;
            $MCourse->grantees_startingdate  = $res->grantees_startingdate ;
            $MCourse->grantees_finishingdate  = $res->grantees_finishingdate ;
            $MCourse->grantees_numofperson  = $res->grantees_numofperson ;
            $MCourse->grantees_institution  = $res->grantees_institution ;
            $MCourse->grantees_celphone  = $res->grantees_celphone ;
            $MCourse->grantees_telephone  = $res->grantees_telephone ;
            $MCourse->department_id  = $res->department_id ;
            $MCourse->city_id  = $res->city_id ;
            $MCourse->grantees_creationDate  = $res->grantees_creationDate ;
            $MCourse->payment  = $res->payment ;
            $MCourse->language_id  = $res->language_id ;
            $MCourse->grantees_occupation  = $res->grantees_occupation ;
            $MCourse->grantees_birthdate  = $res->grantees_birthdate ;
            $MCourse->otherLanguages  = $res->otherLanguages ;
            $MCourse->courseReceived  = GCoursesreceived::find(
                                            [
                                                "person_id = '$res->grantees_id'"
                                            ]
                                        ); 

            array_push($allGGrantees, $MCourse);
        }
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allGGrantees
                
            )
        );
        return $response;
        
    }catch(\Exception $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e
           )
            
        );
    
        return $response;
    }
});

$app->post('/grantees', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();

        $manager = new TxManager();
        $transaction = $manager->get();

        $MCourse = new MGrantees();
        $MCourse->setTransaction($transaction);
        // $MCourse->grantees_id  = $request_data->grantees_id ;
        $MCourse->grantees_name  = $request_data->grantees_name ;
        $MCourse->grantees_identity  = $request_data->grantees_identity ;
        $MCourse->grantees_email  = $request_data->grantees_email ;
        $MCourse->grantees_profession  = $request_data->grantees_profession ;
        $MCourse->grantees_program  = $request_data->grantees_program ;
        $MCourse->grantees_coursetitle  = $request_data->grantees_coursetitle ;
        $MCourse->grantees_jicacenter  = $request_data->grantees_jicacenter ;
        $MCourse->grantees_teammanager  = $request_data->grantees_teammanager ;
        $MCourse->grantees_startingdate  = $request_data->grantees_startingdate ;
        $MCourse->grantees_finishingdate  = $request_data->grantees_finishingdate ;
        $MCourse->grantees_numofperson  = $request_data->grantees_numofperson ;
        $MCourse->grantees_institution  = $request_data->grantees_institution ;
        $MCourse->grantees_celphone  = $request_data->grantees_celphone ;
        $MCourse->grantees_telephone  = $request_data->grantees_telephone ;
        $MCourse->department_id  = $request_data->department_id ;
        $MCourse->city_id  = $request_data->city_id ;
        $MCourse->grantees_creationDate  = $request_data->grantees_creationDate ;
        $MCourse->payment  = $request_data->payment ;
        $MCourse->language_id  = $request_data->language_id ;
        $MCourse->grantees_occupation  = $request_data->grantees_occupation ;
        $MCourse->grantees_birthdate  = $request_data->grantees_birthdate ;
        $MCourse->otherLanguages  = $request_data->otherLanguages ;
        
        $isCommit = $MCourse->save();

        $isNotInsert = false;

        if ($isCommit === false) {
            http_response_code(406);
            $transaction->rollback(
                "No se pudo realizar el registro, fallo la transacción"
            );

            $message = 'No se pudo realizar el registro, fallo la transacción';
        }
        elseif($isCommit === true){
            $todoArray = $MCourse->grantees_id;
            foreach($request_data->courseReceived  as $repayment){
                $dataLoca = $repayment;
                $GCoursesreceived = new GCoursesreceived();

                $GCoursesreceived->setTransaction($transaction);

                $GCoursesreceived->courseType_id = $repayment->courseType_id;
                $GCoursesreceived->coursesReceived_name = $repayment->coursesReceived_name;
                $GCoursesreceived->coursesReceived_nroprogram = $repayment->coursesReceived_nroprogram;
                $GCoursesreceived->coursesReceived_teme = $repayment->coursesReceived_teme;
                $GCoursesreceived->coursesReceived_center = $repayment->coursesReceived_center;
                $GCoursesreceived->coursesReceived_participationAge = $repayment->coursesReceived_participationAge;
                $GCoursesreceived->person_id = $MCourse->grantees_id;

                $isCommit2 = $GCoursesreceived->save();

                if ($isCommit2 === false) {
                    http_response_code(406);
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo la transacción de GCOURSE"
                    );

                    $message = 'No se pudo realizar el registro, fallo la transacción de GCOURSE';
                    $isNotInsert = true;
                    break;
                }

            }

            $person_name=$request_data->grantees_name ;
            $person_identity=$request_data->grantees_identity ;
            $person_celphone=$request_data->grantees_celphone ;
            $person_phone=$request_data->grantees_telephone ;
            $person_email=$request_data->grantees_email ;
            $person_birthdate=$request_data->grantees_birthdate ;
            $course_id=1;
            $person_direction="N/A";
            $person_genus="N/A";
            $person_image="resources/icons/login/login.png";
            $profile_id="5";
            
            $personExists = MPerson::find(
                                            [
                                                "person_identity = $person_identity"
                                            ]
                                        );



        if(COUNT($personExists)==0){
            $lengthIdentity = strlen($person_identity);
            $pattern = '/\d/i';
            $substitution = '';
            $resultValidation = preg_replace($pattern, $substitution, $person_identity);

            $msjErr = "";
            if($lengthIdentity == 13 && $resultValidation==''){
                

                $person = new MPerson();
                $person->setTransaction($transaction);

                $person->person_name=$person_name;
                $person->person_identity=$person_identity;
                $person->person_celphone=$person_celphone;
                $person->person_phone=$person_phone;
                $person->person_email=$person_email;
                $person->person_birthdate=$person_birthdate;
                $person->course_id=$course_id;
                $person->person_direction=$person_direction;
                $person->person_genus=$person_genus;
                $person->person_image=$person_image;
                
                $isCommit3 = $person->save();

                if ($isCommit3 === false) {
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo insert en persona"
                    );
                    $msjErr = "No se pudo realizar el registro, fallo insert en persona";
                }
                elseif($isCommit3===true){
                    #creacion de llave para la encriptacion
                    $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
                    
                    $time_start = microtime(true);
                    $tokenName = explode(" ", $person_name);
                    $now = date("Y-m-d H:i:s");
                    if(COUNT($tokenName) > 0){
                        #creacion de la llave compuesta por tres letras del username
                        $llave = substr($person_identity, 0,2).substr($person_identity, -1);
                        $llave = strtoupper($llave);
                        #encriptacion de la llave
                        $llave = encrypt($llave,$key);
                        #encriptacion del password como complemento del password total
                        $complemetName = substr($person_name, 0,2).substr($person_name, -1);
                        $completePass = $complemetName.''.$time_start;

                        $complement = encrypt($completePass,$key);
                        #concatenacion de la llave con el complemento para generar un password seguro
                        $password_encrypted = $llave.$complement;
                        #encriptando el password
                        $password_encrypted = encrypt($password_encrypted,$key);
                    }
                    $msjErr = "La persona si se registro";
                    $MUser = new MUser();
                    $MUser->setTransaction($transaction);

                    $MUser->user_name= $person->person_identity;
                    $MUser->user_password=$password_encrypted;
                    $MUser->user_log= 1;
                    $MUser->user_creationDate= $now;
                    $MUser->profile_id= $profile_id;
                    $MUser->changePassword = 1;

                    $isCommitUser2 = $MUser->save();
                    if ($isCommitUser2 === false) {
                        http_response_code(406);
                        $transaction->rollback(
                            "No se pudo realizar el registro, fallo inserten usuarios"
                        );
                        $msjErr = "No se pudo realizar el registro, fallo inserten usuarios";
                    }
                    elseif($isCommitUser2 === true){
                        $userByPerson = new MUsersbyperson();
                        $userByPerson->setTransaction($transaction);
                        $msjErr = "El usuario si se registro";
                        $userByPerson->user_id = $MUser->user_id;
                        $userByPerson->person_id = $person->person_id;

                        $isCommitUserByPerson2 = $userByPerson->save();

                        if ($isCommitUserByPerson2 === false) {
                            http_response_code(406);
                            $transaction->rollback(
                                "No se pudo realizar el registro, fallo insert en usuarios por persona"
                            );
                            $msjErr = "No se pudo realizar el registro, fallo insert en usuarios por persona";
                        }
                        elseif($isCommitUserByPerson2 === true){

                            $msjErr = "Tambien se registro el usuario por persona";
                            $message = '<b>'.$person_name.'</b>, bienvenido(a) a nuestro sistema. <br ><p><b>¡Su registro fue exitoso.!</b></p><br >Datos: <br >Usuario: '.$person_identity.'<br >Contrase&#241;a: '.$completePass;
                            $subject = 'Registro AHBEJA';
                            try{
                                sendMailData($person_email,$message,$subject);
                                http_response_code(200);
                            }catch(\Exception $e){
                                $msjErr = "Lo que fallo fue el envio de correo.";
                                http_response_code(406);
                            }
                            
                        }
                    }
                }

                
            }
            else{
                http_response_code(400);
                $response->setJsonContent(
                    array(
                        'status' => http_response_code(),
                        'message' => 'El numero de identidad no tiene el formato correcto',
                        'data' => ''
                    )
                );
            }
        }
        else{
            http_response_code(409);
            $response->setJsonContent(
                array(
                    'status' => http_response_code(),
                    'message' => 'Este usuario ya fue registrado',
                    'data' => ''
                )
            );
        }
            
        }
        

            
        if(!$isNotInsert){
            $transaction->commit();
            http_response_code(200);
            $message = 'success';
        }
        
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MCourse
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->put('/grantees', function () use ($app) {
    try{
        $response = new Phalcon\Http\Response();
        $request_data = $app->request->getJsonRawBody();
        
        

        

        $manager = new TxManager();
        $transaction = $manager->get();

        $MCourse = new MGrantees();
        $MCourse->setTransaction($transaction);
        $MCourse->grantees_id  = $request_data->grantees_id ;
        $MCourse->grantees_name  = $request_data->grantees_name ;
        $MCourse->grantees_identity  = $request_data->grantees_identity ;
        $MCourse->grantees_email  = $request_data->grantees_email ;
        $MCourse->grantees_profession  = $request_data->grantees_profession ;
        $MCourse->grantees_program  = $request_data->grantees_program ;
        $MCourse->grantees_coursetitle  = $request_data->grantees_coursetitle ;
        $MCourse->grantees_jicacenter  = $request_data->grantees_jicacenter ;
        $MCourse->grantees_teammanager  = $request_data->grantees_teammanager ;
        $MCourse->grantees_startingdate  = $request_data->grantees_startingdate ;
        $MCourse->grantees_finishingdate  = $request_data->grantees_finishingdate ;
        $MCourse->grantees_numofperson  = $request_data->grantees_numofperson ;
        $MCourse->grantees_institution  = $request_data->grantees_institution ;
        $MCourse->grantees_celphone  = $request_data->grantees_celphone ;
        $MCourse->grantees_telephone  = $request_data->grantees_telephone ;
        $MCourse->department_id  = $request_data->department_id ;
        $MCourse->city_id  = $request_data->city_id ;
        $MCourse->grantees_creationDate  = $request_data->grantees_creationDate ;
        $MCourse->payment  = $request_data->payment ;
        $MCourse->language_id  = $request_data->language_id ;
        $MCourse->grantees_occupation  = $request_data->grantees_occupation ;
        $MCourse->grantees_birthdate  = $request_data->grantees_birthdate ;
        $MCourse->otherLanguages  = $request_data->otherLanguages ;
        
        $isCommit = $MCourse->save();

        $isNotInsert = false;

        if ($isCommit === false) {
            $isNotInsert = true;
            http_response_code(406);
            $transaction->rollback(
                "No se pudo realizar el registro, fallo la transacción"
            );

            $message = 'No se pudo realizar la actualización, fallo la transacción';
        }
        elseif($isCommit === true){

            $GCoursesreceived = GCoursesreceived::find(
                                        [
                                            "person_id = '$request_data->grantees_id'"
                                        ]
                                    );

            foreach($GCoursesreceived  as $resdell){

                $GCoursesreceivedDel = new GCoursesreceived();
                $GCoursesreceivedDel->setTransaction($transaction);

                $GCoursesreceivedDel->coursesReceived_id = $resdell->coursesReceived_id;
                $GCoursesreceivedDel->courseType_id = $resdell->courseType_id;
                $GCoursesreceivedDel->coursesReceived_name = $resdell->coursesReceived_name;
                $GCoursesreceived->coursesReceived_nroprogram = $repayment->coursesReceived_nroprogram;
                $GCoursesreceived->coursesReceived_teme = $repayment->coursesReceived_teme;
                $GCoursesreceived->coursesReceived_center = $repayment->coursesReceived_center;
                $GCoursesreceivedDel->coursesReceived_participationAge = $resdell->coursesReceived_participationAge;
                $GCoursesreceivedDel->person_id = $MCourse->grantees_id;

                $isDeleteCommit = $GCoursesreceivedDel->delete();

            }
                
            foreach($request_data->courseReceived  as $repayment){
                $dataLoca = $repayment;
                $GCoursesreceived = new GCoursesreceived();

                $GCoursesreceived->setTransaction($transaction);
                $GCoursesreceived->courseType_id = $repayment->courseType_id;
                $GCoursesreceived->coursesReceived_name = $repayment->coursesReceived_name;
                $GCoursesreceived->coursesReceived_nroprogram = $repayment->coursesReceived_nroprogram;
                $GCoursesreceived->coursesReceived_teme = $repayment->coursesReceived_teme;
                $GCoursesreceived->coursesReceived_center = $repayment->coursesReceived_center;
                $GCoursesreceived->coursesReceived_participationAge = $repayment->coursesReceived_participationAge;
                $GCoursesreceived->person_id = $MCourse->grantees_id;

                $isCommit2 = $GCoursesreceived->save();

                if ($isCommit2 === false) {
                    $isNotInsert = true;
                    http_response_code(406);
                    $transaction->rollback(
                        "No se pudo realizar el registro, fallo la transacción de GCOURSE"
                    );

                    $message = 'No se pudo realizar el registro, fallo la transacción de GCOURSE';
                    $isNotInsert = true;
                    break;
                }

            }

            // $transaction->commit();
            // http_response_code(200);
            // $message = 'success';
        }
        

        if(!$isNotInsert){
            $transaction->commit();
            http_response_code(200);
            $message = 'success';
        }
            
        
        
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => $message,
                'data' => $MCourse
            )
        );
        return $response;
        
    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/department', function () use ($app) {
    $response = new Phalcon\Http\Response();
    try{
        
        $allDepartment = MDepartment::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allDepartment
            )
        );
        return $response;

    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/languaje', function () use ($app) {
    $response = new Phalcon\Http\Response();
    try{
        
        $allGLanguage = GLanguage::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allGLanguage
            )
        );
        return $response;

    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});

$app->get('/gTypeCourse', function () use ($app) {
    $response = new Phalcon\Http\Response();
    try{
        
        $allGCoursetype = GCoursetype::find();
        
        http_response_code(200);
        $response->setJsonContent(
            array(
                'status' => http_response_code(),
                'message' => 'success',
                'data' => $allGCoursetype
            )
        );
        return $response;

    }catch(TxFailed $e){
        http_response_code(500);
        $response->setJsonContent(
           array(
            'status' => http_response_code(),
            'message' => 'Internal error services',
            'error' => $e->getMessage()
           )
            
        );
    
        return $response;
    }
});


/*-------------------------------definition of functions-----------------------------------------*/
//encrypt password of user
function encrypt($data, $key){
    return md5(
      mcrypt_encrypt(
          MCRYPT_RIJNDAEL_128,
          $key,
          $data,
          MCRYPT_MODE_CBC,
          "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
      )
    );
};

//validate token of user for petition
function validateToken($jwt){
  try {
    $key = "example_key";
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    return true;
  } catch (Exception $e) {
    return false;
  }
};

function sendMailData($destination,$msjs,$subject){
    
    $mail=new PHPMailer(true);
     //$mail->From="franjaciel@hotmail.com";
    $mail->From=$destination;
    $mail->IsHTML(true);
    $mail->Subject=$subject;
    $mail->Body=$msjs;
    $mail->AddAddress("$destination");//este es destino debe de estar en from tambien ara que se envie
    $mail->IsSMTP();
    $mail->Host="ssl://smtp.gmail.com:465";
    $mail->SMTPAuth=true;
    $mail->Username="registro.ahbeja@gmail.com";//correo de gmail
    $mail->Password="registro2017$";//clave del correo
    $mail->FromName = "Asociación Hondureña de Becarios de Japón(AHBEJA)";
    $mail->Send();
}

function encryptPass($username, $pass){
    #creacion de llave para la encriptacion
    $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
    #creacion de la llave compuesta por tres letras del username
    $llave = substr($username, 0,2).''.substr($username, -1);
    $llave = strtoupper($llave);
    #encriptacion de la llave
    $llave = encrypt($llave,$key);
    #encriptacion del password como complemento del password total
    $complement = encrypt($pass,$key);
    #concatenacion de la llave con el complemento para generar un password seguro
    $password_encrypted = $llave.$complement;
    #encriptando el password
    $password_encrypted = encrypt($password_encrypted,$key);

    return $password_encrypted;
}



// Mensaje de Error 
/*-----------------------------------------------------------------------------------------------*/
$app->notFound( function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'Ooops, URL no valida.';
});



$app->handle();

?>