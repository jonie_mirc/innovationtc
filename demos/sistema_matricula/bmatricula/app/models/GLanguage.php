<?php

class GLanguage extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $language_id;

    /**
     *
     * @var string
     * @Column(type="string", length=4, nullable=false)
     */
    public $language_abbreviation;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=false)
     */
    public $language_name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ahbeja_matricula");
        $this->setSource("g_language");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'g_language';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GLanguage[]|GLanguage|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GLanguage|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
