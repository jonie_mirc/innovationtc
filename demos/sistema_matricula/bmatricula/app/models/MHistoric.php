<?php

class MHistoric extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $historic_id;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $historic_qualification;

    /**
     *
     * @var string
     * @Column(type="string", length=3, nullable=false)
     */
    public $historic_observation;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $historic_date;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $enrolment_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_historic';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MHistoric[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MHistoric
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
