<?php

class MClassday extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $classDay_id;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=false)
     */
    public $classDay_name;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $classDay_description;

    

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_classday';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MClassday[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MClassday
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
