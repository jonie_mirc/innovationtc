<?php

class MBlacklist extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $blacklist_id;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=false)
     */
    public $blacklist_ip;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $blacklist_date;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_blacklist';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MBlacklist[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MBlacklist
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
