<?php

class MCourse extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $course_id;

    /**
     *
     * @var string
     * @Column(type="string", length=90, nullable=false)
     */
    public $course_name;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $course_description;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_course';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCourse[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCourse
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
