<?php

class TblPersonas extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $CODIGO_PERSONA;

    /**
     *
     * @var string
     */
    public $NOMBRE;

    /**
     *
     * @var string
     */
    public $APELLIDO;

    /**
     *
     * @var integer
     */
    public $TEL_MOVIL;

    /**
     *
     * @var integer
     */
    public $TEL_FIJO;

    /**
     *
     * @var string
     */
    public $CORREO_ELECTRONICO;

    /**
     *
     * @var string
     */
    public $NUMERO_IDENTIDAD;

    /**
     *
     * @var string
     */
    public $FECHA_NACIMIENTO;

    /**
     *
     * @var string
     */
    public $GENERO;

    /**
     *
     * @var integer
     */
    public $CODIGO_LOCALIZACION;

    /**
     *
     * @var integer
     */
    public $CODIGO_ROL;

    /**
     *
     * @var string
     */
    public $estado;

    /**
     *
     * @var integer
     */
    public $CODIGO_NIVEL;

    /**
     *
     * @var string
     */
    public $profile;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tbl_personas';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TblPersonas[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TblPersonas
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
