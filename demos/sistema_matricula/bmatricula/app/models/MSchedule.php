<?php

class MSchedule extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $schedule_id;

    /**
     *
     * @var string
     * @Column(type="string", length=60, nullable=false)
     */
    public $schedule_name;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $schedule_description;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=false)
     */
    public $schedule_start;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=false)
     */
    public $schedule_end;
    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_schedule';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MSchedule[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MSchedule
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
