<?php

class MPerson extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $person_id;

    /**
     *
     * @var string
     */
    public $person_name;

    /**
     *
     * @var string
     */
    public $person_email;

    /**
     *
     * @var string
     */
    public $person_phone;

    /**
     *
     * @var string
     */
    public $person_celphone;

    /**
     *
     * @var string
     */
    public $person_identity;

    /**
     *
     * @var string
     */
    public $person_birthdate;

    /**
     *
     * @var integer
     */
    public $course_id;

    /**
     *
     * @var string
     */
    public $person_direction;

    /**
     *
     * @var string
     */
    public $person_image;

    /**
     *
     * @var string
     */
    public $person_genus;

    public $oldIdPerson;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_person';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MPerson[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MPerson
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
