<?php

class GCoursesreceived extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $coursesReceived_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $courseType_id;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=false)
     */
    public $coursesReceived_name;

    /**
     *
     * @var string
     * @Column(type="string", length=40, nullable=true)
     */
    public $coursesReceived_field;

    /**
     *
     * @var string
     * @Column(type="string", length=40, nullable=true)
     */
    public $coursesReceived_subfield;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $coursesReceived_participationAge;

    /**
     *
     * @var highlight_string(   )
     * @Column(type="string", length=30, nullable=false)
     */
    public $coursesReceived_nroprogram;

    /**
     *
     * @var string
     * @Column(type="string", length=40, nullable=true)
     */
    public $coursesReceived_teme;

    /**
     *
     * @var string
     * @Column(type="string", length=40, nullable=true)
     */
    public $coursesReceived_center;



    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $person_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ahbeja_matricula");
        $this->setSource("g_coursesreceived");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'g_coursesreceived';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GCoursesreceived[]|GCoursesreceived|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GCoursesreceived|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
