<?php

class MClassroom extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $classroom_id;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=false)
     */
    public $classroom_name;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $classroom_description;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $building_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_classroom';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MClassroom[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MClassroom
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
