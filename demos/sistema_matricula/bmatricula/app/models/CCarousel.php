<?php

class CCarousel extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $carousel_id;

    /**
     *
     * @var string
     * @Column(type="string", length=90, nullable=false)
     */
    public $carousel_img_path;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $carousel_desciption;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $carousel_title;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $profile_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'c_carousel';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Mcarousel[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Mcarousel
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
