<?php

class MDatatoken extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $datatoken_id;
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $datatoken_expirationtime;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $datatoken_key;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $datatoken_creationtime;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_datatoken';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MDatatoken[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MDatatoken
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
