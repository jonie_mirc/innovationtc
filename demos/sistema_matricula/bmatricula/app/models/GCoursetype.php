<?php

class GCoursetype extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $courseType_id;

    /**
     *
     * @var string
     * @Column(type="string", length=40, nullable=false)
     */
    public $courseType_name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ahbeja_matricula");
        $this->setSource("g_courseType");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'g_courseType';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GCoursetype[]|GCoursetype|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GCoursetype|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
