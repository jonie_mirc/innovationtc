<?php

class MGrantees extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $grantees_id;

    /**
     *
     * @var string
     * @Column(type="string", length=80, nullable=false)
     */
    public $grantees_name;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=false)
     */
    public $grantees_identity;

    /**
     *
     * @var string
     * @Column(type="string", length=80, nullable=false)
     */
    public $grantees_email;

    /**
     *
     * @var string
     * @Column(type="string", length=120, nullable=false)
     */
    public $grantees_profession;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_program;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_coursetitle;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_jicacenter;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_teammanager;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $grantees_startingdate;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $grantees_finishingdate;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $grantees_numofperson;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_institution;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=false)
     */
    public $grantees_celphone;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_grantees';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MGrantees[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MGrantees
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
