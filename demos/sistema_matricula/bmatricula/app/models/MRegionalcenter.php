<?php

class MRegionalcenter extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $regionalCenter_id;

    /**
     *
     * @var string
     * @Column(type="string", length=90, nullable=false)
     */
    public $regionalCenter_name;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $regionalCenter_description;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $city_id;

    /**
     *
     * @var string
     * @Column(type="string", length=300, nullable=false)
     */
    public $regionalCenter_direction;

    /**
     *
     * @var string
     * @Column(type="string", length=26, nullable=false)
     */
    public $regionalCenter_phone;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_regionalcenter';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MRegionalcenter[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MRegionalcenter
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
