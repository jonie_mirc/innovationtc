<?php

class MParameter extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $parameter_id;

    /**
     *
     * @var string
     */
    public $parameter_name;

    /**
     *
     * @var string
     */
    public $parameter_value;

    /**
     *
     * @var string
     */
    public $parameter_description;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_parameter';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MParameter[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MParameter
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
