<?php

class MPaymentconcept extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $paymentConcept_id;

    /**
     *
     * @var string
     * @Column(type="string", length=90, nullable=false)
     */
    public $paymentConcept_name;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $paymentConcept_desciption;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $paymentConcept_amount;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_paymentconcept';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MPaymentconcept[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MPaymentconcept
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
