<?php

class MGrantees extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $grantees_id;

    /**
     *
     * @var string
     * @Column(type="string", length=80, nullable=false)
     */
    public $grantees_name;

    /**
     *
     * @var string
     * @Column(type="string", length=80, nullable=false)
     */
    public $grantees_email;

    /**
     *
     * @var string
     * @Column(type="string", length=120, nullable=true)
     */
    public $grantees_profession;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_program;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_coursetitle;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_jicacenter;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_teammanager;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $grantees_startingdate;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $grantees_finishingdate;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $grantees_numofperson;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $grantees_institution;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=false)
     */
    public $grantees_celphone;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $grantees_identity;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $grantees_creationDate;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $grantees_birthdate;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $city_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $department_id;

    /**
     *
     * @var string
     * @Column(type="string", length=8, nullable=true)
     */
    public $grantees_telephone;

    /**
     *
     * @var string
     * @Column(type="string", length=120, nullable=false)
     */
    public $grantees_occupation;

    public $payment;

    public $language_id;

    public $otherLanguages;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ahbeja_matricula");
        $this->setSource("m_grantees");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_grantees';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MGrantees[]|MGrantees|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MGrantees|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
