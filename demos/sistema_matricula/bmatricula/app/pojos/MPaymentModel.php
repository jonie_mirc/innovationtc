<?php
    class MPaymentModel{
        public $enrolment_id;
        public $section_id;
        public $class_name;
        public $class_description;
        public $period;
        public $paymentConcept_id;
        public $paymentConcept_amount;
        public $person_id;
        public $paymentByEnrolment_state;
        public $paymentByEnrolment_stateName;
        public $paymentByEnrolment_date;
        public $enrolment_year;
        
    }
?>