<?php
    class MHistoricModel{
        public $enrolment_id;
        public $section_id;
        public $class_name;
        public $class_description;
        public $period;
        public $historic_id;
        public $historic_qualification;
        public $historic_observation;
        public $historic_date;
        public $enrolment_year;
        
    }
?>