<?php

class MUserModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=false)
     */
    public $user_name;


    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_log;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $user_creationDate;

    
    public $profile;

    

}
?>