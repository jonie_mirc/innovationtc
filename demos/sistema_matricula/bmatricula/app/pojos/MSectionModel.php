<?php
    class MSectionModel{
    public $enrollment_id;
    public $section_id;
    public $section_date;
    public $schedule_id;
    public $schedule_start;
    public $schedule_end;
    public $class_id;
    public $class;
    public $classCode;
    public $classDesciption;
    public $classroom_id;
    public $classroom;
    public $building_id;
    public $building;
    public $period_id;
    public $period;
    public $person_id;
    public $student;
    public $person;
    public $classDay_id;
    public $classDay;
    public $course_id;
    public $sectionDescription;
    public $mSectionDescription;

    }
?>