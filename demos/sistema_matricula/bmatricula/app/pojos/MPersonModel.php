<?php

class MPersonModel
{

    /**
     *
     * @var integer
     */
    public $person_id;

    /**
     *
     * @var string
     */
    public $person_name;

    /**
     *
     * @var string
     */
    public $person_email;

    /**
     *
     * @var string
     */
    public $person_phone;

    /**
     *
     * @var string
     */
    public $person_celphone;

    /**
     *
     * @var string
     */
    public $person_identity;

    /**
     *
     * @var string
     */
    public $person_birthdate;

    /**
     *
     * @var integer
     */
    public $course_id;

    /**
     *
     * @var string
     */
    public $person_direction;

    /**
     *
     * @var string
     */
    public $person_image;

    /**
     *
     * @var string
     */
    public $person_genus;

    public $usersByPerson = array();  

}
?>