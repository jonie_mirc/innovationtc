import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './error/404.component';
import { P500Component } from './error/500.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import{CategoriasComponent} from './views/Administracion/Categorias/categorias';
import {TipoRecursosComponent} from './views/Mantenimientos/TipoRecursos/tiporecursos';
import {PerfilesComponent} from './views/Seguridad/Perfiles/perfiles';
import {CentrosComponent} from './views/Administracion/Centros/centros';
import {InformacionComponent} from './views/Administracion/Informacion/informacion';
import {UsuariosComponent} from './views/Seguridad/usuarios/users.component';
import {ArgoogleComponent} from './views/Administracion/Argoogle/Argoogle';
import {CategoryPlaceomponent} from './views/Mantenimientos/CategoriasLugares/categoriaslugares';
import {PaisesComponent} from './views/Mantenimientos/Paises/paises';
import {DeptosComponent} from './views/Mantenimientos/Departamentos/departamentos';
import {PerfilComponent} from './views/Administracion/Perfil/perfil';
import {TutorialesComponent} from './views/Administracion/Tutoriales/Tutoriales';
import {PersonsComponent} from './views/security/persons/persons.component';
import { SectionsComponent } from './views/Mantenimientos/sections/sections.component';
import { BuildingsComponent } from './views/Mantenimientos/buildings/buildings.component';
import { ClassroomsComponent } from './views/Mantenimientos/classrooms/classrooms.component';
import { ModulesComponent } from './views/Mantenimientos/modules/modules.component';
import { SchedulesComponent } from './views/Mantenimientos/schedules/schedules.component';
import { ClassDaysComponent } from './views/Mantenimientos/class-days/class-days.component';
import { ParametersComponent } from './views/Mantenimientos/parameters/parameters.component';
import { ClassComponent } from './views/Mantenimientos/class/class.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },

  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: ''
    },
    children: [
      {
        path: 'cambio-clave',
        loadChildren: './views/security/security.module#SecurityModule'
      },
      {
        path: 'inicio',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
    
    {
      path: 'administracion/categorias',
      component: CategoriasComponent,
      data: {
        title: 'administracion/categorias'
      }
      
    }, 
    {
      path:"administracion/geopocision",
      component:ArgoogleComponent,
      data: {
        title: 'administracion/geopocision'
      }
    },
    {
        path:"administracion/perfil",
        component:PerfilComponent,
        data:{
          title:"perfil"
        }
    },
    {
        path:'administracion/tutorial',
        component:TutorialesComponent,
        data:{
          title:'tutoriales'
        }
    },
    {
      path:'mantenimientos/tiporecursos',  
      component:TipoRecursosComponent,
      data: {
        title: 'mantenimientos/tiporecursos'
      }
    },
    {
      path:'mantenimientos/categoriaslugares',
      component:CategoryPlaceomponent,
      data:{
        title:'mantenimientos/categoriaslugares'
      }
    },
    {
      
      path:'mantenimientos/paises',
      component:PaisesComponent,
      data:{
        title:'mantenimientos/paises'
      }
      
    },
    {
      path:'mantenimientos/departamentos',
      component:DeptosComponent,
      data:{
        title:'mantenimientos/departamentos'
      }
      
    },
    {
      path:'mantenimiento/secciones',
      component:SectionsComponent,
      data:{
        title:'Mantenimiento/Secciones'
      }
    },
    {
      path:'mantenimiento/modulos',
      component:ModulesComponent,
      data:{
        title:'Mantenimiento/Modulos'
      }
    },
    {
      path:'mantenimiento/edificios',
      component:BuildingsComponent,
      data:{
        title:'Mantenimiento/Edificios'
      }
    },
    {
      path:'mantenimiento/aulas',
      component:ClassroomsComponent,
      data:{
        title:'Mantenimiento/Aulas'
      }
    },
    {
      path:'mantenimiento/horarios',
      component:SchedulesComponent,
      data:{
        title:'Mantenimiento/Horarios'
      }
    },
    {
      path:'mantenimiento/dias-clase',
      component:ClassDaysComponent,
      data:{
        title:'Mantenimiento/Dias clase'
      }
    },
    {
      path:'mantenimiento/parametros',
      component:ParametersComponent,
      data:{
        title:'Mantenimiento/Parametros'
      }
    },
    {
      path:'mantenimiento/clases',
      component:ClassComponent,
      data:{
        title:'Mantenimiento/Clases'
      }
    },

    {
      path:'seguridad/perfiles',
      component:PerfilesComponent,
      data: {
        title: 'seguridad/perfiles'
      }
    },
    {
      path:'seguridad/usuarios',
      component:UsuariosComponent,
      data:{
        title: 'seguridad/usuarios'
      }
    },
    {
      path:'registro',
      component:PersonsComponent,
      data:{
        title: 'Registro'
      }
    },
    {
      path:'administracion/centros',
      component:CentrosComponent,
      data:{
        title:'administracion/centros'
      }
    },
    {
      path:"administracion/informacion",
      component:InformacionComponent,
      data:{
        title:"administracion/informacion"
      }
    }
    ]

  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
