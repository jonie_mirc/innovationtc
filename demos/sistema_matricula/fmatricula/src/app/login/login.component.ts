import { Component, OnInit, ViewEncapsulation, ViewContainerRef  } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from './../service/service.service';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';



@Component({
  templateUrl: 'login.component.html',
  providers : [ServiceService],
  encapsulation : ViewEncapsulation.None
})

export class LoginComponent implements OnInit {

  public bodyLogin;
  loginForm: FormGroup;
  public url = "login";
  public uriParameters = "parameters";
  public parameters:any;
  constructor(
    private router: Router,
    private service: ServiceService,
    private toastr: ToastrService
  ) {

    this.bodyLogin = {
      username: "",
      userpassword: ""
    };
   }

   Clean(): void {
    this.bodyLogin = {
      username: "",
      userpassword: ""
    };
   }

  ngOnInit() {
    sessionStorage.removeItem('token');

    this.loginForm = new FormGroup({
      'username': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(20)
      ]),
      'userpassword': new FormControl( null, [
        Validators.required,
        Validators.minLength(5)
      ])
    });
  }

  Login(login) {

    var body;
   
      body = JSON.stringify({
        "username" : login.username,
        "userpassword" : login.userpassword
      });
      
      this.service.PostLogin(body, this.url).subscribe(
        data => {
          //alert(data.status)
         
          if (data.status == 200) {
            var token = data.userToken;          
            sessionStorage.setItem('token', token);
            sessionStorage.setItem('data', JSON.stringify(data));
            this.LoadParameters();
            this.router.navigate(['/inicio']);
           
          } else if (data.status == 404) {
            this.toastr.warning(data.message);
          }
          else{
            this.toastr.warning(data.message);

          }
        
        
        },
       Error => this.toastr.error('Ocurrio un error', 'Advertencia!'),
      
      )
  }

  LoadParameters(){
    this.service.Get(this.uriParameters)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.parameters = data.data;
            sessionStorage.setItem('parameters', JSON.stringify(this.parameters));
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');
            
          } else {
            this.toastr.error(data.message, 'Error!');
            
          }
        },
        Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          
        }
      );
  }
}





