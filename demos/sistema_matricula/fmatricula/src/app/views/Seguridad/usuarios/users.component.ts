import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl,  } from '@angular/forms';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { ServiceService } from './../../../service/service.service';

import swal from 'sweetalert2';



@Component({
  templateUrl: 'users.component.html',
  providers: [ServiceService]
  
})
export class UsuariosComponent implements OnInit {

 
  

  public  p = 1;

  public user;
  public typeUser;
  public users = [];
  public typeUsers = [];
  public tipoUsuario;
  public estado;
  public acceso;
  public dato;

  public arreglo: any = [];
  public Center=[];

  UserForm: FormGroup;

  public uriUser = "user";
  public uriTypeUser = "typeuser";
  public uricenter="center";

  public type = 1;
  public action = "Nuevo usuario";

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) {
    

    this.estado = [
      { id: 1, state: 'Activo' },
      { id: 2, state: 'Inactivo' }
    ];    

    

    
  }

  @ViewChild(ModalDirective) modal: ModalDirective;

 

  hide(): void {
    this.modal.hide();
  }

  changeValue($event: any) {
    console.log(event);
  }

  ngOnInit() {

    this.LoadUsers();
    this.LoadTypeUser();
    this.LoadCenter();
    this.service.validationCharacterNumerick();

    this.user = {
      id : "",
      names : "",
      nameuser: "",
      email : "",
      phone : "",
      password : "",
      state :1,
      idtypeuser:0,
      idcenter:0
    };  

   

    this.UserForm = new FormGroup({
      'names' : new FormControl(null, [
       Validators.required,
       Validators.minLength(5),
       Validators.maxLength(100)
      ]),
      'nameuser' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(20)
      ]),
      'email' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5)
       ]),
       'phone' : new FormControl(null, [
        Validators.required
       ]),
       'password' : new FormControl(null, [
        Validators.required
       ]),
       'state' : new FormControl(null, [
        Validators.required,
       ]),
       'idcenter' : new FormControl(null, [
        Validators.required,
       ]),
       'idtypeuser' : new FormControl(null, [
        Validators.required,
       ]),
    });
  }

  LoadUsers() {
  this.service.Get(this.uriUser)
      .subscribe(
        data => {

          if (data.status == 200) {
            this.users = data.data;
            
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }
        },
        Error => {
          if(Error.status==401 || Error.status==403  ) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }        }
    );
  }

  LoadTypeUser() {
    this.service.Get(this.uriTypeUser)
    .subscribe(
      data => {
        
        if (data.status == 200) {
          this.typeUsers = data.data;

        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        
      },
      Error => {
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }      }
    );
  }

  LoadCenter() {
    this.service.Get(this.uricenter)
    .subscribe(
      data => {
        
        if (data.status == 200) {
          this.Center = data.data;

        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        
      },
      Error => {
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }      }
    );
  }

  
  Save(usuario) {
   
    var body;
    var state;

  
    (usuario.state==1)?state='A':state='B';   

  
    
   if (this.type == 1) {
     body = JSON.stringify({
      names : usuario.names,
      nameuser : usuario.nameuser,
      email : usuario.email,
      phone : usuario.phone,
      password : usuario.password,
      state : state,
      idtypeuser : usuario.idtypeuser,
      idcenter : usuario.idcenter,

      });

    } else {
       body = JSON.stringify({
        id : usuario.id,
        names : usuario.names,
        nameuser : usuario.nameuser,
        email : usuario.email,
        phone : usuario.phone,
        password : usuario.password,
        state : state,
        idtypeuser : usuario.idtypeuser,
        idcenter : usuario.idcenter,
      });
    }
    this.service.Send(body, this.uriUser, this.type)
    .subscribe(
    data => {
      if(data.status == 200) { 
        swal("Exito", data.message, "success");

      } else if (data.status == 404) {
        swal("Advertencia!", data.message, "warning");

      } else if (data.status == 406) {
        swal("Advertencia!", data.message, "warning");

      } else {
        swal("Error!",  data.message, "error");
      }
      this.ngOnInit();
      this.hide();
    },
    Error => { 
      if(Error.status==401 || Error.status==403  ) {
        this.service.redirect();
      } else {
        swal("Error!", "Ocurrio un error", "error");
      }
     
    },
   );
  }

 

  open(usuario) {
    if (usuario != 0 && usuario != null && usuario !="") {
      this.user = usuario;    
  
      (usuario.state == 'A') ? this.user.state = 1: this.user.state = 2;

      
      this.action = "Editar usuario";
      this.type = 2;
    } else {
      this.ngOnInit();
      this.action = "Nuevo usuario";
      this.type = 1;
    }
    this.modal.show();
  }

  filterRol(id){
    for(let x of this.typeUsers)
        if(x.id==id)
            return x.description
  }

  filterCenter(id){
    for(let x of this.Center)
          if(x.id==id)
              return x.description
  }
  
}

