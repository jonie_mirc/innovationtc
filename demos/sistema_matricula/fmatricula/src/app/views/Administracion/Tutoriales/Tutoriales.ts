import {
    Component,
    OnInit,
    ViewChild,
    AfterViewInit
  } from '@angular/core';
  import {
    FormGroup,
    Validators,
    FormControl
  } from '@angular/forms';
  
  import {
    ToastrService
  } from 'ngx-toastr';
  import {
    ModalDirective
  } from 'ngx-bootstrap/modal';
  import {
    ServiceService
  } from '../../../service/service.service';
  
  import {
    DataTableDirective
  } from 'angular-datatables';
  import {
    Subject
  } from 'rxjs';
  import {DomSanitizer} from '@angular/platform-browser';
  
  import swal from 'sweetalert2';
  
  @Component({
    templateUrl: 'Tutoriales.html',
    styleUrls:['Tutoriales.css'],
    providers: [ServiceService]
  })
  export class TutorialesComponent implements  OnInit {
  
  
    /*@ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
  
    dtOptions: DataTables.Settings = {};
  
    dtTrigger = new Subject();
  
    public Information;
    public InformationWTC;
    public typeData = [];
    public CategoryData = [];
    public typeResource = [];
    public estado;
    public allresources = [];
    public wtcadd="";
    public wtcvisible=false;
    ComForm: FormGroup;
    MyForm: FormGroup;
    MyFormWTC:FormGroup;*/
  
  
    /*public uriService = "main_information";
    public uriServiceDelete="deleteresourcemaininfo"
    public uricategory = "categorias"
    public urityperesource = "resource_type";
    public uriResourceCenter = "resource_center";
    public uriwtc="main_informationwtc";
    public updatewtc="updatewtc";*/
  
    public idMainInfo;
    public file;
    public urlimg;
    public type = 1;
    public action = "Nuevo";
    public host = this.service.apiUrl + 'files/';
    public vistaAR: boolean = false;
    public dataAR;
    Videos;
   
    baseUrl:string = 'https://www.youtube.com/embed/';
    verificar:boolean=false;
  
    @ViewChild(ModalDirective) modal: ModalDirective;
  
    show() {
  
  
    }
  
    hide(): void {
      this.modal.hide();
      this.type = 1;
    }
  
    constructor(
      private service: ServiceService,
      private toastr: ToastrService,
      private sanitizer: DomSanitizer,
    ) {
  
  
     /* this.estado = [{
          id: 1,
          descripcion: "Activo"
        },
        {
          id: 2,
          descripcion: "Inactivo"
        }
      ];
  
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
        language: this.service.lenguaje()
      };*/
    }
  
    ngOnInit() {
  
      this.Videos={
        videoTarget:"",
        videomuro:"",
        videoyoutube:"",
        videopunto:""
      }

      this.loadVideos()

        
     /* this.loadInformation();
      this.loadCategorys();
      this.loadtypeResources()
      this.service.validationCharacterNumerick();*/
  
  
      /*this.Information = {
        id: "",
        description: "",
        state: 1,
        img_main: "",
        title: "",
        idcenter: 1,
        idcategory: 1,
        urlexternal:""
      };
      this.InformationWTC={
        id: "",
        description: "",
        state: 1,
        img_main: "",
        title: "",
        idcenter: 1,
        idcategory: 1,
        target:"",
        urlexternal:""
      }
  
      this.dataAR = {
        id: "",
        description: "",
        uri: "",
        idtyperesources: "",
        idcenter: "",
        extension: "",
        idtyperesourcear: "",
        urlexternal:""
  
      }
      this.MyFormWTC=new FormGroup({
        'target': new FormControl(null, [
          Validators.required
        ]),
      })
  
      this.MyForm = new FormGroup({
        'description': new FormControl(null, [
          Validators.required,
          Validators.minLength(5)
        ]),
        'uri': new FormControl(null, null),
        'idtyperesources': new FormControl(null, [
          Validators.required
        ])
      })
  
  
      this.ComForm = new FormGroup({
  
        'description': new FormControl(null, [
          Validators.required,
          Validators.minLength(5)
        ]),
        'img_main': new FormControl(null, null),
        'title': new FormControl(null, [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50)
        ]),
        'idcategory': new FormControl(null, [
          Validators.required
        ]),
  
        'state': new FormControl(null, [
          Validators.required
        ]),
        'idtyperesourcear': new FormControl(null, [
          Validators.required
        ]),
        'urlexternal':new FormControl(null, null)
      });
  
      this.verificar=false;*/
    }
  
  
    loadVideos(){
      //alert()
      this.Videos.videomuro = this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl + "-9pKt3PDK8c");   
      this.Videos.videoTarget = this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl + "gnrXd9MxKTQ");
      this.Videos.videoyoutube = this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl +"ecjs5lq3TD4" );
      this.Videos.videopunto = this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl + "FSZcJLA2ESg");
      this.verificar=true;
  
    }
    /*loadInformation() {
      this.service.Get(this.uriService)
        .subscribe(
          data => {
            if (data.status == 200) {
              this.typeData = data.data;
            } else if (data.status == 404) {
              this.toastr.warning(data.message, 'Advertencia!');
  
            } else {
              this.toastr.error(data.message, 'Error!');
            }
            this.rerender();
  
          },
          Error => {
            if (Error.status == 401 || Error.status == 403) {
              this.service.redirect();
            } else {
              swal("Error!", "Ocurrio un error", "error");
            }
          }
        );
    }
  
    loadtypeResources() {
      this.service.Get(this.urityperesource)
        .subscribe(
          data => {
            if (data.status == 200) {
              this.typeResource = data.data;
            } else if (data.status == 404) {
              this.toastr.warning(data.message, 'Advertencia!');
  
            } else {
              this.toastr.error(data.message, 'Error!');
            }
            //this.rerender();
  
          },
          Error => {
            if (Error.status == 401 || Error.status == 403) {
              this.service.redirect();
            } else {
              swal("Error!", "Ocurrio un error", "error");
            }
          }
        );
    }
  
    Save(comp) {
      var body;
      var state;
  
      (comp.state == 1) ? state = 'A': state = 'B';
  
      if (this.type == 1) {
  
  
        let formData = new FormData();
        formData.append('file', this.file);
  
        this.service.SendFile(formData, 'uploadmainimage')
          .subscribe(
            data => {
              if (data.status == 200) {
                body = JSON.stringify({
                  description: comp.description,
                  title: comp.title,
                  idcenter: 1,
                  idcategory: comp.idcategory,
                  img_main: data.data,
                  state: state,
                  idtyperesourcear: comp.idtyperesourcear,
                  urlexternal:comp.urlexternal
                });
  
                this.service.Send(body, this.uriService, this.type)
                  .subscribe(
                    data => {
                      if (data.status == 200) {
                        swal("Exito", data.message, "success");
                      } else if (data.status == 404) {
                        swal("Advertencia!", data.message, "warning");
  
                      } else {
                        swal("Error!", data.message, "error");
  
                      }
                      // this.rerender();
                      this.ngOnInit();
                      this.hide();
                    },
                    Error => {
                      if (Error.status == 401 || Error.status == 403) {
                        this.service.redirect();
                      } else {
                        swal("Error!", "Ocurrio un error", "error");
                      }
                    },
                  );
  
  
              } else {
                swal("Error!", data.message, "error");
  
              }
            },
            Error => {
              if (Error.status == 401 || Error.status == 403) {
                this.service.redirect();
              } else {
                swal("Error!", "Ocurrio un error", "error");
              }
            },
          );
  
  
  
      } else {
        this.toastr.warning("j");
      }
  
  
    }*/
  
  
  
  }
  