import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ServiceService } from '../../../service/service.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'Argoogle.html',
  styleUrls:['Argoogle.css'],
 providers: [ServiceService]
})
export class ArgoogleComponent implements AfterViewInit, OnInit {

 
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public DataPoi;
  public POIS = [];
  public typeData=[];
  public estado;
  ComForm: FormGroup;

  public uriService = "poi";
  public urityperesource="resource_type";
  public  DataCategoryPlace=[]
  public uriCategoryPlace="categoryplace"
  public file;
  public urlimg;
  public type = 1;
  public action = "Nuevo";
  public host=this.service.apiUrl+'POIS/';
  title: string = 'My first AGM project';
  lat: number = 51.678418;
  lng: number = 7.809007;

  @ViewChild(ModalDirective) modal: ModalDirective;

 

  hide(): void {
    this.modal.hide();
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) { 
    

    this.estado = [
      {id: 1, descripcion : "Activo"},
      {id: 2, descripcion : "Inactivo"}
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language : this.service.lenguaje()
    };

    
  }

  ngOnInit() {

    this.loadPoi();
    this.loadtypeResources();
    this.loadCategoryPlace();
    this.service.validationCharacterNumerick();
    this.service.soloNumeros()

    this.DataPoi = {
      id : "",
      description : "",
      latitud : 0,
      longitud:0,
      altitude:0,
      title:"",
      urlreference:"",
      resource:""    ,
      id_center:"",
      typeresource:"" ,
      idcategoryplace:""
    };

    this.ComForm = new FormGroup({
      
      'description' : new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
       ]),
       'latitud' : new FormControl(null,[
        Validators.required
       ]),
       'longitud' : new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100)
       ]),
       'altitude' : new FormControl(null, [
       
        Validators.minLength(0),
        Validators.maxLength(100)
       ]),
       'title' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
       ]),
       'urlreference' : new FormControl(null, [
      //  Validators.required,
       ]),
      
       'resource' : new FormControl(null, null),
       'typeresource' : new FormControl(null, [
        Validators.required
       ]),
       'idcategoryplace' : new FormControl(null, [
        Validators.required
       ])

       
    });
  }

 

  loadCategoryPlace() {
    this.service.Get(this.uriCategoryPlace)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.DataCategoryPlace = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }


        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }

        }
      );
  }

  loadtypeResources() {
    this.service.Get(this.urityperesource)
    .subscribe(
      data => {
        if (data.status == 200) {
          this.typeData = data.data;
        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        //this.rerender();
       
      },
      Error => {
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }
      }
    );
  }

  FilterCategory(id){
    for(let x of this.DataCategoryPlace)
        if(x.id==id)  
            return x.description

  }
  loadPoi() {
    this.service.Get(this.uriService)
    .subscribe(
      data => {
        if (data.status == 200) {
          this.POIS = data.data;
        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        this.rerender();
       
      },
      Error => {
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }
      
      }
    );
  }

  Filter(index,data){
      for(let x of data)
            if(x.id==index)
                    return x.description

  }

  eliminar(list){

    var body= JSON.stringify({
        id:list.id
    });
    this.service.Delete(body,this.uriService)
    .subscribe(
      data => {
        if (data.status == 200) {
            this.toastr.success(data.message, 'Advertencia!');
            
        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        this.loadPoi()
      //  this.ngOnInit();
        //this.rerender();
       
      },
      Error => {
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }
      
      }
    );

  }
  Save(comp) {
    var body;
     
  
   if (comp.typeresource == 1 ) {


    let formData = new FormData();
    formData.append('file', this.file);

    this.service.SendFile(formData, 'uploadfilepoi')
    .subscribe(
    data => {
      if(data.status==200) { 
              body = JSON.stringify({
                description : comp.description,
                latitud : comp.latitud,
                longitud:comp.longitud,
                altitude:comp.altitude,
                title:comp.title,
                urlreference:comp.urlreference,
                resource:data.data ,
                id_center:comp.id_center,
                typeresource:comp.typeresource,
                idcategoryplace:comp.idcategoryplace
              });

                this.service.Send(body,this.uriService, 1)
                  .subscribe(
                  data => {
                    if(data.status==200) { 
                      swal("Exito", data.message, "success");        
                    } else if(data.status==404) {
                      swal("Advertencia!", data.message, "warning");

                    } else {
                      swal("Error!",  data.message, "error");

                    }
                  // this.rerender();
                    this.ngOnInit();
                    this.hide();
                  },
                  Error => { 
                    if(Error.status==401 || Error.status==403  ) {
                      this.service.redirect();
                    } else {
                      swal("Error!", "Ocurrio un error", "error");
                    }
                  },
                );
       
        
      } else {
        swal("Error!",  data.message, "error");

      }    
    },
    Error => { 
      if(Error.status==401 || Error.status==403  ) {
        this.service.redirect();
      } else {
        swal("Error!", "Ocurrio un error", "error");
      }
    },
   );

     

    } else {
       body = JSON.stringify({
        description : comp.description,
        latitud : comp.latitud,
        longitud:comp.longitud,
        altitude:comp.altitude,
        title:comp.title,
        urlreference:comp.urlreference,
        resource:comp.resource ,
        id_center:comp.id_center,
        typeresource:comp.typeresource,
        idcategoryplace:comp.idcategoryplace
      });
     
      this.service.Send(body, this.uriService, 1)
      .subscribe(
      data => {
        if(data.status==200) { 
          swal("Exito", data.message, "success");        
        } else if(data.status==404) {
          swal("Advertencia!", data.message, "warning");

        } else {
          swal("Error!",  data.message, "error");

        }
      // this.rerender();
        this.ngOnInit();
        this.hide();
      },
      Error => { 
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }
      },
    );
    }

    
  }

  public onArchivoSeleccionado($event) {
    if ($event.target.files.length === 1) {
      this.file = $event.target.files[0];

      console.log(this.file)
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.urlimg = event.target.result;
      }

      reader.readAsDataURL($event.target.files[0]);


    }

  }

  open(comp) {
   

    if (comp != 0 && comp != null && comp !="") {     
      this.DataPoi = comp;
      this.action = "Editar punto";
      this.type = 2;
    } else {
      //this.rerender();
      this.action = "Nuevo Punto";
      this.type = 1;
    }
    this.modal.show();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.ComForm.reset();
    this.ngOnInit();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

}
