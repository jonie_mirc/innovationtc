import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule,
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

import {
  ToastrService
} from 'ngx-toastr';
import {
  ModalDirective
} from 'ngx-bootstrap/modal';
import {
  ServiceService
} from '../../../service/service.service';

import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'centros.html',
  providers: [ServiceService]
})
export class CentrosComponent implements AfterViewInit, OnInit {


  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public Centros;
  public typeData = [];
  public estado;
  ComForm: FormGroup;

  public uriService = "center";
  public uriCountry = "country";
  public uriDeptos = "deptos"
  public uriCategoryPlace = "categoryplace"
  public file;
  public urlimg;
  public type = 1;
  public action = "Nuevo";
  public host = this.service.apiUrl + 'logos/';
  public DataDeptos = []
  public DataCountry = [];
  public DataCategoryPlace = []

  @ViewChild(ModalDirective) modal: ModalDirective;



  hide(): void {
    this.modal.hide();
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) {


    this.estado = [{
        id: 1,
        descripcion: "Activo"
      },
      {
        id: 2,
        descripcion: "Inactivo"
      }
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: this.service.lenguaje()
    };
  }

  ngOnInit() {

    this.loadCentros();
    this.loadCategoryPlace();
    this.loadCountry();

    this.service.validationCharacterNumerick();
 
    this.loadDeptos()

    this.Centros = {
      id: "",
      description: "",
      state: 1,
      img: "",
      email: "",
      telephone: "",
      website: "",
      address: "",
      iddepto: "",
      idcountry: "",
      idcategoryplace: ""
    };

    this.ComForm = new FormGroup({

      'description': new FormControl(null, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100)
      ]),
      'img': new FormControl(null, null),
      'email': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ]),
      'telephone': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(11)
      ]),
      'website': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
      ]),
      'address': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(200)
      ]),

      'state': new FormControl(null, [
        Validators.required
      ]),
      'iddepto': new FormControl(null, [
        Validators.required
      ]),
      'idcountry': new FormControl(null, [
        Validators.required
      ]),
      'idcategoryplace': new FormControl(null, [
        Validators.required
      ])
    });
  }

  loadCentros() {
    this.service.Get(this.uriService)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.typeData = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }
          this.rerender();

        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }

        }
      );
  }

  loadCountry() {
    this.service.Get(this.uriCountry)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.DataCountry = data.data;


          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }


        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }

        }
      );
  }

  loadDeptosFilter(id) {
    var deptos=[]
    for (let x of this.DataDeptos) {
      if (x.idcountry == id) {
        deptos.push(x)
      }
      this.DataDeptos=deptos;
    }


  }
  loadDeptos() {
    //this.DataDeptos=[]
    this.service.Get(this.uriDeptos)
      .subscribe(
        data => {
          this.DataDeptos = []
          if (data.status == 200) {
            this.DataDeptos = data.data;

          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }


        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }

        }
      );
  }

  loadCategoryPlace() {
    this.service.Get(this.uriCategoryPlace)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.DataCategoryPlace = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }


        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }

        }
      );
  }
  Save(comp) {
    var body;
    var state;

    (comp.state == 1) ? state = 'A': state = 'B';

    if (this.type == 1) {


      let formData = new FormData();
      formData.append('file', this.file);

      this.service.SendFile(formData, 'uploadfile')
        .subscribe(
          data => {
            if (data.status == 200) {
              body = JSON.stringify({
                name: comp.name,
                description: comp.description,
                email: comp.email,
                telephone: comp.telephone,
                website: comp.website,
                address: comp.address,
                img: data.data,
                state: state,
                iddepto: comp.iddepto,
                idcountry: comp.idcountry,
                idcategoryplace: comp.idcategoryplace
              });

              this.service.Send(body, "centerinf", this.type)
                .subscribe(
                  data => {
                    if (data.status == 200) {
                      swal("Exito", data.message, "success");
                    } else if (data.status == 404) {
                      swal("Advertencia!", data.message, "warning");

                    } else {
                      swal("Error!", data.message, "error");

                    }

                    this.ngOnInit();
                    this.hide();
                  },
                  Error => {
                    if (Error.status == 401 || Error.status == 403) {
                      this.service.redirect();
                    } else {
                      swal("Error!", "Ocurrio un error", "error");
                    }
                  },
                );


            } else {
              swal("Error!", data.message, "error");

            }
          },
          Error => {
            if (Error.status == 401 || Error.status == 403) {
              this.service.redirect();
            } else {
              swal("Error!", "Ocurrio un error", "error");
            }
          },
        );



    } else {
      body = JSON.stringify({
        id: comp.id,
        name: comp.name,
        description: comp.description,
        email: comp.email,
        telephone: comp.telephone,
        website: comp.website,
        address: comp.address,
        img:comp.img,
        state: state,
        iddepto: comp.iddepto,
        idcountry: comp.idcountry,
        idcategoryplace: comp.idcategoryplace
      });

      this.service.Send(body, "centerinf", this.type)
        .subscribe(
          data => {
            if (data.status == 200) {
              swal("Exito", data.message, "success");
            } else if (data.status == 404) {
              swal("Advertencia!", data.message, "warning");

            } else {
              swal("Error!", data.message, "error");

            }

            this.ngOnInit();
            this.hide();
          },
          Error => {
            if (Error.status == 401 || Error.status == 403) {
              this.service.redirect();
            } else {
              swal("Error!", "Ocurrio un error", "error");
            }
          },
        );
    }


  }

  filterPlace(id) {
    for (let x of this.DataCategoryPlace)
      if (x.id == id)
        return x.description
  }

  filterCountry(id) {
    for (let x of this.DataCountry)
      if (x.id == id)
        return x.description


  }

  filterDepto(id) {
    for (let x of this.DataDeptos)
      if (x.id == id)
        return x.description
  }
  public onArchivoSeleccionado($event) {
    if ($event.target.files.length === 1) {
      this.file = $event.target.files[0];

      console.log(this.file)
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.urlimg = event.target.result;
      }

      reader.readAsDataURL($event.target.files[0]);


    }

  }

  open(comp) {


    if (comp != 0 && comp != null && comp != "") {
      this.Centros = comp;
      (comp.state == 'A') ? this.Centros.state = 1: this.Centros.state = 2;
      this.action = "Editar informacion del centro";
      this.type = 2;
      console.log(comp)
    } else {

      this.action = "Nuevo centro";
      this.type = 1;
    }
    this.modal.show();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.ComForm.reset();
    this.ngOnInit();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

}
