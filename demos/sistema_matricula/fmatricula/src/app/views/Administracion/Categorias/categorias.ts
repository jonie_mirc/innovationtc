import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";

import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ServiceService } from '../../../service/service.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'categorias.html',
 providers: [ServiceService]
})
export class CategoriasComponent implements AfterViewInit, OnInit {

 
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public Category;
  public CategoryData = [];
  public companias;
  public estado;
  ComForm: FormGroup;

  public uriService = "categorias";
  public titulo;

  public type = 1;
  public action = "Nuevo";
  private sub: any;
  @ViewChild(ModalDirective) modal: ModalDirective;

  show(Category) {
    
   
   // console.log(Category)
  }

  hide(): void {
    this.modal.hide();
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { 
    

    this.estado = [
      {id: 'A', descripcion : "Activo"},
      {id: 'B', descripcion : "Inactivo"}
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language : this.service.lenguaje()
    };
  }

 
  ngOnInit() {
   

    this.loadCategorys();
    this.service.validationCharacter();

    this.Category = {
      id : "",
      description : "",
      state : '',
      idcenter:1
    };

    this.ComForm = new FormGroup({
      
      'description' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
       ]),
       'state' : new FormControl(null, [
        Validators.required
       ])
    });

    this.loadtabla()
  }

  loadCategorys() {
    this.service.Get(this.uriService)
    .subscribe(
      data => {
        if (data.status == 200) {
          this.CategoryData = data.data;
        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        this.rerender();
       
      },
      Error => { 
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }
      }
    );
  }

  Save(comp) {
    var body;
    //var state;
  
   // (comp.state==1)?state='A':state='B';   
  
   if (this.type == 1) {
     body = JSON.stringify({
        name: comp.name,
        description : comp.description,
        state : comp.state,
        idcenter:1
      });

    } else {
       body = JSON.stringify({
        id : comp.id,
        description : comp.description,
        state : comp.state,
        idcenter:1
      });
     
    }
    this.service.Send(body, this.uriService, this.type)
    .subscribe(
    data => {
      if(data.status==200) { 
        swal("Exito", data.message, "success");        
      } else if(data.status==404) {
        swal("Advertencia!", data.message, "warning");

      } else {
        swal("Error!",  data.message, "error");

      }
     // this.rerender();
      this.ngOnInit();
      this.hide();
    },
    Error => { 
      if(Error.status==401 || Error.status==403  ) {
        this.service.redirect();
      } else {
        swal("Error!", "Ocurrio un error", "error");
      }
    },
   );
  }

  open(comp) {
   

    if (comp != 0 && comp != null && comp !="") {     
      this.Category = comp;
     // (comp.state == 'A') ? this.Category.state = 1: this.Category.state = 2;
      this.action = "Editar categorías";
      this.type = 2;
    } else {
      //this.rerender();
     // alert()
      this.action = "Nueva categoría";
      this.type = 1;
    }
    this.modal.show();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.ComForm.reset();
    this.ngOnInit();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  requisitosFinal=[];
  rows=[];
  tabla;
  loadtabla(){
    var nuevoArra=[];
    var requisito=[];
    
    this.service.GetFalse("http://localhost/cargo/orden.json")
      .subscribe(
        data => {
          if (data.status == 200) {
            
            this.tabla=data.data[0];
            
            for(let x=1;x<=this.tabla.indexList;x++){
              this.rows.push(x)

            }
        
           // for(let y of this.tabla){                
            this.tabla.requisitosFinal=this.eliminarObjetosDuplicados(this.tabla.requisitos,"id") 

            

            console.log(this.tabla)
              /*for(let y of this.tabla.servicios){                
                y.requisitosFinal=this.eliminarObjetosDuplicados(y.requisitos,"id") 
                for(let x of y.requisitos){
                  console.log(x.id.toString(),x.index.toString())
                  x.key=x.id.toString()+x.index.toString()

                }            
              }*/



             

          } else if (data.status == 404) {
            swal("Advertencia!", data.message, "warning");

          } else if (data.status == 406) {
            swal("Advertencia!", data.message, "warning");

          } else if (data.status == 500) {
            swal("Advertencia!", data.message, "warning");

          } else {
            swal("Error!", data.message, "error");

          }
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        },
      );
  }

  eliminarObjetosDuplicados(arr, prop) {
    var nuevoArray = [];
    var lookup = {};

    for (var i in arr) {
      lookup[arr[i][prop]] = arr[i];
    }

    for (i in lookup) {
      nuevoArray.push(lookup[i]);
    }

    return nuevoArray;
  }

  probar(data){
   // console.log(data)
    var cant = 0;
    var contRow=0;
    var contColum=0;
    var contColum2=0;
    var row=this.rows.length;
    var column=data.requisitosFinal.length;
    console.log(row)

    $(".inputsTable").each(function(){  //todos los que sean de la clase row1
        
        if(contColum==column){
         // cant=0;
            cant++;
            contColum=0;
        }
        console.log("servicio: "+ $(this).attr('id')+ ", indice: "+ cant+", valor: "+ $(this).val())

        
       
        contRow++;
        contColum++;
       
    });
  }
}
