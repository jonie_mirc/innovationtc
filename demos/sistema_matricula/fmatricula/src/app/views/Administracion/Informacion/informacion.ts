import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

import {
  ToastrService
} from 'ngx-toastr';
import {
  ModalDirective
} from 'ngx-bootstrap/modal';
import {
  ServiceService
} from '../../../service/service.service';

import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'informacion.html',
  styleUrls:['informacion.css'],
  providers: [ServiceService]
})
export class InformacionComponent implements AfterViewInit, OnInit {


  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public Information;
  public InformationWTC;
  public typeData = [];
  public CategoryData = [];
  public typeResource = [];
  public estado;
  public allresources = [];
  public wtcadd="";
  public wtcvisible=false;
  ComForm: FormGroup;
  MyForm: FormGroup;
  MyFormWTC:FormGroup;


  public uriService = "main_information";
  public uriServiceDelete="deleteresourcemaininfo"
  public uricategory = "categorias"
  public urityperesource = "resource_type";
  public uriResourceCenter = "resource_center";
  public uriwtc="main_informationwtc";
  public updatewtc="updatewtc";

  public idMainInfo;
  public file;
  public urlimg;
  public type = 1;
  public action = "Nuevo";
  public host = this.service.apiUrl + 'files/';
  public vistaAR: boolean = false;
  public dataAR;
  video;
  baseUrl:string = 'https://www.youtube.com/embed/';
  verificar:boolean=false;

  @ViewChild(ModalDirective) modal: ModalDirective;

  show() {


  }

  hide(): void {
    this.modal.hide();
    this.type = 1;
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
  ) {


    this.estado = [{
        id: 1,
        descripcion: "Activo"
      },
      {
        id: 2,
        descripcion: "Inactivo"
      }
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: this.service.lenguaje()
    };
  }

  ngOnInit() {

    this.loadInformation();
    this.loadCategorys();
    this.loadtypeResources()
    this.service.validationCharacterNumerick();


    this.Information = {
      id: "",
      description: "",
      state: 1,
      img_main: "",
      title: "",
      idcenter: 1,
      idcategory: 1,
      urlexternal:""
    };
    this.InformationWTC={
      id: "",
      description: "",
      state: 1,
      img_main: "",
      title: "",
      idcenter: 1,
      idcategory: 1,
      target:"",
      urlexternal:""
    }

    this.dataAR = {
      id: "",
      description: "",
      uri: "",
      idtyperesources: "",
      idcenter: "",
      extension: "",
      idtyperesourcear: "",
      urlexternal:""

    }
    this.MyFormWTC=new FormGroup({
      'target': new FormControl(null, [
        Validators.required
      ]),
    })

    this.MyForm = new FormGroup({
      'description': new FormControl(null, [
        Validators.required,
        Validators.minLength(5)
      ]),
      'uri': new FormControl(null, null),
      'idtyperesources': new FormControl(null, [
        Validators.required
      ])
    })


    this.ComForm = new FormGroup({

      'description': new FormControl(null, [
        Validators.required,
        Validators.minLength(5)
      ]),
      'img_main': new FormControl(null, null),
      'title': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ]),
      /*'idcenter' : new FormControl(null, [
       Validators.required
      ]),*/
      'idcategory': new FormControl(null, [
        Validators.required
      ]),

      'state': new FormControl(null, [
        Validators.required
      ]),
      'idtyperesourcear': new FormControl(null, [
        Validators.required
      ]),
      'urlexternal':new FormControl(null, null)
    });

    this.verificar=false;
  }


  verificarFn(url){
    //alert()
    this.video = this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl + url);    
    this.verificar=true;

  }
  loadInformation() {
    this.service.Get(this.uriService)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.typeData = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }
          this.rerender();

        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        }
      );
  }

  loadtypeResources() {
    this.service.Get(this.urityperesource)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.typeResource = data.data;  
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }
          //this.rerender();

        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        }
      );
  }

  Save(comp) {
    var body;
    var state;

    (comp.state == 1) ? state = 'A': state = 'B';

    if (this.type == 1) {


      let formData = new FormData();
      formData.append('file', this.file);

      this.service.SendFile(formData, 'uploadmainimage')
        .subscribe(
          data => {
            if (data.status == 200) {
              body = JSON.stringify({
                description: comp.description,
                title: comp.title,
                idcenter: 1,
                idcategory: comp.idcategory,
                img_main: data.data,
                state: state,
                idtyperesourcear: comp.idtyperesourcear,
                urlexternal:comp.urlexternal
              });

              this.service.Send(body, this.uriService, this.type)
                .subscribe(
                  data => {
                    if (data.status == 200) {
                      swal("Exito", data.message, "success");
                    } else if (data.status == 404) {
                      swal("Advertencia!", data.message, "warning");

                    } else {
                      swal("Error!", data.message, "error");

                    }
                    // this.rerender();
                    this.ngOnInit();
                    this.hide();
                  },
                  Error => {
                    if (Error.status == 401 || Error.status == 403) {
                      this.service.redirect();
                    } else {
                      swal("Error!", "Ocurrio un error", "error");
                    }
                  },
                );


            } else {
              swal("Error!", data.message, "error");

            }
          },
          Error => {
            if (Error.status == 401 || Error.status == 403) {
              this.service.redirect();
            } else {
              swal("Error!", "Ocurrio un error", "error");
            }
          },
        );



    } else {
      this.toastr.warning("j");
    }


  }

  SaveAR(comp) {
    var body;

   // if(this.dataAR.idtyperesources==1){
      let formData = new FormData();
      formData.append('file', this.file);
      this.service.SendFile(formData, 'uploadmainimage')
      .subscribe(
        data => {
          if (data.status == 200) {
            body = JSON.stringify({
              description: comp.description,
              extension: comp.title,
              idtyperesources: comp.idtyperesources,
              uri: data.data,
              id_main_info: this.idMainInfo,
              urlexternal:comp.urlexternal
            });

            this.service.Send(body, this.uriResourceCenter, 1)
              .subscribe(
                data => {
                  if (data.status == 200) {
                    swal("Exito", data.message, "success");
                    this.idMainInfo = "";
                    this.ngOnInit();
                    location.reload()
                  } else if (data.status == 404) {
                    swal("Advertencia!", data.message, "warning");

                  } else {
                    swal("Error!", data.message, "error");

                  }
                },
                Error => {
                  if (Error.status == 401 || Error.status == 403) {
                    this.service.redirect();
                  } else {
                    swal("Error!", "Ocurrio un error", "error");
                  }
                },
              );


          } else {
            swal("Error!", data.message, "error");

          }
        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        },
      );
   //}
    /*else{

      body = JSON.stringify({
        description: comp.description,
        extension: comp.title,
        idtyperesources: comp.idtyperesources,
        uri: this.dataAR.uri,
        id_main_info: this.idMainInfo,
        urlexternal:comp.urlexternal
      });

      this.service.Send(body, this.uriResourceCenter, 1)
        .subscribe(
          data => {
            if (data.status == 200) {
              swal("Exito", data.message, "success");
              this.idMainInfo = "";
              this.ngOnInit();
              location.reload()
            } else if (data.status == 404) {
              swal("Advertencia!", data.message, "warning");

            } else {
              swal("Error!", data.message, "error");

            }
          },
          Error => {
            if (Error.status == 401 || Error.status == 403) {
              this.service.redirect();
            } else {
              swal("Error!", "Ocurrio un error", "error");
            }
          },
        );


    }*/

  }

  SaveWTC(comp) {
    var body;
    var state;



    let formData = new FormData();
    formData.append('file', this.filWTC);

    this.service.SendFile(formData, 'uploadmainimage')
      .subscribe(
        data => {
          if (data.status == 200) {
            body = JSON.stringify({
              description: this.InformationWTC.description,
              id: this.InformationWTC.id,
              idtyperesources: this.InformationWTC.idtyperesources,
              title: this.InformationWTC.title,
              target: data.data,
              idcategory:this.InformationWTC.idcategory,
              idtyperesourcear:this.InformationWTC.idtyperesourcear,
              state:this.InformationWTC.state,
              img_main:this.InformationWTC.img_main,
              urlexternal:comp.urlexternal
             
            });

            this.service.Send(body, this.uriwtc, 2)
              .subscribe(
                data => {
                  if (data.status == 200) {
                    swal("Exito", data.message, "success");
                    this.idMainInfo = "";
                    this.ngOnInit();
                    location.reload()
                  } else if (data.status == 404) {
                    swal("Advertencia!", data.message, "warning");

                  } else {
                    swal("Error!", data.message, "error");

                  }
                },
                Error => {
                  if (Error.status == 401 || Error.status == 403) {
                    this.service.redirect();
                  } else {
                    swal("Error!", "Ocurrio un error", "error");
                  }
                },
              );


          } else {
            swal("Error!", data.message, "error");

          }
        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        },
      );

  }

  Eliminar(lista) {

    var body = JSON.stringify({
      id: lista.id
    });

    this.service.Delete(body, this.uriService)
      .subscribe(
        data => {
          if (data.status == 200) {
            swal("Exito", data.message, "success");
            location.reload()
          } else if (data.status == 404) {
            swal("Advertencia!", data.message, "warning");

          } else {
            swal("Error!", data.message, "error");

          }
          // this.rerender();
          this.ngOnInit();
          this.hide();
        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        },
      );
  }

  EliminarTarget(){
            var body;
            body = JSON.stringify({
              description: this.InformationWTC.description,
              id: this.InformationWTC.id,
              idtyperesources: this.InformationWTC.idtyperesources,
              title: this.InformationWTC.title,
              target: "",
              idcategory:this.InformationWTC.idcategory,
              idtyperesourcear:this.InformationWTC.idtyperesourcear,
              state:this.InformationWTC.state,
              img_main:this.InformationWTC.img_main
             
            });

            this.service.Send(body, this.updatewtc, 2)
              .subscribe(
                data => {
                  if (data.status == 200) {
                    swal("Exito", data.message, "success");
                    this.idMainInfo = "";
                    this.ngOnInit();
                    location.reload()
                  } else if (data.status == 404) {
                    swal("Advertencia!", data.message, "warning");

                  } else {
                    swal("Error!", data.message, "error");

                  }
                },
                Error => {
                  if (Error.status == 401 || Error.status == 403) {
                    this.service.redirect();
                  } else {
                    swal("Error!", "Ocurrio un error", "error");
                  }
                },
              );


          
        
  }

  EliminarResource(id){
    console.log(id)
    

    var body = JSON.stringify({
      id: id
    });

    this.service.Delete(body, this.uriServiceDelete)
      .subscribe(
        data => {
          if (data.status == 200) {
            swal("Exito", data.message, "success");
            location.reload()
          } else if (data.status == 404) {
            swal("Advertencia!", data.message, "warning");

          } else {
            swal("Error!", data.message, "error");

          }
          // this.rerender();
          this.ngOnInit();
          this.hide();
        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        },
      );

  }
  public onArchivoSeleccionado($event) {
    if ($event.target.files.length === 1) {
      this.file = $event.target.files[0];

      console.log(this.file)
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.urlimg = event.target.result;
      }

      reader.readAsDataURL($event.target.files[0]);


    }

  }


  public filWTC;
  public urlWTC;
  public onArchivoSeleccionadoWTC($event) {
    if ($event.target.files.length === 1) {
      this.filWTC = $event.target.files[0];

      console.log(this.filWTC)
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.urlWTC = event.target.result;
      }

      reader.readAsDataURL($event.target.files[0]);


    }

  }

  open(comp) {


    if (comp != 0 && comp != null && comp != "") {
      this.Information = comp;
      (comp.state == 'A') ? this.Information.state = 1: this.Information.state = 2;
      this.action = "Editar publicación ";
      this.type = 2;
    } else {
      //this.rerender();
      this.action = "Nueva publicación";
      this.type = 1;
    }
    this.modal.show();
  }
  loadCategorys() {
    this.service.Get(this.uricategory)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.CategoryData = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }

        },
        Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
        }
      );
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    // this.ComForm.reset();
    this.modal.hide();
    this.ngOnInit();
    this.type = 1;

  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  truncate(text) {
    return text.substring(0, 10);

  }
  filterCategory(id) {
    for (let x of this.CategoryData)
      if (x.id == id)
        return x.description

  }
  filterResource(id) {
    for (let x of this.typeResource)
      if (x.id == id)
        return x.description

  }

  
  ActivarVistaAr(list, items, value) {
    this.wtcadd=""
    this.vistaAR = value;
    this.allresources = items;
    this.dataAR.idtyperesources = list.idtyperesourcear;
    this.idMainInfo = list.id;
    this.InformationWTC=list;
    this.wtcadd=list.target;
    this.wtcvisible=value;
    console.log(this.InformationWTC)


  }

}
