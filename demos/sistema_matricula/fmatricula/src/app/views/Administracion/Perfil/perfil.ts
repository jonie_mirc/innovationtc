import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule,
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

import {
  ToastrService
} from 'ngx-toastr';
import {
  ModalDirective
} from 'ngx-bootstrap/modal';
import {
  ServiceService
} from '../../../service/service.service';

import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'perfil.html',
  providers: [ServiceService]
})
export class PerfilComponent implements AfterViewInit, OnInit {


  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public PerfilInfo;
  public typeData = [];
  public companias;
  public estado;
  ComForm: FormGroup;

  public uriService = "perfilcentro";

  public type = 1;
  public action = "Nuevo";
  public host;
  public ArrayMult;
  public dataupdate;
  public habilitar=0;
  public file;
  public urlimg;


  @ViewChild(ModalDirective) modal: ModalDirective;

  show() {


  }

  hide(): void {
    this.modal.hide();
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) {


    this.estado = [{
        id: 'A',
        descripcion: "Activo"
      },
      {
        id: 'B',
        descripcion: "Inactivo"
      }
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: this.service.lenguaje()
    };
  }


  ngOnInit() {

    this.loadMainInfo();
    this.service.validationCharacterNumerick();
    this.host = this.service.apiUrl;

    this.ArrayMult = {
      telefonos: [],
      redes: [],
      correos: [],
      direcciones:[]
    }

    this.PerfilInfo = {
      id: "",
      description: "",
      telephone: "",
      website: "",
      address: "",
      email:""
    };

    this.ComForm = new FormGroup({

      'description': new FormControl(null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(100)
      ]),
      'telephone': new FormControl(null, null),
      'website': new FormControl(null,null),
      'address': new FormControl(null, null),
      'email':new FormControl(null, null),

    });

    this.dataupdate=null;
    this.habilitar=0;
  }


  updateImgs(comp,tipo){
      //1 es logo, 2 es banner

      this.dataupdate=comp;
      this.habilitar=tipo;

      console.log(this.habilitar);
      this.ArrayMult.telefonos=comp.telephone.split(',');    
      this.ArrayMult.redes =comp.website.split(','); 
      this.ArrayMult.correos =comp.email.split(','); 
      this.ArrayMult.direcciones=comp.address.split(','); 



  }

  saveImg(){
      if(this.dataupdate==null || this.dataupdate=='' || typeof(this.dataupdate)==undefined){
          this.toastr.warning('Debe seleccionar una imagen')
            return;
      }

      
      var serviceUrl;
      if(this.habilitar==1){
         serviceUrl="uploadfile";
      }
      else{
         serviceUrl="uploadmainimage";
      }

      let formData = new FormData();
      formData.append('file', this.file);

      this.service.SendFile(formData, serviceUrl)
        .subscribe(
          data => {
            if (data.status == 200) {    
                if(this.habilitar==1){
                    this.dataupdate.img=data.data; 
                }
                else{
                    this.dataupdate.cover_page=data.data; 
                }
                 
                this.Save(this.dataupdate)
            } else {
              swal("Error!", data.message, "error");

            }
          },
          Error => {
            if (Error.status == 401 || Error.status == 403) {
              this.service.redirect();
            } else {
              swal("Error!", "Ocurrio un error", "error");
            }
          },
        );
      
  }

  

  public onArchivoSeleccionado($event) {
    if ($event.target.files.length === 1) {
      this.file = $event.target.files[0];

      console.log(this.file)
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.urlimg = event.target.result;
      }

      reader.readAsDataURL($event.target.files[0]);


    }

  }

  addArray(deescription, type) {
    switch (type) {
      case 1:
        this.ArrayMult.telefonos.push(deescription)
        break;
      case 2:
        this.ArrayMult.redes.push(deescription)
        break;
      case 3:
        this.ArrayMult.correos.push(deescription)
        break;
      case 4:
        this.ArrayMult.direcciones.push(deescription)
        break;
    }

  }

  eliminarBadge(type,index){
    switch (type) {
        case 1:
          this.ArrayMult.telefonos.splice(index,1)
          break;
        case 2:
          this.ArrayMult.redes.splice(index,1)
          break;
        case 3:
          this.ArrayMult.correos.splice(index,1)
          break;
        case 4:
          this.ArrayMult.direcciones.splice(index,1)
          break;
      }
  }

  filter(id) {
    for (let x of this.estado) {
      if (x.id == id)
        return x.descripcion

    }
  }
  loadMainInfo() {
    this.service.Get(this.uriService)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.typeData = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }
          this.rerender();

        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        }
      );
  }

  Save(comp) {
    var body;

    
    console.log(comp)

    body = JSON.stringify({
        id: comp.id,
        description: comp.description,
        telephone: this.ArrayMult.telefonos.join(),
        website: this.ArrayMult.redes.join(),
        address: this.ArrayMult.direcciones.join(),
        email:this.ArrayMult.correos.join(),
        cover_page:comp.cover_page,
        idcategoryplace:comp.idcategoryplace,
        idcountry:comp.idcountry,
        iddepto:comp.iddepto,
        img:comp.img,
        state:comp.state


      });


    this.service.Send(body, this.uriService, 2)
      .subscribe(
        data => {
          if (data.status == 200) {
            swal("Exito", data.message, "success");
          } else if (data.status == 404) {
            swal("Advertencia!", data.message, "warning");

          } else {
            swal("Error!", data.message, "error");

          }
          // this.rerender();
          this.ngOnInit();
          this.hide();
        },
        Error => {
          if (Error.status == 401 || Error.status == 403) {
            this.service.redirect();
          } else {
            swal("Error!", "Ocurrio un error", "error");
          }
        },
      );
  }

  open(comp) {


    if (comp != 0 && comp != null && comp != "") {
      this.PerfilInfo = comp;
      this.action = "Editar categoría de lugares";
      this.type = 2;

      this.ArrayMult.telefonos=comp.telephone.split(',');    
      this.ArrayMult.redes =comp.website.split(','); 
      this.ArrayMult.correos =comp.email.split(','); 
      this.ArrayMult.direcciones=comp.address.split(','); 

      comp.telephone="";
      comp.website="";
      comp.email="";
      comp.address="";


    } else {
      //this.rerender();
      this.action = "Nueva categoría de  lugares";
      this.type = 1;
    }
    this.modal.show();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.ComForm.reset();
    this.ngOnInit();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

}
