export class Classes{
    constructor(
        public class_id:number,
        public class_name:string,
        public class_description:string,
        public module_id:number
    ){}
}