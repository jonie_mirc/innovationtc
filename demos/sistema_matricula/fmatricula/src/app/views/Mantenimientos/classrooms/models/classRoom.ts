export class ClassRoom{
    constructor(
        public classroom_id:number,
        public classroom_name:string,
        public classroom_description:string,
        public building_id:number
    ){}
}