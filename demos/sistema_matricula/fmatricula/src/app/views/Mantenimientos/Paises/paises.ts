import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ServiceService } from '../../../service/service.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'paises.html',
 providers: [ServiceService]
})
export class PaisesComponent implements AfterViewInit, OnInit {

 
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public typeResource;
  public typeData = [];
  public companias;
  public estado;
  ComForm: FormGroup;

  public uriService = "country";

  public type = 1;
  public action = "Nuevo";

  @ViewChild(ModalDirective) modal: ModalDirective;

  show() {
    
   
  }

  hide(): void {
    this.modal.hide();
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) { 
    

    this.estado = [
      {id: 1, descripcion : "Activo"},
      {id: 2, descripcion : "Inactivo"}
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language : this.service.lenguaje()
    };
  }


  ngOnInit() {

    this.loadPaises();
    this.service.validationCharacterNumerick();

    this.typeResource = {
      id : "",
      description : "",
      state : 1
    };

    this.ComForm = new FormGroup({
      
      'description' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
       ]),
       'state' : new FormControl(null, [
        Validators.required
       ])
    });
  }

  filter(id){
    for(let x of this.estado){
        if(x.id==id)
          return x.descripcion

    }
  }
  loadPaises() {
    this.service.Get(this.uriService)
    .subscribe(
      data => {
        if (data.status == 200) {
          this.typeData = data.data;
        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        this.rerender();
       
      },
      Error => {
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }
      }
    );
  }

  Save(comp) {
    var body;
    var state;
  
    (comp.state==1)?state='A':state='B';   
  
   if (this.type == 1) {
     body = JSON.stringify({
        name: comp.name,
        description : comp.description,
        state : state,
        keyd:comp.keyd
      });

    } else {
       body = JSON.stringify({
        id : comp.id,
        description : comp.description,
        state : state,
        keyd:comp.keyd
      });
     
    }
    this.service.Send(body, this.uriService, this.type)
    .subscribe(
    data => {
      if(data.status==200) { 
        swal("Exito", data.message, "success");        
      } else if(data.status==404) {
        swal("Advertencia!", data.message, "warning");

      } else {
        swal("Error!",  data.message, "error");

      }
     // this.rerender();
      this.ngOnInit();
      this.hide();
    },
    Error => { 
      if(Error.status==401 || Error.status==403  ) {
        this.service.redirect();
      } else {
        swal("Error!", "Ocurrio un error", "error");
      }
    },
   );
  }

  open(comp) {
   

    if (comp != 0 && comp != null && comp !="") {     
      this.typeResource = comp;
      (comp.state == 'A') ? this.typeResource.state = 1: this.typeResource.state = 2;
      this.action = "Editar país";
      this.type = 2;
    } else {
      //this.rerender();
      this.action = "Nueva país";
      this.type = 1;
    }
    this.modal.show();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.ComForm.reset();
    this.ngOnInit();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

}
