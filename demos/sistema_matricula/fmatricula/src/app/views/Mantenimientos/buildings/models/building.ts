export class Building{
    constructor(
        public building_id:number,
        public building_name:string,
        public building_description:string
    ){}
}