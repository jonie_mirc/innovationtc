import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { TooltipConfig } from 'ngx-bootstrap/tooltip';
import { ServiceService } from './../../../service/service.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import swal from 'sweetalert2';
import { Schedule } from './models/schedule';

export function getAlertConfig(): TooltipConfig {
  return Object.assign(new TooltipConfig(), {
    container: 'body'
  });
}

@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.scss'],
  providers: [ServiceService, { provide: TooltipConfig, useFactory: getAlertConfig }]
})
export class SchedulesComponent implements AfterViewInit, OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  config = {
    displayKey: "descripcion",
    placeholder: 'Seleccione',
    search: true,
    height: 'auto',
    customComparator: () => { },
    limitTo: 10

  };

  public uriAllSchaduls = "schedules";
  public data:any;
  public scheduls: Schedule[];

  constructor(
    private spinner: NgxSpinnerService,
    private service: ServiceService,
    private toastr: ToastrService
  ) {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: this.service.lenguaje()
    };
   }

  ngOnInit() {
    this.loadSchedule();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  loadSchedule() {
    this.spinner.show(); 
    this.service.Get(this.uriAllSchaduls)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.scheduls = data.data;
            this.rerender();

            this.spinner.hide();
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');
            this.spinner.hide();
          } else {
            this.toastr.error(data.message, 'Error!');
            this.spinner.hide();
          }
        },
        Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          this.spinner.hide();
        }
      );
  }
}
