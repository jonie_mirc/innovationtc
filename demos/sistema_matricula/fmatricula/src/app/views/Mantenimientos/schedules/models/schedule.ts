export class Schedule{
    constructor(
        public schedule_id:number,
        public schedule_name:string,
        public schedule_description:string,
        public schedule_start:string,
        public schedule_end:string
    ){}
}