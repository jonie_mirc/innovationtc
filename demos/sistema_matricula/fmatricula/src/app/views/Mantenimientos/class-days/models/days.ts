export class Day{
    constructor(
        public classDay_id:number,
        public classDay_name:string,
        public classDay_description:string
    ){}
}