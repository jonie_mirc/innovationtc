export class Module{
    constructor(
        public course_id:number,
        public course_name:string,
        public course_description:string
    ){}
}