export class Parameter{
    constructor(
        public parameter_id:number,
        public parameter_name:string,
        public parameter_value:string,
        public parameter_description:string
    ){}
}