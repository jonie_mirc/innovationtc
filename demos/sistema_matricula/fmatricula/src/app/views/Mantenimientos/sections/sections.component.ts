import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { TooltipConfig } from 'ngx-bootstrap/tooltip';
import { ServiceService } from './../../../service/service.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import swal from 'sweetalert2';
import { Section } from './models/section';

export function getAlertConfig(): TooltipConfig {
  return Object.assign(new TooltipConfig(), {
    container: 'body'
  });
}

@Component({ 
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.scss'],
  providers: [ServiceService, { provide: TooltipConfig, useFactory: getAlertConfig }]
})
export class SectionsComponent implements AfterViewInit, OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  config = {
    displayKey: "descripcion",
    placeholder: 'Seleccione',
    search: true,
    height: 'auto',
    customComparator: () => { },
    limitTo: 10

  };

  public uriSections = "sections";
  public data:any;
  public year:any;
  public period:any;
  public sections: Section[];

  constructor(
    private spinner: NgxSpinnerService,
    private service: ServiceService,
    private toastr: ToastrService
  ) {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: this.service.lenguaje()
    };
  }

  ngOnInit() {
    this.data = JSON.parse(sessionStorage.getItem("parameters"));
    this.getParamterSectionsAPI(this.data);
    this.loadSections();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }
  

  loadSections() {
    let url = this.uriSections + "/" + this.period + "/" + this.year;
    this.spinner.show(); 
    this.service.Get(url)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.sections = data.data;
            this.rerender();

            this.spinner.hide();
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');
            this.spinner.hide();
          } else {
            this.toastr.error(data.message, 'Error!');
            this.spinner.hide();
          }
        },
        Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          this.spinner.hide();
        }
      );
  }

  getParamterSectionsAPI(data:any) {
    this.period = data[0].parameter_value;
    let temp = data[3].parameter_value;
    let temps = temp.split('-');
    this.year = temps[0];
    console.log(this.year);
    console.log(this.period);
  }

}
 