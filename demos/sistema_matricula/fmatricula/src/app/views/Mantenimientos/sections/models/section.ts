export class Section{
    constructor(
        public class_id:number,
        public classCode:string,
        public classDesciption:string,
        public schedule_start:string,
        public schedule_end:string,
        public person:string,
        public classDay:string
    ) {}
} 