import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { NgxSpinnerService } from 'ngx-spinner';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { ServiceService } from './../../service/service.service';
import { ToastrService } from 'ngx-toastr';
import { CarouselModule } from 'ngx-bootstrap/carousel';

@Component({
  templateUrl: 'dashboard.component.html',
  providers: [ServiceService,
    { provide: CarouselConfig, useValue: { interval: 2500, noPause: true, showIndicators: true } }
  ]
})
export class DashboardComponent implements OnInit {

  public uriSlides = "slidesbyprofile";
  public slides: { image: string, title:string, description:string }[] = [];
  public data:any;
  public activeSlideIndex = 0;
  constructor (
    private spinner: NgxSpinnerService,
    private service: ServiceService,
    private toastr: ToastrService
    ) {

  }

  ngOnInit(): void {
  
     this.data =JSON.parse(sessionStorage.getItem("data"));
    this.uriSlides += "/"+this.data.data.userRole;
    
    this.LoadSlides();

  }
  LoadSlides() {
    this.spinner.show();
    this.service.Get(this.uriSlides)
        .subscribe(
          data => {
            if (data.status == 200) {
              let allSlides = data.data;
              allSlides.forEach(slide => {
                let cSlide = {image: slide.carousel_img_path,title:slide.carousel_title, description: slide.carousel_description};
                this.slides.push(cSlide);
    
                this.spinner.hide();
              });
            } else if (data.status == 404) {
              this.toastr.warning(data.message, 'Advertencia!');
              this.spinner.hide();
            } else {
              this.toastr.error(data.message, 'Error!');
              this.spinner.hide();
            }
          },
          Error => {
              this.toastr.error('Ocurrio un error con el servicio', 'Error!');
              this.spinner.hide();
          }
      );
    }
}

