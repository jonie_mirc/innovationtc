import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ServiceService } from './../../../service/service.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'modules.component.html',
  providers: [ServiceService]
})
export class ModulesComponent implements AfterViewInit, OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public mod;
  public mods = [];
  public companys;
  public company = [];
  public modulos;
  public estado;
  ModForm: FormGroup;

  public uriModules = "module";
  public uriCompany = "company";

  public type = 1;
  public action = "Nuevo";

  @ViewChild(ModalDirective) modal: ModalDirective;

  show(): void {
    this.modal.show();
  }

  hide(): void {
    this.modal.hide();
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) { 
    this.mod = {
      id : "",
      name:"",
      description : "",
      state : "",
      id_company: ""
    };

    this.estado = [
      { id: 1, descripcion: "Activo"},
      { id: 2, descripcion: "Inactivo"}
    ];

    this.companys = {
      id : "",
      name : "",
      description : "",
      state : ""
    };


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language : this.service.lenguaje()
    };
  }

  ngOnInit() {

    this.LoadModules();
    this.LoadCompanys();

    this.mod = {
      id : "",
      name: "",
      description : "",
      state : "",
      id_company : ""
    };

    this.ModForm = new FormGroup({
      'name' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
       ]),
      'description' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
       ]),
       'state' : new FormControl(null, [
        Validators.required
       ]),
      'id_company' : new FormControl(null, [
        Validators.required,
        Validators.minLength(1)
      ])
    });
  }

  LoadModules() {
    this.service.Get(this.uriModules)
    .subscribe(
      data => {
        console.log(data);
        if (data.status == 200) {
          this.mods = data.data;
          console.log(this.mods);
        } else if (data.status == 406) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        this.rerender();
        
      },
      Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
      }
    );
  }

  LoadCompanys() {
    this.service.Get(this.uriCompany)
    .subscribe(
      data => {
        if( data.status == 200) {
          this.company = data.data;
        
        } else if( data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia');
            
        } else if ( data.status == 406) {
          this.toastr.warning(data.message, 'Advertencia');

        } else {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
        }
      },
      Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
      }
    );
  }

  Save(modulo) {
    var body;
    var state;
    var fecha;

    if (modulo.estado == 1) {
      state = "A";
    } else {
      state = "I";
    }

  
    fecha = new Date();
    
   if (this.type == 1) {
     body = JSON.stringify({
        name: modulo.name,
        description : modulo.description,
        state : state,
        id_company :  modulo.id_company
      });

    } else {
       body = JSON.stringify({
        id : modulo.id,
        name: modulo.name,
        description : modulo.description,
        state : state,
        id_company : modulo.id_company
      });
     
    }
   console.log(body);
    this.service.Send(body, this.uriModules, this.type)
    .subscribe(
    data => {
      console.log(data);
      if(data.status==200) { 
        swal("Exito", data.message, "success");
        
      } else if(data.status==406) {
        swal("Advertencia!", data.message, "warning");

      } else {
      swal("Error!",  data.message, "error");

      }
      this.rerender();
      this.ngOnInit();
      this.hide();
    },
    Error => { 
      if(Error.status==401 || Error.status==403  ) {
        this.service.redirect();
      } else {
        swal("Error!", "Ocurrio un error", "error");
      }
    },
   );
  }

  Edit(modulo) {
    if (modulo != 0 && modulo != null && modulo !="") {
      this.mod = modulo;
      this.action = "Editar";
      this.type = 2;
    } else {
      this.ngOnInit();
      this.action = "Nuevo";
      this.type = 1;
    }
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.ModForm.reset();
    this.ngOnInit();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

}
