import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ServiceService } from './../../../service/service.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import swal from 'sweetalert2';
//import { AnyARecord } from 'dns';


@Component({
  templateUrl: 'type-users.component.html',
  providers: [ServiceService]
})
export class TypeUsersComponent implements AfterViewInit, OnInit {  

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: any = {};

  dtTrigger = new Subject();


  public typeUser;
  public typeUsers = [];
  public tipoUsuario;
  public estado;
  Rolform: FormGroup;

  public uriTypeUser = "security";

  public type = 1;
  public action = "Nuevo";

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) {
    this.typeUser = {
      id : "",
      description : "",
      state : ""
    };

    this.estado = [
      { id : 1, descripcion : 'Activo'},
      { id : 2, descripcion : 'Inactivo'}
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language : this.service.lenguaje(),
      // buttons: [{
      //   'columnsToggle',
      //   'colvis',
      //   'copy',
      //   'print',
      //   'excel'
      // }]
    };
  }

  isCollapsed= false;

  @ViewChild(ModalDirective) modal: ModalDirective;

  show(): void {
    this.modal.show();
  }

  hide(): void {
    this.modal.hide();
  }

  

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  ngOnInit(): void {

    
    this.LoadTypeUser();

    this.typeUser = {
      id : "",
      description : "",
      state : ""
    };

    this.estado = [
      {
        id : 1,
        descripcion : 'Activo'
      },{
        id : 2,
        descripcion : 'Inactivo'
      }
    ];

    this.Rolform = new FormGroup({
      'description' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
       ]),
       'state' : new FormControl(null, [
        Validators.required
       ])
    });
  }

    LoadTypeUser() {
      this.service.Get(this.uriTypeUser)
      .subscribe(
        data => {
          console.log(data);
          if (data.status == 200) {
            this.typeUsers = data.data;
            console.log(this.typeUsers);
          } else if (data.status == 406) {
            this.toastr.warning(data.message, 'Advertencia!');
  
          } else {
            this.toastr.error(data.message, 'Error!');
          }
          this.rerender();
        },
        Error => {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
        }
      );
    }

    Save(tipoUsuario) {
      var body;
      var state;
  
      if (tipoUsuario.state == 1) {
        state = "A";
      } else if(tipoUsuario.state == 2) {
        state = "I";
      }
      
     if (this.type == 1) {
       body = JSON.stringify({
          description : tipoUsuario.description,
          state : state
        });
  
      } else {
         body = JSON.stringify({
          id : tipoUsuario.id,
          description : tipoUsuario.description,
          state : state
        });
       
      }
     console.log(body);
      this.service.Send(body, this.uriTypeUser, this.type)
      .subscribe(
      data => {
        console.log(data);
        if(data.status==200) { 
          swal("Exito", data.message, "success");
          
        } else if(data.status==406) {
          swal("Advertencia!", data.message, "warning");

        } else {
          swal("Error!",  data.message, "error");

        }
        this.rerender();
      },
      Error => { 
        if(Error.status==401 || Error.status==403  ) {
          this.service.redirect();
        } else {
          swal("Error!", "Ocurrio un error", "error");
        }
        this.ngOnInit();
      },
     );
    }
  
    Edit(tipoUsuario) {
      if (tipoUsuario != 0 && tipoUsuario != null && tipoUsuario !="") {
        
        this.typeUser = tipoUsuario;
        if (tipoUsuario.state == 1) {
          this.typeUser.state = "A";
        } else if(tipoUsuario.state == 2) {
          this.typeUser.state = "I";
        }
    
        if (tipoUsuario.remote_access == 1) {
          this.typeUser.remote_access = "A";
        } else if(tipoUsuario.remote_access == 2) {
          this.typeUser.remote_access = "I";
        }
        this.action = "Editar";
        this.type = 2;
      } else {
        this.ngOnInit();
        this.action = "Nuevo";
        this.type = 1;
      }
    }

    Asignar(tipoUsuario) {
      
      if (tipoUsuario != 0 && tipoUsuario != null && tipoUsuario !="") {
        this.action = "Asignar recursos";
      }
    }


    ngAfterViewInit(): void {
      this.dtTrigger.next();
    }
  
    Clean() {
      this.Rolform.reset();
      this.ngOnInit();
    }
  
    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    }

}
