import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl,  } from '@angular/forms';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { TooltipConfig } from 'ngx-bootstrap/tooltip';
import { ServiceService } from './../../../service/service.service';

import swal from 'sweetalert2';

export function getAlertConfig(): TooltipConfig {
  return Object.assign(new TooltipConfig(), {
    container: 'body'
  });
}

@Component({
  templateUrl: 'users.component.html',
  providers: [ServiceService, { provide: TooltipConfig, useFactory: getAlertConfig }]
  
})
export class UsersComponent implements OnInit {

 
  config = {
    displayKey: "descripcion",
    placeholder:'Seleccione',
    search: true,
    height: 'auto',
    customComparator: () => {},
    limitTo: 10
    
  };

  public  p = 1;

  public user;
  public typeUser;
  public users = [];
  public typeUsers = [];
  public tipoUsuario;
  public estado;
  public acceso;
  public dato;

  public arreglo: any = [];

  UserForm: FormGroup;

  public uriUser = "user";
  public uriTypeUser = "security";

  public type = 1;
  public action = "Nuevo usuario";

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) {
    this.user = {
      id : "",
      names : "",
      user_name: "",
      passwor_duser : "",
      state : "",
      id_role : "",
      remote_access : ""
    };

    this.estado = [
      { id: 1, state: 'Activo' },
      { id: 2, state: 'Inactivo' }
    ];    

    this.acceso = [
      { id : 1, descripcion : 'Activo' },
      { id : 2, descripcion : 'Inactivo' }
    ];

    
  }

  @ViewChild(ModalDirective) modal: ModalDirective;

  show(): void {
    this.modal.show();
  }

  hide(): void {
    this.modal.hide();
  }

  changeValue($event: any) {
    console.log(event);
  }

  ngOnInit() {

    this.LoadUsers();
    this.LoadTypeUser();

    this.user = {
      id : "",
      names : "",
      user_name : "",
      passwor_duser : "",
      state : "",
      id_role : "",
      remote_access : ""
    };
    

    this.estado = [
      {
        id : 1,
        descripcion : 'Activo'
      },{
        id : 2,
        descripcion : 'Inactivo'
      }
    ];

    this.acceso = [
      {
        id : 1,
        descripcion : 'Activo'
      },{
        id : 2,
        descripcion : 'Inactivo'
      }
    ];

    this.UserForm = new FormGroup({
      'names' : new FormControl(null, [
       Validators.required,
       Validators.minLength(5),
       Validators.maxLength(100),
       Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'user_name' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(20),
        Validators.pattern('')
      ]),
      'passwor_duser' : new FormControl(null, [
        Validators.required,
        Validators.minLength(8)
       ]),
       'state' : new FormControl(null, [
        Validators.required
       ]),
       'id_role' : new FormControl(null, [
        Validators.required
       ]),
       'remote_access' : new FormControl(null, [
        Validators.required,
       ]),
    });
  }

  LoadUsers() {
  this.service.Get(this.uriUser)
      .subscribe(
        data => {

          if (data.status == 200) {
            this.users = data.data;
            
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');
          }
        },
        Error => {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
        }
    );
  }

  LoadTypeUser() {
    this.service.Get(this.uriTypeUser)
    .subscribe(
      data => {
        
        if (data.status == 200) {
          this.typeUsers = data.data;

        } else if (data.status == 404) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        
      },
      Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
      }
    );
  }

  
  Save(usuario) {
    console.log(usuario);
    console.log(usuario.state);
    console.log(usuario.remote_access);
    console.log(usuario.id_role);
    console.log(this.type);
    var body;
    

  
    if (usuario.state[0].id == 1) {
      this.user.state = "A";
    } else if(usuario.state[0].id == 2) {
      this.user.state = "I";
    }

    if(usuario.remote_access[0].id == 1) {
      this.user.remote_access = "A";
    } else if(usuario.remote_access[0].id == 2) {
      this.user.remote_access = "I";
    } 

    
   if (this.type == 1) {
     body = JSON.stringify({
        names : usuario.names,
        user_name : usuario.user_name,
        passwor_duser : usuario.passwor_duser,
        state : this.user.state,
        id_role : usuario.id_role[0].id,
        remote_access : this.user.remote_access
      });
      console.log(body);

    } else {
       body = JSON.stringify({
        id : usuario.id,
        names : usuario.names,
        user_name: usuario.user_name,
        passwor_duser : usuario.passwor_duser,
        state : this.user.state,
        id_role : usuario.id_role[0].id,
        remote_access : this.user.remote_access
      });
      console.log(body);
    }
   console.log(body);
    this.service.Send(body, this.uriUser, this.type)
    .subscribe(
    data => {
      console.log(data);
      if(data.status == 200) { 
        swal("Exito", data.message, "success");

      } else if (data.status == 404) {
        swal("Advertencia!", data.message, "warning");

      } else if (data.status == 406) {
        swal("Advertencia!", data.message, "warning");

      } else {
        swal("Error!",  data.message, "error");
      }
      this.ngOnInit();
      this.hide();
    },
    Error => { 
      if(Error.status==401 || Error.status==403  ) {
        this.service.redirect();
      } else {
        swal("Error!", "Ocurrio un error", "error");
      }
      this.ngOnInit();
      this.hide();

    },
   );
  }

  Clean() {
    this.UserForm.reset();
    this.ngOnInit();
  }

  Edit(usuario) {
    if (usuario != 0 && usuario != null && usuario !="") {
      this.user = usuario;
      if (usuario.state == 1) {
        this.user.state = "A";
      } else if(usuario.state == 2) {
        this.user.state = "I";
      }
  
      if (usuario.remote_access == 1) {
        this.user.remote_access = "A";
      } else if(usuario.remote_access == 2) {
        this.user.remote_access = "I";
      }
      this.action = "Editar";
      this.type = 2;
      console.log(usuario);
    } else {
      this.ngOnInit();
      this.UserForm.reset();
      this.action = "Nuevo";
      this.type = 1;
    }
  }

  field(campo) {
    if (campo=="estado" || campo == "remoto") {
      this.config.displayKey = "descripcion";
    }
    if(campo=="role") {
      this.config.displayKey = "description";
    }
  }
}

