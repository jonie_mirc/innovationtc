import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { TooltipConfig } from 'ngx-bootstrap/tooltip';
import { ServiceService } from './../../../service/service.service';
import { Person } from './models/person';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import swal from 'sweetalert2';

export function getAlertConfig(): TooltipConfig {
  return Object.assign(new TooltipConfig(), {
    container: 'body'
  });
}

@Component({
  templateUrl: 'persons.component.html',
  styleUrls: ['./sass/persons.component.scss'],
  providers: [ServiceService, { provide: TooltipConfig, useFactory: getAlertConfig }]

})
export class PersonsComponent implements AfterViewInit, OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();



  config = {
    displayKey: "descripcion",
    placeholder: 'Seleccione',
    search: true,
    height: 'auto',
    customComparator: () => { },
    limitTo: 10

  };

  public persons: Person[];
  public person: Person;
  public p = 1;



  public arreglo: any = [];

  RegPersonForm: FormGroup;

  public uriPerson = "allPersons";
  public searchText: string;
  public type = 1;
  public action = "Nuevo Registro";
  public user;
  public personData;
  public userData;
  public allCourses;
  public allProfiles;
  public submitted = false;

  constructor(
    private spinner: NgxSpinnerService,
    private service: ServiceService,
    private toastr: ToastrService
  ) {
    this.person = new Person(0, "", "", "", "", "", "", null, "", 0, 0, "", 0, "");
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: this.service.lenguaje()
    };
  }

  // Propiedades del modal
  @ViewChild(ModalDirective) modal: ModalDirective;

  show(): void {
    this.modal.show();
  }

  hide(): void {
    this.modal.hide();
  }

  changeValue($event: any) {
    console.log(event);
  }

  ngOnInit() {

    this.LoadPerson();
    this.PropertiesInit();
    this.LoadAllCourses();
    this.LoadAllProfiles();

    this.user = {
      id: "",
      names: "",
      user_name: "",
      passwor_duser: "",
      state: "",
      id_role: "",
      remote_access: ""
    };



    this.RegPersonForm = new FormGroup({
      'names': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'identity': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'celphone': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'phone': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'email': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'birthdate': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^(\d{1,2}/\d{1,2}/\d{4})+$/')
      ]),
      'course': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'genus': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ])
      ,
      'profile': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ]),
      'direction': new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('/^([A-Za-z ñáéíóú])+$/')
      ])
    });


  }

  get f() { return this.RegPersonForm.controls; }
 
    

  PropertiesInit() {
    this.personData = {
      person_id: "",
      person_name: "",
      person_identity: "",
      person_celphone: "",
      person_phone: "",
      person_email: "",
      person_birthdate: "",
      course_id: "",
      person_direction: "",
      person_genus: "",
      person_image: "assets/img/profiles/profile_1503031647.3284.png",
      profile_id: "",
      usersByPerson: []
    }
    this.userData = {
      username: "",
      profileId: "",
      personId: ""
    }
  }
  LoadPerson() {
    this.spinner.show();
    this.service.Get(this.uriPerson)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.persons = data.data;
            this.rerender();

            this.spinner.hide();
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');
            this.spinner.hide();
          } else {
            this.toastr.error(data.message, 'Error!');
            this.spinner.hide();
          }
        },
        Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          this.spinner.hide();
        }
      );
  }

  LoadAllCourses(){
    let uriAllCourses = "allCourses";
    this.service.Get(uriAllCourses)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.allCourses = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');
          } else {
            this.toastr.error(data.message, 'Error!');
          }
        },
        Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
        }
      );
  }

  LoadAllProfiles(){
    let uriAllProfiles = "allProfiles";
    this.service.Get(uriAllProfiles)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.allProfiles = data.data;
          } else if (data.status == 404) {
            this.toastr.warning(data.message, 'Advertencia!');
          } else {
            this.toastr.error(data.message, 'Error!');
          }
        },
        Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
        }
      );
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.RegPersonForm.reset();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }
  Save(typeResource: any) {
    this.submitted = true;
 
        if (this.RegPersonForm.invalid) {
            return;
        }
 
        alert('Mensaje Enviado !')
   }
}

