export class UserByPerson{
    constructor(
        public user_id:number,
        public user_name:string,
        public user_creationDate:Date,
        public user_log: number
    ){}
}