export class Person{
    constructor(
        public person_id:number,
        public person_name:string,
        public person_direction:string,
        public person_phone:string,
        public person_celphone:string,
        public person_email:string,
        public person_identity:string,
        public person_birthdate:Date,
        public person_genus: string,
        public course_id: number,
        public codigo_rol:number,
        public estado:string,
        public codigo_nivel:number,
        public person_image:string
    ){}
}