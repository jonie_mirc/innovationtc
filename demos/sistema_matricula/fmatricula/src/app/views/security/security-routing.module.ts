import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from "./users/users.component";
import { TypeUsersComponent } from './type-users/type-users.component';
import { ResourcesComponent } from './resources/resources.component';
import { ModulesComponent } from './modules/modules.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Seguridad'
    }, children: [
      {
        path: 'users',
        component: UsersComponent,
        data: {
            title: 'Usuarios'
        }
      },
      {
        path: 'typeusers',
        component: TypeUsersComponent,
        data: {
          title: 'Tipos de usuario'
        }
      },
      {
        path: 'resources',
        component: ResourcesComponent,
        data: {
          title: 'Recursos'
        }
      },
      {
        path: 'modules',
        component: ModulesComponent,
        data: {
          title: 'Modulos'
        }
      },
      
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule { }
