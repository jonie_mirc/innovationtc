import { DataTableDirective } from 'angular-datatables';
import { Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ServiceService } from './../../../service/service.service';
import { Subject } from 'rxjs';

import swal from 'sweetalert2';

@Component({
  templateUrl: 'resources.component.html',
  providers: [ServiceService]
})
export class ResourcesComponent implements AfterViewInit, OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger = new Subject();

  public resource;
  public mods = [];
  public resources = [];
  public recurso;
  public estado;
  ResourceForm: FormGroup;

  public uriResource = "resource";
  public uriModules = "module";

  public type = 1;
  public action = "Nuevo";

  @ViewChild(ModalDirective) modal: ModalDirective;

  show(): void {
    this.modal.show();
  }

  hide(): void {
    this.modal.hide();
  }

  constructor(
    private service: ServiceService,
    private toastr: ToastrService
  ) { 
    this.resource = {
      id : "",
      name: "",
      description : "",
      state : "",
      id_module: ""
    };

    this.estado = [
      { id: 1, descripcion: "Activo"},
      { id: 2, descripcion: "Inactivo"}
    ];

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language : this.service.lenguaje()
    };
  }

  ngOnInit() {
    this.LoadResource();
    this.LoadModules();

    this.resource = {
      id : "",
      name: "",
      description : "",
      state : "",
      id_module: ""
    };

    this.ResourceForm = new FormGroup({
      'name' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
      ]),
      'description' : new FormControl(null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
       ]),
       'state' : new FormControl(null, [
        Validators.required
       ]),
       'id_module' : new FormControl(null, [
        Validators.required
       ])
    });
  }

  LoadResource() {
    this.service.Get(this.uriResource)
    .subscribe(
      data => {
        console.log(data);
        if (data.status == 200) {
          this.resources = data.data;
          console.log(this.resources);
        } else if (data.status == 406) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        this.rerender();
        
      },
      Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
      }
    );
  }

  LoadModules() {
    this.service.Get(this.uriModules)
    .subscribe(
      data => {
        console.log(data);
        if (data.status == 200) {
          this.mods = data.data;
          console.log(this.mods);
        } else if (data.status == 406) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');
        }
        this.rerender();
      },
      Error => {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
      }
    );
  }

  Save(recurso) {
    var body;
    var state;

    if (recurso.state == 1) {
      state = "A";
    } else if (recurso.state == 2) {
      state = "I";
    }
    
   if (this.type == 1) {
     body = JSON.stringify({
        name : recurso.name,
        description : recurso.description,
        state : state,
        id_module : recurso.id_module
      });

    } else {
       body = JSON.stringify({
        id : recurso.id,
        name: recurso.name,
        description : recurso.description,
        state : state,
        id_module: recurso.id_module
      });
     
    }
   console.log(body);
    this.service.Send(body, this.uriResource, this.type)
    .subscribe(
    data => {
      console.log(data);
      if(data.status==200) { 
        swal("Exito", data.message, "success");
        
      } else if(data.status==406) {
        swal("Advertencia!", data.message, "warning");

      } else {
        swal("Error!",  data.message, "error");
      }
      this.rerender();
      this.ngOnInit();
      this.hide();
    },
    Error => { 
      if(Error.status==401 || Error.status==403  ) {
        this.service.redirect();
      } else {
        swal("Error!", "Ocurrio un error", "error");
      }
    },
   );
  }

  Edit(recurso) {
    if (recurso != 0 && recurso != null && recurso !="") {
      this.resource = recurso;
      if (recurso.state == 1) {
        this.resource.state = "A";
      } else if(recurso.state == 2) {
        this.resource.state = "I";
      }
      this.action = "Editar";
      this.type = 2;
    } else {
      this.ngOnInit();
      this.action = "Nuevo";
      this.type = 1;
    }
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  Clean() {
    this.ResourceForm.reset();
    this.ngOnInit();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

}
