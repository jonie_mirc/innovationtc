import { NgxPaginationModule } from 'ngx-pagination';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';


import { SecurityRoutingModule } from './security-routing.module';
import { UsersComponent } from "./users/users.component";
import { TypeUsersComponent } from './type-users/type-users.component';
import { ResourcesComponent } from './resources/resources.component';
import { ModulesComponent } from './modules/modules.component';
import { CommonModule } from '@angular/common';

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { DataTablesModule } from 'angular-datatables';
import { SelectDropDownModule } from 'ngx-select-dropdown';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    BsDropdownModule,
    SecurityRoutingModule,
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    CommonModule,
    DataTablesModule,
    NgxPaginationModule,
    CollapseModule.forRoot(),
    TooltipModule.forRoot(),
    SelectDropDownModule,
  ],
  declarations: [
    UsersComponent,
    TypeUsersComponent,
    ResourcesComponent,
    ModulesComponent
    ],

  exports: [
    ModalModule
  ]
})
export class SecurityModule { }
