import {
  Injectable
} from '@angular/core';

import {
  Http,
  Response,
  Headers,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import {
  Configuration
} from '../ConfigSystems/constants';
// tslint:disable-next-line:import-blacklist
import {
  Observable
} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';

declare var $;

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  public lenguajeTable;

  constructor(private http: Http,
    private _url: Configuration) {

  }

  public apiUrl = this._url.ServerLocal;




  public PostLogin(data, url) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    // headers.append('Token', sessionStorage.getItem('Token'));
    var options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });

    

    return this.http.post(this.apiUrl + url, data, options)
      .map(res => res.json())

  }


public GetFalse(url) {
    let headers = new Headers();
   // headers.append('Content-Type', 'application/json; charset=utf-8');
    //headers.append('Token', sessionStorage.getItem('token'));
    
    // headers.append('param', '1 ')
    let options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get( url, options)
      .map((res) => res.json());
  }

  public Get(url) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');

    headers.append('XAuthToken', sessionStorage.getItem('token'));
    
    // headers.append('param', '1 ')
    let options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(this.apiUrl + url, options)
      .map((res) => res.json());
  }



  public Post(data, url) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    headers.append('Token', sessionStorage.getItem('token'));
    let options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });

    return this.http.post(this.apiUrl + url, data, options)
      .map(res => res.json());
  }


  public Delete(data, url) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    headers.append('Token', sessionStorage.getItem('token'));
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      headers: headers
    });

    return this.http.post(this.apiUrl + url, data, options)
      .map(res => res.json());
  }

  public Send(data, url, type) {
    let typeService;
    if (type == 1) {
      typeService = RequestMethod.Post;
    } else {
      typeService = RequestMethod.Put;
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    headers.append('Token', sessionStorage.getItem('token'));
    let options = new RequestOptions({
      method: typeService,
      headers: headers
    });
    return this.http.post(this.apiUrl + url, data, options)
      .map(res => res.json());
  }

  public redirect() {
    // window.location.reload();
    window.location.href = "http://localhost:4200";
  }

  public lenguaje() {
    this.lenguajeTable = {
      "search": "Buscar:",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      },
      "emptyTable": "Datos no encontrados",
      "info": "Mostrando _START_ a  _END_ de _TOTAL_ Datos",
      "lengthMenu": "Cargados _MENU_ Datos",
      "zeroRecords": "No se encontrarón resultados",
      "infoEmpty": "Mostrando 0  de _MAX_ Datos",
      "infoFiltered": "(filtrado entre _MAX_ total de Datos)"

    };

    return this.lenguajeTable;
  }

  public SendFile(data, url) {

    // alert(data);
    let headers = new Headers();
    //headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'))
    let options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });
    return this.http.post(this.apiUrl + url, data, options)
      .map(res => res.json());
  }



  public validationCharacter() {
    $('body').on('keypress', '.sololetras', function (e) {
      var key = e.keyCode || e.which;
      var tecla = String.fromCharCode(key).toLowerCase();
      var letras = " abcdefghijklmnñopqrstuvwxyz";
      var especiales: any = "8-37-39-46";

      var tecla_especial = false
      for (var i in especiales) {
        if (key == especiales[i]) {
          tecla_especial = true;
          break;
        }
      }

      if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
      }
      var text = $(this).val();
      text = text.replace(/\s+/gi, ' ');
      $(this).val(text);



    });
  }

 
  public replaceSpace() {
    $('body').on('keypress', '.sinespacio', function (e) {

      let key = e.keyCode ? e.keyCode : e.which;
      if (key == 32) {
        return false;
      }
      var text = $(this).val();
      text = text.replace(/\s+/gi, '');
      $(this).val(text);



    });
  } 

  public validationCharacterNumerick() {
    $('body').on('keypress', '.letrasandnum', function (e) {
      var key = e.keyCode || e.which;
      var tecla = String.fromCharCode(key).toLowerCase();
      var letras = " abcdefghijklmnñopqrstuvwxyz134567890";
      var especiales: any = "8-37-39-46";

      var tecla_especial = false
      for (var i in especiales) {
        if (key == especiales[i]) {
          tecla_especial = true;
          break;
        }
      }

      if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
      }
      var text = $(this).val();
      text = text.replace(/\s+/gi, ' ');
      $(this).val(text);



    });
  }

  public soloNumeros(){
    $('body').on('keypress','.solonumero-punto',function(e){
    
        var key = window.event ? e.which : e.keyCode ;
        return ((key >= 48 && key <= 57) || (key==8) || key==46 || key==45) ;
    
 });
  }

}
