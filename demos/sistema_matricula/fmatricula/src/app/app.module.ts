import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { DataTablesModule } from 'angular-datatables';
import { NgxPaginationModule } from 'ngx-pagination';
import { AgmCoreModule } from '@agm/core';

import {CategoriasComponent} from './views/Administracion/Categorias/categorias'
import {TipoRecursosComponent} from './views/Mantenimientos/TipoRecursos/tiporecursos'
import {PerfilesComponent} from './views/Seguridad/Perfiles/perfiles'
import {CentrosComponent} from './views/Administracion/Centros/centros'
import {InformacionComponent} from './views/Administracion/Informacion/informacion'

import { CarouselModule } from 'ngx-bootstrap/carousel';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './error/404.component';
import { P500Component } from './error/500.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {UsuariosComponent} from './views/Seguridad/usuarios/users.component';
import {ArgoogleComponent} from './views/Administracion/Argoogle/Argoogle';
import {CategoryPlaceomponent} from './views/Mantenimientos/CategoriasLugares/categoriaslugares';
import {PaisesComponent} from './views/Mantenimientos/Paises/paises';
import {DeptosComponent} from './views/Mantenimientos/Departamentos/departamentos';
import {PerfilComponent} from './views/Administracion/Perfil/perfil';
import {TutorialesComponent} from './views/Administracion/Tutoriales/Tutoriales';
import {PersonsComponent} from './views/security/persons/persons.component';
import { SectionsComponent } from './views/Mantenimientos/sections/sections.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Configuration } from './ConfigSystems/constants';
import { HttpModule } from '@angular/http';
import { from } from 'rxjs';
import { BuildingsComponent } from './views/Mantenimientos/buildings/buildings.component';
import { ClassroomsComponent } from './views/Mantenimientos/classrooms/classrooms.component';
import { ModulesComponent } from './views/Mantenimientos/modules/modules.component';
import { SchedulesComponent } from './views/Mantenimientos/schedules/schedules.component';
import { ClassDaysComponent } from './views/Mantenimientos/class-days/class-days.component';
import { ParametersComponent } from './views/Mantenimientos/parameters/parameters.component';
import { ClassComponent } from './views/Mantenimientos/class/class.component';



@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 1100,
      positionClass: 'toast-top-right',
      preventDuplicates: true
    }),
    HttpModule,
    DataTablesModule,
    
    /*AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC8ZtAcnnrB8vDWCPpXhEkFKd4Ut0VgYUo'
    })*/
    
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    CategoriasComponent,
    TipoRecursosComponent,
    PerfilesComponent,
    CentrosComponent,
    InformacionComponent,
    UsuariosComponent,
    ArgoogleComponent,
    CategoryPlaceomponent,
    PaisesComponent,
    DeptosComponent,
    PerfilComponent,
    TutorialesComponent,
    PersonsComponent,
    SectionsComponent,
    BuildingsComponent,
    ClassroomsComponent,
    ModulesComponent,
    SchedulesComponent,
    ClassDaysComponent,
    ParametersComponent,
    ClassComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  },
    Configuration
],
  bootstrap: [ AppComponent ]

})
export class AppModule { }
