import { Component, Input ,OnInit} from '@angular/core';
import { navItems } from './../../_nav';
import { ServiceService } from '../../service/service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent  implements  OnInit{
  public navItems = [];
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  public uriService="resource_rol";
  public data:any;
  public img:string; 

  constructor(private service: ServiceService) {

    
    
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }

  ngOnInit() {
    this.data =JSON.parse(sessionStorage.getItem("data")); 
    this.img = this.data.data.personImage;
    this.loadresoruces();
  }
  loadresoruces() {
    
    this.navItems.push({
      name:"Inicio",
      url: "/inicio",
      icon: "fa fa-home"
    });
    for(let x of this.data.data.userMenu){
      var children=[];

      
      if(x.listResources.length>0){
          for(let i of x.listResources){
            children.push({
              name:i.resource_name,
              url: i.resource_url,
              icon: i.resource_icon
            });
          }
          this.navItems.push(
            {
              name:x.resource_name,
              url: x.resource_url,
              icon: x.resource_icon,
              children:children
            })
      }
      else{
        this.navItems.push(
          {
            name:x.resource_name,
            url: x.resource_url,
            icon: x.resource_icon
            
          }
        )
      }
      
      
      

    }
  }
}
