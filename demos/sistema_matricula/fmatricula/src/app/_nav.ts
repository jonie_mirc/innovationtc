

export const navItems = [
  {
    name: 'Inicio',
    url: '/dashboard',
    icon: 'icon-speedometer',
    
  },
  {
    name: 'Administracion',
    url: '/security',
    icon: 'monitor',
    children: [
      {
        name: 'Centros',
        url: '/administracion/centros',
        icon: 'icon-options-vertical'
      },
      {
        name: 'Categorias Centros',
        url: '/administracion/categorias',
        icon: 'icon-options-vertical'
      },
      
      {
        name: 'Informacion',
        url: '/administracion/informacion',
        icon: 'icon-options-vertical'
      }
    ]
  },
  {
    name: 'Mantenimientos',
    url: '/security',
    icon: 'settings',
    children: [
      {
        name: 'Tipo recursos',
        url: '/mantenimientos/tiporecursos',
        icon: 'icon-options-vertical'
      }
      
    ]
  },
  {
    name: 'Seguridad',
    url: '/security',
    icon: 'icon-lock',
    children: [
      {
        name: 'Perfiles',
        url: '/seguridad/perfiles',
        icon: 'user-female'
      },
      {
        name: 'Usuarios',
        url: '/seguridad/usuarios',
        icon: 'people'
      }
      
      
    ]
  }
];
